==========
hmc_marine
==========


A collection of functions and class for wave Marine Engineering applications at HMC


Description
===========

This projects gives a collection of spectral functions. It comes with the following modules

* wave_spectra : A set of functions to create wave spectra and distribution functions
* wave_fields : Create and simulate 1D and 2D waves based on the *wave_spectra* module
* date_readers : Several data reader of marine wave spectra (Fugro/MetOcean/Wave Rider Buoy) and
  RAO's (LiftDyn
* sequency_tool_utils : utilities for reading the sequence tool output

Examples
========

* Wave Spectra Functions : :download:`../examples/examples_wave_spectra.html`
* Wave Field Modelling 1D : :download:`../examples/example_wave_field_modelling.html`
* Wave Field Modelling 2D : :download:`../examples/example_wave_field_2D_modelling.html`
* Liftdyn Data Reader : :download:`../examples/examples_data_readers_liftdyn.html`
* Spectral Data Readers : :download:`../examples/example_spectral_data_readers.html`
* Sequence Tool Example : :download:`../examples/example_sequence_tool_utils.html`

Unit Test
=========

In order to run the standard unit test do

    python setup.py test

Installation
============

Installation at the HMC package directory X: (where X points to the network drive installation folder)

* Create the installation package for the current checkout version of the source code

    python setup.py sdist

* Install the package at location X:/

    pip install hmc_marine --no-index --find-links ./dist/hmc_marine-0.1.1.tar.gz --prefix=X:/


* For upgrading the package a --upgrade option needs to be supplied

    pip install hmc_marine --no-index --find-links ./dist/hmc_marine-0.1.1.post0.tar.gz --prefix=X:/hmc_py35 --upgrade

* To install the documentation first create a directory X:/hmc_py35/docs/hmc_marine and then do

    python setup.py docs --build-dir X:/hmc_py35/docs/hmc_marine

Note
====

This project has been set up using PyScaffold 3.0. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
