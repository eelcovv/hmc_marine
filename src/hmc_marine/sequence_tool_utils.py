# -*- coding: utf-8 -*-
"""
Created on Thu Jan 29 09:20:10 2015

@author: Eelco van Vliet

Serie of tools for marine applications

sequenceToolSummary
    read the summary file of the sequence tool and fill the data fields
    with the information
"""
from __future__ import division
from __future__ import print_function

import os
import re  # regular expressions
from builtins import object
from builtins import range
from fnmatch import fnmatch

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io as sio
import seaborn as sns

from hmc_utils.date_time import (matlabnum2date)
from hmc_utils.plotting import (set_limits)
from hmc_utils.misc import (get_logger)

sns.set(context="notebook")


class PeriodsMonths(object):
    """
    Defines the period strings over the year
    """

    def __init__(self):
        self.months = \
            [
                "01.Jan - 31.Jan",
                "01.Feb - 29.Feb",
                "01.Mar - 31.Mar",
                "01.Apr - 30.Apr",
                "01.May - 31.May",
                "01.Jun - 30.Jun",
                "01.Jul - 31.Jul",
                "01.Aug - 31.Aug",
                "01.Sep - 30.Sep",
                "01.Oct - 31.Oct",
                "01.Nov - 30.Nov",
                "01.Dec - 31.Dec"
            ]
        self.year = "01.Jan - 31.Dec"

        # extract the short month name
        self.months_short = []
        self.months_index = []
        regExp = re.compile('\d+\.(\w+) - \d+\.\w+')
        for i in range(len(self.months)):
            match = regExp.match(self.months[i])
            if bool(match):
                self.months_short.append(match.group(1))
                self.months_index.append(i + 1)

    def __str__(self):
        info = dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))
        info_str = ""
        max_number_of_items = 20
        for attribute, value in info.items():
            is_list = ""
            if isinstance(value, list):
                val = value[0]
                if isinstance(val, list):
                    val = val[0]
                    is_list = " (first item from double list)"
                else:
                    if len(value) <= max_number_of_items:
                        val = value
                    else:
                        val = value[:max_number_of_items]
                        is_list = " (first {} items)".format(max_number_of_items)
            else:
                val = value
            info_str += "{:20s} : {} {}\n".format(attribute, val, is_list)
        return info_str


class SequenceData(object):
    """
    Class to store the sequence tool data for the total Operation time, W.o.W.
    time and workability.

    Attributes
    ----------
    per_month : list
        WoW per month in number of hours
    per_month_in_days : list
        WoW per month in days
    per_month_sequence : list
        WoW for each individual operation in the sequence
    per_year : float
        WoW per year in number of hours
    per_year_in_days : float
        WoW per year in days
    per_year_sequence : list
        WoW for each individual operation in the sequence
    nett_days : float
        Nett number of working days. Only available for the total Time
    nett_days_sequence : float
        Nett number of working days per operation in the sequence. Only available for the total Time
    """

    def __init__(self):
        self.per_month = []
        self.per_month_in_days = []
        self.per_month_sequence = []
        self.per_year = 0
        self.per_year_in_days = 0
        self.per_year_sequence = []
        self.nett_days = 0
        self.nett_days_sequence = 0

    def __str__(self):
        info = dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))
        info_str = ""
        max_number_of_items = 20
        for attribute, value in info.items():
            is_list = ""
            if isinstance(value, list):
                val = value[0]
                if isinstance(val, list):
                    val = val[0]
                    is_list = " (first item from double list)"
                else:
                    if len(value) <= max_number_of_items:
                        val = value
                    else:
                        val = value[:max_number_of_items]
                        is_list = " (first {} items)".format(max_number_of_items)
            else:
                val = value
            info_str += "{:20s} : {} {}\n".format(attribute, val, is_list)
        return info_str


class CumulativeDataSeqTool(object):
    """
    Class to store the cumulative density function data of the sequence tool data
    The lists per_month and per_year contain the cumulative data

    Attributes
    ----------
    per_month : list
        Mean Wow in hours per month
    per_month_in_days : list
        Mean Wow in days per month
    per_year : list
        Mean Wow in hours per year
    per_year_in_days : list
        Mean Wow in days per year
    p90_per_month : list
        p90 Wow in hours per month
    p90_per_year : float
        P90 Wow in hours per year
    probability : float
        Current probability
    """

    def __init__(self):
        self.per_month = []
        self.per_month_in_days = []
        self.per_year = []
        self.per_year_in_days = []
        self.p90_per_month = []
        self.p90_per_year = 0
        self.probability = 0

    def __str__(self):

        info = dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))
        info_str = ""
        max_number_of_items = 20
        for attribute, value in info.items():
            is_list = ""
            if isinstance(value, list):
                val = value[0]
                if isinstance(val, list):
                    val = val[0]
                    is_list = " (first item from double list)"
                else:
                    if len(value) <= max_number_of_items:
                        val = value
                    else:
                        val = value[:max_number_of_items]
                        is_list = " (first {} items)".format(max_number_of_items)
            else:
                val = value
            info_str += "{:20s} : {} {}\n".format(attribute, val, is_list)
        return info_str


class ReadFlagsSeqTool(object):
    """
    Dictionary with the section identification lines

    Attributes
    ----------
    sections: dict
        All the section identifiers

    The update_flags routine is used to set the flags once a key line is encountered.
    """

    def __init__(self):
        self.sections = {
            "Header": [True, "This is the header starting with True"],
            "Planning": [False,
                         "Planning description"],
            "RAO": [False,
                    "Used rao files:"],
            "Variables": [False,
                          "Program variables:"],
            "TotalOperationTime": [False,
                                   "Average total operation time incl. WoW and delay \[hrs\]"],
            "WowTime": [False,
                        "Average waiting time \[hrs\] incl. WoW and delay \[hrs\]:"],
            "Workability": [False,
                            "Workability percentages:"],
            "CumulativeFunctionWoWHrs":
                [False, "Cumulative distribution functions of total waiting on weather  \[hrs\]"],
            "CumulativeFunctionWoWDays":
                [False, "Cumulative distribution functions of total waiting on weather  \[days\]"],
        }

    def update_flags(self, line):
        """
        Set the flags once a key line is encountered.

        Parameters
        ----------
        line: str
            Current line to scan

        """
        # loop over all the flags and test if we encounter a key line specifying
        # a new section. If so, change the flags
        for key, v in list(self.sections.items()):
            key_line = v[1]
            if bool(re.search(key_line, line)):
                # we encountered a key line. Set all the flags false except this one
                for k2, v2 in list(self.sections.items()):
                    if k2 == key:
                        self.sections[k2] = [True, v2[1]]
                    else:
                        self.sections[k2] = [False, v2[1]]
                # stop processing
                break


class SequenceToolSummary(object):
    """
    Read the summary file of the sequence tool and return a class containing
    all the values

    Parameters
    ----------
    filename: str
        Name of the sequency tool seq_out file to read

    Attributes
    ----------
    number_of_sequence: int
        Number of sequences

    Examples
    --------

    To read a sequence tool summary file you can do the following

    >>> file_name = "data\\SequenceTool\\Sanha_Deep\\pipelay_summary.seq_out"
    >>> if not os.path.exists(file_name):
    ...    file_name = os.path.join("..", file_name)
    >>> seq = SequenceToolSummary(file_name)

    The "seq" object now carries all the information of the Sequency tool summary file. The
    Waiting for weather per month in days is for instance

    >>> seq.wow_time.per_month_in_days
    [82.8, 106.1, 122.2, 129.5, 120.7, 103.8, 83.1, 66.6, 58.2, 51.2, 50.5, 59.5]

    Or the cumulative probability

    >>> seq.cumulative_wow_probability.p90_per_month
    [158.9, 199.8, 199.5, 197.1, 180.9, 151.9, 127.9, 103.1, 79.8, 76.8, 122.9, 109.8]

    """

    def __init__(self, filename):

        self.filename = filename

        if not fnmatch(filename, '*_summary.seq_out'):
            raise ValueError("Filename should be a sequqnce tool summary ending with "
                             "'_summary.seq_out'")

        # initialise the read flag to specify which section to read
        read = ReadFlagsSeqTool()

        # initialise some variables
        self.datarun1 = None
        self.number_of_sequence = 0

        # initialise the sequence data for total time, wow time and workability
        self.total_operation_time = SequenceData()
        self.wow_time = SequenceData()
        self.workability = SequenceData()

        self.cumulative_wow_probability = CumulativeDataSeqTool()

        self.periods = PeriodsMonths()

        # start the loop over all the line of the file
        # closing is done automatically in case of failure
        with open(filename, 'r') as fp:
            for line in fp:

                # update the flags to specify which section we are in
                read.update_flags(line)

                # analyse the current line according to the appropriate
                # section
                if read.sections["Header"][0]:
                    self.read_header_line(line)
                elif read.sections["Planning"][0]:
                    self.read_planning_line(line)
                elif read.sections["RAO"][0]:
                    # skipped for now. 
                    pass
                elif read.sections["Variables"][0]:
                    # skip for now
                    pass
                elif read.sections["TotalOperationTime"][0]:
                    # read the average waiting on waiter time per period
                    self.read_sequence_data(line, self.total_operation_time)
                elif read.sections["WowTime"][0]:
                    # read the average waiting on waiter time per period
                    self.read_sequence_data(line, self.wow_time)
                elif read.sections["Workability"][0]:
                    # read the workability and store in self.workability              
                    self.read_sequence_data(line, self.workability)
                elif read.sections["CumulativeFunctionWoWHrs"][0]:
                    # read the cumulative WoW data
                    self.read_sequence_data(line, self.cumulative_wow_probability, True)
                elif read.sections["CumulativeFunctionWoWDays"][0]:
                    # pass this one, it is the same as the previous
                    pass

    def read_sequence_data(self, line, data, cummulative=False):
        # reads the section containing the sequence data (total Time, WoW,
        # workability) per month and the total year. In case of the totalTime
        # also the Nett span line is read

        # check if the line start with '01.Jan - 01.Feb', etc. -or- 'Nett span'
        regular_expression = re.compile('(\d+\.\w+ - \d+\.\w+)|(Nett span)|(Period)')
        match = regular_expression.match(line)
        if (bool(match)):
            # get the matched reg expr. and remove it from the line
            period = match.group()
            line = line.replace(period, '').strip()

            # split the rest of the line in collumns
            cols = line.split()

            if not cummulative and period != 'Period':
                # the first numberOfSeq numbers are per step in the sequence
                sequence = cols[:self.number_of_sequence]

                # than the total number of hours for the period is given            
                total = float(cols[self.number_of_sequence])

                # depending on the period (or month, the whole year, or nett)
                if period in PeriodsMonths().months:
                    # the period is a month. Store the list and totals
                    data.per_month.append(total)
                    # store the  number per mont in days, round of up to one digit
                    data.per_month_in_days.append(float("{:.1f}".format(total / 24.0)))
                    data.per_month_sequence.append([float(s) for s in sequence])
                elif period == PeriodsMonths().year:
                    # the period is the whole year. Store the list and totals
                    data.per_year = total
                    data.per_year_in_days = float("{:.1f}".format(total / 24.))
                    data.per_year_sequence = [float(s) for s in sequence]
                elif period == 'Nett span':
                    # the period is the Nett spann. Store the list and total
                    data.nett_days = total
                    data.nett_days_sequence = sequence
            else:
                # we are reading the cumulative data now. 
                # if the period is 'Period' then the following data
                # give the probability of under exceeding
                if period == 'Period':
                    # just store the changes 10,20...100
                    data.probability = [float(c) for c in cols if c not in ("Total", "days",
                                                                            "[days]")]
                else:
                    # convert the hours in days and retrieve the P90
                    cols_in_days = [float("{:.1f}".format(float(x) / 24.)) for x in cols]
                    P90 = cols_in_days[8]
                    if period in PeriodsMonths().months:
                        # store the probabiltiy function and P90 per month
                        data.per_month.append([float(c) for c in cols])
                        data.per_month_in_days.append([float(c) for c in cols_in_days])
                        data.p90_per_month.append(P90)
                    elif period in PeriodsMonths().year:
                        # store the probabiltiy function and P90 for the year
                        data.per_year.append([float(c) for c in cols])
                        data.per_year_in_days.append([float(c) for c in cols_in_days])
                        data.p90_per_year = P90

    def read_header_line(self, line):
        # read the header section containing some info about the sequence
        cols = line.split(':')
        if len(cols) >= 2:
            if bool(re.search("Date run", cols[0])):
                # The data run is set two times, so store them both
                if self.datarun1 is None:
                    self.datarun1 = cols[1].strip() + ":" + cols[2] + ":" + cols[3]
                else:
                    self.datarun2 = cols[1].strip() + ":" + cols[2] + ":" + cols[3]
                    # get the tool version
            elif bool(re.search("Tool version", cols[0])):
                self.toolversion = cols[1].strip()
            # get the filename
            elif bool(re.search("File name", cols[0])):
                self.filename = cols[1].strip()
                if len(cols) > 2:
                    self.filename += ":" + cols[2].strip()
            elif bool(re.search("Project Name", cols[0])):
                self.ProjectName = cols[1].strip()
            elif bool(re.search("Project Code", cols[0])):
                self.ProjectCode = cols[1].strip()
            elif bool(re.search("Vessel name", cols[0])):
                self.VesselName = cols[1].strip()
            elif bool(re.search("Originator", cols[0])):
                self.Originator = cols[1].strip()
            elif bool(re.search("Environmental Data", cols[0])):
                self.EnvironmentalData = cols[1].strip()
                if len(cols) > 2:
                    self.filename += ":" + cols[2].strip()
            elif bool(re.search("Startday", cols[0])):
                self.Startday = cols[1].strip()
            elif bool(re.search("Startmonth", cols[0])):
                self.Startmonth = cols[1].strip()
            elif bool(re.search("Startyear", cols[0])):
                self.Startyear = cols[1].strip()
            elif bool(re.search("Endday", cols[0])):
                self.Endday = cols[1].strip()
            elif bool(re.search("Endmonth", cols[0])):
                self.Endmonth = cols[1].strip()
            elif bool(re.search("Endyear", cols[0])):
                self.Endyear = cols[1].strip()
            elif bool(re.search("Dayinterval", cols[0])):
                self.Dayinterval = cols[1].strip()
            elif bool(re.search("Latitude", cols[0])):
                self.Latitude = cols[1].strip()
            elif bool(re.search("Longitude", cols[0])):
                self.Longitude = cols[1].strip()
            elif bool(re.search("RadiusOfClearance", cols[0])):
                self.RadiusOfClearance = cols[1].strip()
            elif bool(re.search("DMOBMOB", cols[0])):
                self.DMOBMOB = cols[1].strip()

    def read_planning_line(self, line):
        # counts the number of planning steps. Can be extended
        # later to fill in the details
        cols = line.split('|')
        if bool(re.search("^\d+", cols[0])):
            self.number_of_sequence += 1

    def props(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes        
        return dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def list_properties(self):
        # list of the property fields and its values of this class
        for k, v in list(self.props().items()):
            if isinstance(v, list):
                if isinstance(v[0], list):
                    print("{:<18} : {}...".format(k, v[0][0:5]))
                else:
                    print("{:<18} : {}...".format(k, v[0:5]))
            else:
                print("{:<18} : {}".format(k, v))

    def report(self):
        # make a report of the stored data
        bar = "----------------------------------------------------------"

        months = PeriodsMonths().months
        year = PeriodsMonths().year

        print("\n{}\n{}\n{}".format(bar, "Total Operation Time", bar))
        c = 0
        for totOp in self.total_operation_time.per_month:
            print("{:<18} : {} hrs ({:.1f} days)".format(months[c], totOp, totOp / 24.))
            c += 1
        print("{}".format(bar))
        totOp = self.total_operation_time.per_year
        print("{:<18} : {} hrs ({:.1f} days)".format(year, totOp, totOp / 24.))
        print("{}".format(bar))
        totOp = self.total_operation_time.nett_days
        print("{:<18} : {} hrs ({:.1f} days)".format('Nett span', totOp, totOp / 24.))
        print("{}".format(bar))

        print("\n{}\n{:<18} {:>18}{:>18}\n{}".format(bar, "Period", "WoW (days)", "P90", bar))

        for c in range(12):
            wow = self.wow_time.per_month_in_days[c]
            P90 = self.cumulative_wow_probability.p90_per_month[c]
            print("{:<18}:{:>18}{:>18}".format(months[c], wow, P90))

        print("{}".format(bar))
        wow = self.wow_time.per_year_in_days
        print("{:<18}:{:>18}{:>18}".format(year, wow, P90))
        print("{}".format(bar))

        print("\n{}\n{}\n{}".format(bar, "Workability", bar))
        c = 0
        for workability in self.workability.per_month:
            print("{:<18} : {} %".format(months[c], workability))
            c += 1
        print("{}".format(bar))

    def plot_wow(self, fig=None, axis=None, plot_title=None, plot_sub_title=None,
                  legend_title=None, line_style="-o", line_title="", wow_min=0,
                 wow_max=None, p90_min=0, p90_max=None):
        """
        Plot the current wow + P90 vs the month

        Parameters
        ----------
        fig: object, optional
            Reference to figure
        axis: list, optional
            Reference to axis
        plot_title: str or None, optional
            A title to add above the plot
        plot_sub_title: str or None, optional
            A title to add above the plot
        legend_title: str or None, optional
            A title to add
        line_style: str or None, optional
            Style of the current line
        line_title: str or None, optional
            Title of the current line
        wow_min: float, optional
            Minimum wow value in plot. Default = 0
        wow_max: float, optional
            Maximum wow value in plot. Default = None, ie automatic
        p90_min: float, optional
            Minimum p90 value in plot. Default = 0
        p90_max: float, optional
            Maximum p90 value in plot. Default = None, ie automatic

        Returns
        -------
        tuple (fig, axis)

        """

        if fig is None:
            fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True)
            if plot_title is not None:
                fig.canvas.set_window_title(plot_title)
            if plot_sub_title is not None:
                plt.figtext(0.02, 0.98, plot_sub_title, ha="left", va="top")

        # extract the monthIndex, WoW and P90 from data structure seq
        ind = self.periods.months_index
        months = self.periods.months_short
        wow = self.wow_time.per_month_in_days
        P90 = self.cumulative_wow_probability.p90_per_month

        for i_ax, ax in enumerate(axis):
            if i_ax == 0:
                if plot_title is not None:
                    ax.set_title(plot_title)

                ax.set_ylabel('WoW [days]')
                ax.plot(ind, wow, line_style, label=line_title)
                lim = set_limits(ax, v_min=wow_min, v_max=wow_max, direction="y")
            elif i_ax == 1:
                ax.set_xlabel('Month')
                ax.set_ylabel('P$_{90}$ [days]')
                ax.plot(ind, P90, line_style)

                # set the xticks with the month names.
                ax.set_xticks(ind)
                ax.set_xticklabels(months)
                lim = set_limits(ax, v_min=p90_min, v_max=p90_max, direction="y")

        # add the legend
        if legend_title is not None:
            axis[0].legend(loc="best", title=legend_title)

        return fig, axis


class Spec2d(object):
    """
   convert a numpy structure to a '3d' spectrum to a class
    """

    def __init__(self, struct):
        for name in struct.dtype.names:
            setattr(self, name, struct[name][0])


class SequenceToolEnvironment(object):
    """
    Read the environment output file from the sequence tool in order
    to get the all the weather data and spectra and responses
    """

    def __init__(self, filename, store_in_data_frame=True):

        self.logger = get_logger(__name__)

        self.store_in_data_frame = store_in_data_frame

        if not fnmatch(filename, '*.env_mat'):
            raise ValueError("Filename should be an environment matlab file ending with '.env_mat'")

        # read the matlab structure
        # closing is done automatically in case of failure
        try:
            envmat = sio.loadmat(filename)
        except IOError as err:
            print("{}".format(err))
            raise
        except:
            print("An unexpected error")
            raise

        # set the version and header from the matlab struct
        try:
            self.__header__ = envmat['__header__']
        except:
            print("No header found")
        try:
            self.__version__ = envmat['__version__']
        except:
            print("No version found")

        # the field Environment contains a one file numpy value containing
        # the structure of the matlab environment values
        # Therefore I copy this to the environment variable, and then loop over
        # the dtype names and construct a property field for each name
        environment = envmat['Environment'][0, 0]

        if self.store_in_data_frame:
            self.data = pd.DataFrame(index=matlabnum2date(environment["Time"].reshape(-1)))
            self.info = dict()
        else:
            self.data = None
            self.info = None

        for name in environment.dtype.names:
            # this adds a new class field with the name'name' and the data
            if name == "selection":
                # this field contains a list of numpy strings of all the
                # select variables used. I convert them to ordinary strings 
                selection = []
                for item in environment[name]:
                    selection.append(item[0][0])
                    setattr(self, name, selection)
            elif name == "s3d":
                # contains a list of 3d spectra (freq vs direction)
                # store the s3d in numpy and list format. 
                setattr(self, name, environment[name])
                nl = name + "list"
                setattr(self, nl, self.ML2PYspec3d(environment[name]))
            else:
                # read the data. Reshape to get rid of the (npoint,1)
                # data format and turn it into a (npoint) array
                self.logger.debug("Storing data {}".format(name))
                if self.store_in_data_frame:
                    try:
                        self.data[name] = environment[name].reshape(-1)
                    except ValueError:
                        # if it does not fit the index, it is a information string
                        self.info[name] = environment[name].reshape(-1)
                else:
                    setattr(self, name, environment[name].reshape(-1))

    def ML2PYspec3d(self, s3dlist):
        """
        For each spectrum in time convert it to a class an put it in a list
        """
        spec3d_class_list = []
        for i in range(s3dlist.size):
            spec3d_class_list.append(Spec2d(s3dlist[i]))
        return spec3d_class_list

    def props(self):
        # purpose: return all properties of the class in one dictionary, but
        # exclude the classes        
        return dict(
            (key, getattr(self, key)) for key in dir(self) if key not in dir(self.__class__))

    def list_properties(self):
        # list of the property fields and its values of this class
        for k, v in list(self.props().items()):
            tv = type(v)
            if tv is np.ndarray:
                print("{:<40} : {:<8}".format(k, v.size))
            elif tv is dict:
                print("{:<40} : {}".format(k, type(v)))
            elif tv is str or tv is bytes:  #
                print("{:<40} : {}".format(k, v))
                # else :
                #    print("{:<40} : {}".format(k,type(v)))
