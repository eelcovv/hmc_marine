"""
some Marine data reader functions

Eelco van Vliet, 2017
"""
from __future__ import division
from __future__ import print_function

import logging
import os
import re
from collections import (OrderedDict, namedtuple)

import colorcet as cc
import dateutil.parser as dparser
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np
import pandas as pd
from future.builtins import object
from future.builtins import range
from matplotlib import animation
from scipy import linspace
from scipy import pi, sqrt, exp
from scipy.constants import nautical_mile
from scipy.interpolate import (interp1d, interp2d)
from scipy.special import erf

from hmc_marine.wave_spectra import (omega_e_vs_omega, set_heading, spectrum_jonswap,
                                     spectrum_gauss, spreading_function, spreading_function2)
from hmc_utils import Q_
from hmc_utils.file_and_directory import (make_directory, scan_base_directory)
from hmc_utils.geographic import (travel_distance_and_heading_from_coordinates)
from hmc_utils.misc import (get_logger, read_value_from_dict_if_valid, Timer, create_logger)
from hmc_utils.numerical import (get_nearest_index, make_2d_array_cyclic)
from hmc_utils.plotting import (sub_plot_axis_to_2d, clean_up_artists)

LIFT_DYN_FILE_EXTENSION = (".sgr_plt", ".stf_plt")
TRANS_SPEC2D_FILE_EXTENSION = (".csv", ".nc", ".oct")
WRB_FILE_EXTENSION = (".spt", ".nc")
SPEC_COMP_EXTENSION = (".xls", ".xlsx")

RELATIVE_DIRECTION_PREFIX = "relative"

# create named tuple for sacs reader
SACS_ELEMENT = namedtuple("SACS_ELEMENT", "member_number member_end group_id forces moments")

# definition of a default spectral_components dictionary giving the definition of the type of
# spectral ccomponent which can be present in a component input file. Note that for both the
# spectral distribution and directional distribution
# a version can be speficied: either hmc or dnv. For the spectrum it is save to take the "hmc"
# version, however, for the directional distribution the "dnv" version is implemented: the shape is
# almost the same, except that the "dnv"
# version allows to set a much more narrow width of the directional distribution than the "hmc"
# version
SPECTRAL_COMPONENTS = dict(
    wind_waves=dict(
        Hs="Hs_wind",
        Tp="Tp_wind",
        Theta0="direction_wave_wind",
        directional_distribution=dict(
            s_spreading_factor=5,
            version="dnv"),
        energy_spectrum=dict(
            type="jonswap",
            gamma=3.3,
            version="hmc")
    ),
    swell1=dict(
        Hs="Hs_swell",
        Tp="Tp_swell",
        Theta0="direction_wave_swell",
        directional_distribution=dict(
            s_spreading_factor=13,
            version="dnv"),
        energy_spectrum=dict(
            type="gauss",
            version="hmc")
    ),
    swell2=dict(
        Hs="Hs_swell2",
        Tp="Tp_swell2",
        Theta0="direction_wave_swell2",
        directional_distribution=dict(
            s_spreading_factor=13,
            version="dnv"),
        energy_spectrum=dict(
            type="gauss",
            version="hmc")
    )
)


class NetCDFInfo(object):
    """
    Class to hold information of a netcdf object

    Parameters
    ----------
    file_prefix : str, optional
        Prefix for the file name to open. Default = envi_db
    file_location : str, optional
        Directory where the netcdf file is located. Default = "." (current directory)
    write_new_data : bool, optional
        If true, the netcdf is initialised, otherwise data is appended to an existing database.
        Default = True
    read_stored_data : bool, optional
        In case a message_pck file exist, read this data in steat of the original. Default = True
    settings_dict : dict, optional
        Dictionary containing the settings which can also be passed with the command line. If the
        settings dict is passed, all the settings a imposed based on the values in the dictionary.
        The dictionary should contain the following fields

        * file_prefix : "envi_db"
        * file_location : "."
        * write_new_data: True
        * read_stored_data: True
    reset_netcdf : bool, optional
        Reset the netcdf file if it was already existing
    group_name : str, optional
        Name of the group of variables as stored in the netcdf file

    Notes
    -----

    The NetCDFInfo class is used to set up a netcdf data base. This data base is used to store the
    converted spectral data from various sources. After storing the converted data the first time,
    each next run you can read the spectral data from the netcdf data base in stead of  having to
    convert the spectral data again. This speeds up the data reading time significantly.

    Examples
    --------

    To set up a data base to:

    >>> data_location = os.path.join("..", "data")
    >>> file_name = os.path.join(data_location, "Fugro_I0450_20170510_06.csv")
    >>> netcdf = NetCDFInfo(file_location=data_location, reset_netcdf=False)

    The object `netcdf` can be passed to the spectral reader function as

    >>> fugro = WaveSpectralEnergy2DReader(file_name=file_name, netcdf=netcdf)

    The first time the data is read, all the data will be dumped to the netcdf file specified in the
    netcdf object. The next run the netcdf exists and all the spectral data will be obtained from
    here. For Fugro spectral data the reading time is reduced with a factor of 3, for the wave rider
    buoy the reading time is reduced with a factor of 100.

    """

    def __init__(self, file_prefix="envi_db", file_location=".", write_new_data=True,
                 read_stored_data=True, settings_dict=None, reset_netcdf=False,
                 group_name="header_variables"):

        self.logger = get_logger(__name__)
        self.file_prefix = file_prefix
        self.file_location = file_location
        try:
            self.file_base_name = os.path.join(self.file_location, self.file_prefix)
        except TypeError:
            self.file_base_name = None
        self.file_name_ds = None
        self.file_name_mp = None
        self.write_new_data = write_new_data
        self.read_stored_data = read_stored_data
        self.reset_netcdf = reset_netcdf
        self.group_name = group_name
        self.dataset = None
        self.header_df = None
        # a holder for all the groups in the netcdf data set
        self.groups = OrderedDict()
        if settings_dict is not None:
            self.read_settings(settings_dict)

    def read_settings(self, settings_dict):
        """
        Impose the values of the class Attributes based on the setting dictionary
        Parameters
        ----------
        settings_dict : dict
            Dictionary with all the settings to be imposed
        """
        self.file_prefix = read_value_from_dict_if_valid(settings_dict, "file_prefix", "envi_db")
        self.file_location = read_value_from_dict_if_valid(settings_dict, "file_location", ".")
        make_directory(self.file_location)
        self.file_base_name = os.path.join(self.file_location, self.file_prefix)
        self.write_new_data = read_value_from_dict_if_valid(settings_dict, "write_new_data", True)
        self.read_stored_data = read_value_from_dict_if_valid(settings_dict, "read_stored_data",
                                                              True)

    def init_net_cdf_database(self, file_appendix):
        """
        Initialise a netcdf file to  write or read

        Parameters
        ----------
        file_appendix: str
            Appendix to add to the file name
        """

        # the file_base_name is te base name of the netcdf files including the output path. Append a
        # file_appex to create a file for each start data/time and database type
        self.file_name_ds = "_".join([self.file_base_name, file_appendix]) + ".nc"
        self.file_name_mp = "_".join([self.file_base_name, file_appendix]) + ".msg_pack"

        if self.write_new_data or self.read_stored_data:
            # we want to read or write data from the net CFD file
            if self.reset_netcdf or not os.path.exists(self.file_name_ds):
                # we want to reset the net cdf data base or it does not yet exist. Initialise it
                self.logger.info("Creating new netcdf data base {}".format(self.file_name_ds))
                self.dataset = nc.Dataset(self.file_name_ds, "w", format="NETCDF4")
                # self.groups[self.group_name] = self.dataset.createGroup(self.group_name)
            else:
                # we want to read an existing net cdf data base
                if self.write_new_data:
                    # in case new data is created, store it to the netcdf file
                    this_mode = "a"
                else:
                    # we only want to read the net cdf file
                    this_mode = "r"

                self.logger.debug("Open existing netcdf data base {} in mode '{}'"
                                  "".format(self.file_name_ds, this_mode))
                self.dataset = nc.Dataset(self.file_name_ds, this_mode)
                try:
                    self.header_df = pd.read_msgpack(self.file_name_mp)
                except (IOError, ValueError):
                    self.logger.debug("Could not read message pack file {}"
                                      "".format(self.file_name_mp))

    def add_dimension(self, name, units=None, data_type="f8", values=None, group=None):
        """
        Add a dimension to the date set and put a reference

        Parameters
        ----------
        name : str
            Name of this dimension
        units : str, optional
            The units of the quantity to create. Default = None
        data_type: str, optional
            The data type of the variable data. Default = "f8". See netcdf manual for the available
            types
        values: None or ndarray, optional
            The data to fill the dimension. Default = None
        group: None or str
            The name of the group. Default = None
        """

        if group is not None:
            dataset = group
        else:
            dataset = self.dataset

        if re.search("time", name) and units is None:
            units = "hours since 0001-01-01 00:00:00"
            calendar = "gregorian"
        else:
            calendar = None

        if values is None:
            length = None
        else:
            length = values.size

        if name not in list(dataset.variables.keys()):
            # create the dimension
            self.logger.debug("In NetCDFInfo Adding dimension {} {} {} size {} to group"
                              "".format(name, units, data_type, length, group))
            dataset.createDimension(name, length)
            dimension_data = dataset.createVariable(name, data_type, (name,), zlib=True)
            dimension_data[:] = values
            if calendar is not None:
                dimension_data.calendar = calendar

            dimension_data.units = units

    def get_next_time_index(self, time_var_name="time"):
        """
        Obtain the next index to to the variables in a net cdf data base

        Parameters
        ----------
        time_var_name: str, optional
            Name of the time index. Default = "time"

        Returns
        -------
        (integer, float)
            Tupple (i, t) with the next index i and the last time value t
        """

        time_var = self.dataset.variables[time_var_name]

        # get the last index based on the size of the array
        last_time_index = time_var[:].shape[0] - 1

        # if the index is valid we can retrieve the time value belonging to the last index
        if last_time_index >= 0:
            last_time_value = time_var[last_time_index - 1]
        else:
            # no times have been stored yet, return None for the value
            last_time_value = None

        next_time_index = last_time_index + 1

        return next_time_index, last_time_value

    def append_data(self, record_date_time, record_data, record_data_name="psd",
                    time_var_name="time"):
        """
        Add the record data to the netcdf dataset at time `record_date_time`

        Parameters
        ----------
        record_date_time : DataFrame
            The current 2D record data
        record_data: DataFrame
            2D array with the psd data
        """

        # turn the date/time stamp into the float of hours since 1/1/1
        hours_since = nc.date2num(pd.to_datetime(record_date_time),
                                  units=self.dataset.variables["time"].units,
                                  calendar=self.dataset.variables["time"].calendar)

        time_var = self.dataset.variables[time_var_name]
        time_index = np.where(time_var[:] == hours_since)[0]

        if time_index.size > 0:
            # this time was create before. Just store the 2D at this index
            self.logger.debug("Storing data at index ={} hrs={}".format(time_index[0], hours_since))
            self.dataset.variables[record_data_name][time_index[0], :, :] = record_data
        else:

            # get the next index in the time date array
            next_time_index, last_time = self.get_next_time_index(time_var_name=time_var_name)

            self.logger.debug("Adding new record at nxt={} lst={} rcd={} hrs={}"
                              "".format(next_time_index, last_time, record_date_time, hours_since))

            # for some reason we have to add the data 2 times, first at the current time index
            # (which is next -1) and then we already add data to the next time step (which is
            # nex_time_index). The next round the values added in this round will be overwritten by
            # the next values. This allows to read the data in paraview as transient data.
            self.dataset.variables[time_var_name][next_time_index - 1] = hours_since
            self.dataset.variables[record_data_name][next_time_index - 1, :, :] = record_data

            # add already the next time step as well!
            self.dataset.variables[time_var_name][next_time_index] = hours_since
            self.dataset.variables[record_data_name][next_time_index, :, :] = record_data

            mean_orig = np.mean(record_data)
            mean_previous = np.mean(self.dataset.variables[record_data_name][
                                    next_time_index - 1, :, :])
            self.logger.debug("mean prev prev={} mean now {}".format(mean_orig, mean_previous))

    def create_new_group(self, group_name):
        """
        Create a new group name

        Parameters
        ----------
        group_name : str
            Name of the group
        """

        group = self.dataset.createGroup(group_name)
        self.groups[group_name] = group
        self.add_dimension("time_" + group_name, group=group)
        # freq_in_hz = self.dataset.variables["freq_in_hz"][:]
        # direction_deg = self.dataset.variables["direction_deg"][:]
        # self.add_dimension("freq_in_hz", units="1/s", values=freq_in_hz, group=group)
        # self.add_dimension("direction_deg", units="deg", values=direction_deg, group=group)
        group.createVariable("psd_" + group_name, "f8", ("time_" + group_name, "freq_in_hz",
                                                         "direction_deg"),
                             zlib=True)


class LiftDynComponent(object):
    """
    Class to hold all the properties of a single liftdyn `N x M` component

    Attributes
    ----------
    header : :class:`OrderDict`
        Header information of the current RAO or response is stored in a dictionary with the
        following fields

        * TITLES : :obj:`list()` with all the titles
        * PROJECT : title of the project
        * SUBJECT : subject
        * DATE : date when the file was generated
        * JOBNUM : number of the job
        * DRANUM : number
        * LINES : :obj:`list` of strings storing all the LINES field in the liftdyn header
        * TEXVER : label of the vertical axis
        * TEXHOR : label of the vertical axis

    x_data : :class:`ndarray`
        Nx1 array with the data along the x-axis. Can be either frequencies or peak_periods
    frequencies : :class:`ndarray`
        Nx1 array with the frequencies along the x-axis
    peak_periods : :class:`ndarray`
        Nx1 array with the peak_periods along the x-axis
    directions : :class:`ndarray`
        Mx1 array with the directions in rad along the y-axis
    directions_deg : :class:`ndarray`
        Mx1 array with the directions in degrees along the y-axis
    data : :class:`ndarray`
        NxM complex array with the RAO or response amplitudes

    Notes
    -----
    * A LiftDyn component can be either a RAO or a response function.
    * One LiftDyn file may contain one or more RAO's or responses
    * The class is used by the main class :class:`LiftDynReader`
    """

    def __init__(self):
        self.logger = get_logger(__name__)

        # header dictionary
        self.header = OrderedDict(
            list([
                ("TITLES", list()),
                ("PROJECT", None),
                ("SUBJECT", None),
                ("DATE", None),
                ("JOBNUM", None),
                ("DRANUM", None),
                ("LINES", list()),
                ("TEXVER", None),
                ("TEXHOR", None)
            ])
        )
        # data field to store the numpy arrays of frequencies, directions, periods and complex data
        self.name = None
        self.x_data = None
        self.frequencies = None
        self.peak_periods = None
        self.directions = None
        self.directions_deg = None
        self.data = None

    def report(self):
        """
        Create a summary of the header of this component
        """

        for key, value in self.header.items():
            if key == "LINES":
                # do not report the LINES with the directions
                continue
            try:
                print("{:8s} {}".format(key, value.strip()))
            except AttributeError:
                # an attribute error indicates that value is a list
                for val in value:
                    print("{:8s} {}".format(key, val.strip()))

    def summarize(self):
        """Create a one-liner with information about this component"""
        first_item = self.data.flatten()[0]
        shape_str = "{}".format(self.data.shape)
        print("{:55s} : array shape {:10s}; (r, phi)[0] = ({:10.2e}, {:10.2e})"
              "".format(self.name, shape_str, abs(first_item), np.angle(first_item)))


class LiftDynReader(object):
    """ A reader class for reading all RAO's or responses from a Liftdyn output file

    Parameters
    ----------
    file_name: str
        Name of the liftdyn file to read
    name: str, optional
        Name to give to the data. Default  = None

    Examples
    --------
    In order to read a lift dyn RAO

    >>> lift = LiftDynReader(file_name="../data/hermod_11_deep_POI1-acc-RAO.stf_plt")

    The data can be summarized as

    >>> lift.make_report(short_summary=True)
    # of RAO's           : 6
    RAO_Acceleration_incl_horz_gravity_component_AX-g*sin   : array shape (250, 8)  ; (r, phi)[0] = (  3.63e-03,  -3.11e+00)
    RAO_Acceleration_incl_horz_gravity_component_AY+g*sin   : array shape (250, 8)  ; (r, phi)[0] = (  3.46e-06,  -1.52e+00)
    RAO_Acceleration_incl_horz_gravity_component_AZ         : array shape (250, 8)  ; (r, phi)[0] = (  1.00e-04,   3.14e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARX        : array shape (250, 8)  ; (r, phi)[0] = (  2.02e-09,   1.62e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARY        : array shape (250, 8)  ; (r, phi)[0] = (  2.12e-06,  -3.11e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARZ        : array shape (250, 8)  ; (r, phi)[0] = (  3.56e-09,  -2.22e+00)

    A plot of the components can be made with

    >>> fig, axis = lift.plot()

    Resampling the data to a different frequency and direction axis is easy. First define the new
    frequency and direction arrays based on the current directions and frequencies. Note that we
    have to pick the directions and frequencies in rad and rad/s, respectively.
    Use those new axis to resample the RAO data to the new mesh

    >>> new_dirs = np.linspace(lift.directions[0], lift.directions[-1], endpoint=True, num=32)
    >>> new_freq = np.linspace(lift.frequencies[0], lift.frequencies[-1], endpoint=True, num=500)
    >>> lift.resample_data(new_directions=new_dirs, new_frequencies=new_freq)
    >>> lift.make_report(short_summary=True)
    # of RAO's           : 6
    RAO_Acceleration_incl_horz_gravity_component_AX-g*sin   : array shape (500, 32) ; (r, phi)[0] = (  3.63e-03,  -3.11e+00)
    RAO_Acceleration_incl_horz_gravity_component_AY+g*sin   : array shape (500, 32) ; (r, phi)[0] = (  3.46e-06,  -1.52e+00)
    RAO_Acceleration_incl_horz_gravity_component_AZ         : array shape (500, 32) ; (r, phi)[0] = (  1.00e-04,   3.14e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARX        : array shape (500, 32) ; (r, phi)[0] = (  2.02e-09,   1.62e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARY        : array shape (500, 32) ; (r, phi)[0] = (  2.12e-06,  -3.11e+00)
    RAO_Acceleration_incl_horz_gravity_component_ARZ        : array shape (500, 32) ; (r, phi)[0] = (  3.56e-09,  -2.22e+00)

    In case that a correction for the forward speed need to be made the transformed RAO
    can be calculated and stored for a series of velocities over a given range. The velocities are
    defined in m/s. For instance, calculate the shifted RAO's s for the range 0 to 5 m/s at a
    spacing of 0.5 m/s:

    >>> lift.calculate_rao_per_velocity(n_velocity=11, min_velocity=0.0, max_velocity=5.0)

    At this point, the RAO for 10 descrete velocity are calculated. The RAO for a certain velocity can be made active
    with

    >>> speed_in_knots = 5.0
    >>> speed_in_meter_per_second = speed_in_knots * nautical_mile / 3600.0
    >>> print("Picking RAO with velocity nearest to {:.2f} m/s".format(speed_in_meter_per_second))
    Picking RAO with velocity nearest to 2.57 m/s
    >>> lift.pick_rao_for_speed(sailing_speed=speed_in_meter_per_second)
    >>> current_speed = lift.current_speed_in_meter_per_second
    >>> current_speed_in_knots = current_speed * 3600.0 / nautical_mile
    >>> print("Current velocity for RAO is : {:.2f} m/s ({:.2f} kn)".format(current_speed,
    ...                                                                     current_speed_in_knots))
    Current velocity for RAO is : 2.50 m/s (4.86 kn)

    The velocity of the current RAO is not exactly as requested as we have the RAO stored at a
    discrete interval. The error can be made smaller by choosing a higher value for *n_velocity*.
    The advantage of this method is that the RAO transformation only needs to be done one time at
    the beginning of a simulation. After that, the proper RAO can be quickly obtained by using the
    pick_rao_for_speed method.

    Notes
    -----

    See the python scripts and notebooks in the examples directory of this package for more
    information

    Attributes
    ----------

    dof_components : :obj:`list`
        List of all components data of the RAO  or responses
    dof_names : :obj:`OrderedDict`
        List of all components names of the  RAO  or responses
    x_label : str
        Name of the x-axis
    y_label : str
        Name of the x-axis
    z_labels : :obj:`list`
        List of all the names of the RAO components
    data : :obj:`ndarray`
        `PxNxM` array of the `P` RAO or response components, where each component is a `NxM` data
        array
    frequencies :  :obj:`ndarray`
        Nx1 array with the frequency axis in rad/s
    freq_in_hz : :obj:`ndarray`
        Nx1 array with the frequency axis in Hz
    peak_periods : :obj:`ndarray`
        Nx1 array with the peak period axis in s. The peak period axis is defined as 1/freq_in_hz
    directions : :obj:`ndarray`
        Mx1 array with the direction axis in rad
    directions_deg : :obj:`ndarray`
        Mx1 array with the direction axis in degrees
    is_rao : bool
        Flag to indicate wheather we have loaded a response function or RAO
    n_velocity : :obj:`int`
        Number of velocities to calculate the transformed RAO. Default to 1
    min_velocity : :obj:`float`
        Minimum velocity in the range. Defaults to 0
    max_velocity  : :obj:`float`
        Maximum velocity in the range. Defaults to 0
    velocity_range : :obj:`list`
        List of velocity on which we have calculated the shifted RAO. Default to np.array([0.0])
    current_speed_in_meter_per_second : :obj:`float`
        Current speed seen by the RAO
    data_vs_velocity : :obj:`list`
        List of the `n_velocity` shifted RAO's
    """

    def __init__(self, file_name, name=None):

        self.logger = get_logger(__name__)

        # analyse file name and check extension
        self.file_name = file_name
        self.name = name
        self.file_base, self.file_extension = os.path.splitext(self.file_name)
        if self.file_extension not in LIFT_DYN_FILE_EXTENSION:
            raise IOError("File type of file {} not of type {}".format(self.file_name,
                                                                       LIFT_DYN_FILE_EXTENSION))

        # initialise a list to hold all the lift dyn data per DOF
        self.dof_components = list()
        # store the names of the RAO for easier reference later
        self.dof_names = OrderedDict()

        self.x_label = None
        self.y_label = None
        self.z_labels = list()
        self.data = None  # significant double amplitude
        self.frequencies = None  # frequencies are in rad/s
        self.freq_in_hz = None
        self.directions = None
        self.directions_deg = None
        self.peak_periods = None
        self.is_rao = None

        self.n_velocity = None
        self.min_velocity = None
        self.max_velocity = None
        self.velocity_range = np.array([0.0])
        self.current_speed_in_meter_per_second = None
        self.data_vs_velocity = list()

        # read all the lift dyn lines
        self.read_lift_dyn()

        self.post_process()

    def read_lift_dyn(self):
        """
        Read routine for liftdyn files
        """
        read_next_rao = True  # keep reading RAO 2D planes until read_rao is set to false

        # empty list of all the lift dyn components
        self.dof_components = list()

        self.logger.debug("Opening file {}".format(self.file_name))
        with open(self.file_name, "r") as fp:
            while read_next_rao:
                dof_component = self.read_liftdyn_data(fp)
                if dof_component is None:
                    # no RAO was found by read_liftdyn. Close the file and lead the loop
                    read_next_rao = False
                else:
                    self.dof_components.append(dof_component)

                    # get the name of this component and store it in the dictionary with the proper
                    # index
                    name = dof_component.name
                    self.dof_names[name] = len(self.dof_components) - 1

    def read_liftdyn_data(self, fp):
        """
        Routine to read the next RAO 2D plane of the current file. One LiftDyn file may contain one
        or more RAO's or responses

        Parameters
        ----------
        fp:  IO buffer
            pointer to the current line in the file.

        Returns
        -------
        tuple
            tuple of 4 arguments: direction (1d array), frequencies (1d array) and RAO (2d array
            containing the complex amplitudes of the current RAO).
            The fourth and fifth arguments are the horizontal and vertical text labels
        """

        hor_axis_data = list()  # keep a list of list of frequencies in rad/s
        dir_in_deg = list()  # a list of directions in degrees
        complex_amplitudes = list()  # store the list of complex amplitude per direction in a list
        x_data_for_dir = list()  # store the list of frequencies for the current direction
        c_ampl_for_dir = list()  # store the list of complex amplitudes for the current direction

        # initialise the lift_dyn_line
        component = LiftDynComponent()

        # continue looping over the lines of the file point fp. Note that the position is kept by
        # leaving and reentering this routine, so we can continue to the next RAO component
        # (out of six) in this file
        for line in fp:

            # first check if we are not dealing with a header line. If so, extract the label and
            # store in header
            if bool(re.match("^TITLE", line)):
                component.header["TITLES"].append(re.sub("TITLE\d*\s+", "", line))
            elif bool(re.match("^PROJEC", line)):
                component.header["PROJECT"] = re.sub("PROJEC\s+", "", line)
            elif bool(re.match("^SUBJEC", line)):
                component.header["SUBJECT"] = re.sub("SUBJEC\s+", "", line)
            elif bool(re.match("^DATE", line)):
                component.header["DATE"] = re.sub("DATE\s+", "", line)
            elif bool(re.match("^JOBNUM", line)):
                component.header["JOBNUM"] = re.sub("JOBNUM\s+", "", line)
            elif bool(re.match("^DRANUM", line)):
                component.header["DRANUM"] = re.sub("DRANUM\s+", "", line)
            elif re.match("^LINE", line):
                # the section with the directions are stored in the lines starting with LINE with
                # the form
                # LINE1  0  0  2 Wave dir (deg) 0
                # with this regular expression, we can extract the last value which is the  angle in
                # degress
                match1 = re.match("(.*\s+)(\d+)", line)
                dir_in_deg.append(float(match1.group(2)))
                component.header["LINES"].append(re.sub("LINE\d*\s+", "", line))

            elif re.match("^TEXVER", line):
                component.header["TEXVER"] = re.sub("TEXVER\s+", "", line)
                # store a short version of this label with all the trash deleted as the name of this
                # component
                component.name = re.sub("\s+", "_",
                                        re.sub("\.", "",
                                               re.sub("\(.*", "",
                                                      component.header["TEXVER"])).strip())
            elif re.match("^TEXHOR", line):
                component.header["TEXHOR"] = re.sub("TEXHOR\s+", "", line)
            else:
                # all the other lines are consider data lines

                # the read_rao_values flag is set, which means we have passed the line starting
                # with TEXHOR
                if re.match("^END|^\*\*\*", line):
                    # we have found the last point of this direction. store the values and
                    # initialise the new list
                    hor_axis_data.append(x_data_for_dir)
                    complex_amplitudes.append(c_ampl_for_dir)
                    x_data_for_dir = list()
                    c_ampl_for_dir = list()

                    if re.match("^END", line):
                        # this was the last direction. Stop reading here and return to the main
                        # function
                        self.logger.debug("last point of RAO found. Quiting")
                        break
                else:
                    # we are still not at the end of the data line. Store the data
                    omega, data_mag, data_ang = re.split("\s+", line.strip())
                    data_ang_rad = np.deg2rad(float(data_ang))
                    x_data_for_dir.append(float(omega))
                    c_ampl_for_dir.append(float(data_mag) * np.exp(1j * data_ang_rad))

        if hor_axis_data:
            # we have read all direction of the current RAO 2D planes. Stored them as numpy array
            component.directions_deg = dir_in_deg
            component.directions = np.deg2rad(dir_in_deg)
            component.x_data = np.array(hor_axis_data[0])
            component.data = np.ascontiguousarray(np.array(complex_amplitudes).T)
        else:
            component = None

        return component

    def post_process(self):
        """
        just extract the data and set labels after reader
        """

        # collect all data into one array. Also assume that the direction and frequencies of each
        # data array is the same
        self.directions = self.dof_components[0].directions
        self.directions_deg = self.dof_components[0].directions_deg

        # get the properties of the axis from the first component
        component = self.dof_components[0]
        self.x_label = component.header["TEXHOR"]

        if bool(re.search("R.A.O", component.header["TEXVER"])):
            self.is_rao = True
        else:
            self.is_rao = False

        # get the y label from the lines description
        # the reg epx matches '0 0 2 Wave dir (deg) 0\n', we only want the 'Wave dir (deg)' part
        match2 = re.match("(\d\s+){3}(.*)\s\d+\s$", component.header["LINES"][0])
        if bool(match2):
            self.y_label = match2.group(2)

        if re.search("Period", component.header["TEXHOR"]):
            # the lift dyn data contain the peak period data. Store the period and frequency axis
            self.peak_periods = component.x_data
            self.frequencies = 1.0 / self.peak_periods
            self.freq_in_hz = self.frequencies / (2.0 * np.pi)
        else:
            self.frequencies = component.x_data
            self.freq_in_hz = self.frequencies / (2.0 * np.pi)
            self.peak_periods = 2.0 * np.pi / self.frequencies

        data_list = list()

        for component in self.dof_components:
            data_list.append(component.data)
            self.z_labels.append(component.header["TEXVER"])

        self.data = np.array(data_list)

    def get_data_mpm(self, f_factor=1.86, number_of_cycles=None, n_hours=3.0):
        """
        Calculate the single most probable maximum (SMPM) from the SDA (significant double
        amplitude) store in the 2D response array

        Parameters
        ----------
        f_factor : :obj:`float`, optional
            Multiplication factor to convert from sda to mpm. Default = 1.86
        number_of_cycles : :obj:`int`, optional
            Number of cycles _N_ used to calculate mpm. Default = None
        n_hours : :obj:`float`, optional
            Number of hours assumed to calculate the MPM

        Returns
        -------
        :obj:`float`
            Single amplitude most probable maximim

        Notes
        -----
        Two methods to calculate the MPM can be used.

        * In case f_factor is given (which is the default situation), the following equation is
          used to calculate the single sided most probable maximum :math:`A_{smpm}`

          .. math::

                A_{smpm} = \\frac{1}{2} f A_{sda}

          where :math:`A_{sda}` is the significant double amplitude and :math:`f` is the `f_factor`
          value defaulting to 1.86 (i.e. a 3h storm with a peak period of 10 s is assumed)

        * In case f_factor is None, the following equation is used to calculate the mpm

          .. math::

                A_{smpm} = A_{sda} * \sqrt{\ln (N) / 2 },

          with :math:`N` the number of wave passed in a given period.  The number of waves can
          explicitly be passed via the input argument `number_of_cycles` or, in case
          `number_of_cycles` is None, can be calculated with

          .. math::

                N = n_{hours} * 3600 / T_p

          where :math:`n_{hours}` the number of hours of a storm and :math:`T_p`  the assumed wave
          peak period. For the peak period, the `frequency` or `peak_period` axis of the response
          function is used

        Returns
        -------
        ndarray
            2D data array with the most probable maximum
        """

        if f_factor is None:
            if number_of_cycles is None:
                # base number of cycles on Tp value and time period in hours
                number_of_cycles = (n_hours * 3600 / self.peak_periods).reshape(
                    self.peak_periods.shape[0], 1)
            a_mpm = self.data * np.sqrt(np.log(number_of_cycles) / 2.0)
        else:
            # by default, this conversion is taken (same as envieview)
            a_mpm = self.data * 0.5 * f_factor

        return a_mpm

    def get_component_by_name(self, name):
        """ Get the data of the component _name_ from the list of RAO's or responses

        Parameters
        ----------
        name : str
            Name of the component to return

        Returns
        -------
        ndarray
            `NxM` data array of the component `name`

        Notes
        -----
        The components of all the RAO's or responses stored in a 3D numpy array which means that we
        have to get them with an index. However, each index is relate to a name as well which is
        stored in the attributes field `dof_name`. This routine return the data based on the name
        """
        i_comp = self.dof_names[name]
        return self.dof_components[i_comp]

    def make_report(self, short_summary=False):
        """
        Loop over all RAO or reponse components stored in this class and plot the information to
        screen
        """

        fmt = "{:20} : {}"
        if self.is_rao:
            component_type = "RAO's"
        else:
            component_type = "Responses"
        print(fmt.format("# of {}".format(component_type), len(self.dof_components)))
        for cnt, (name, i_component) in enumerate(self.dof_names.items()):
            component = self.get_component_by_name(name)
            if short_summary:
                component.summarize()
            else:
                print("-- component {} : {}  ------------------ ".format(cnt, name))
                try:
                    component.report()
                except AttributeError as err:
                    self.logger.debug(err)

    def plot(self, n_cols=2, n_rows=None, plot_index_selection=[], sub_plot_size=None, figsize=None,
             x_axis_type="frequency", index_title_axes=0, plot_title=None, x_plot_title=0.05,
             y_plot_title=0.99, delta_y_titles=0.03, max_title_lines=None, title_font_size=10,
             x_axis_lim=None, color_map=cc.m_rainbow):
        """Create a plot of all components

        Parameters
        ----------
        n_cols : int, optional
            Number of columns to create. Defaults = 2, but follows from n_rows if None
        n_rows : int, optional
            Number of rows to create. Defaults = None, Follows from n_cols if None
        plot_index_selection : list of int, optional
            Make a selection of the components by their indices. Default = [], which means all
            components are plotted If only one plot is selected, n_cols = 1 and n_rows = 1
        sub_plot_size: tuple
            x and y size of each subplot. The total figure size follows from n_rows and n_cols.
            Default is None, so figure size is not imposed
        figsize: tuple
            x and y size of the total figure. Default is None, so figure size is not imposed
        x_axis_type: {"frequency", "period"}, optional
            quantity to use along the x-axis. Either frequency [Hz] or period [s].
            Default = "frequency"
        plot_title: str
            Title to put in th figure. If None, use the titles found in the liftdyn file
        index_title_axes : int, optional
            index of the subplot to use to plot the title. Default = 0 (top right plot). If set to
            None, do not plot the title. The title is based on all the title lines found in the
            liftdyn file, or explicity passed
        x_plot_title: float, optional
            x position (as a fraction of the sub plot index_title_axis). Default = 0.0
        y_plot_title: float, optional
            y position (as a fraction of the sub plot index_title_axis). Default = 1.1
        delta_y_titles: float, optional
            spacing between the lines of thet title lines. Default = 0.05
        max_title_lines: int or None
            Maxinmum number of title lines to plot. Default = None, ie. plot all
        title_font_size: int
            Size of the title font
        x_axis_lim: tuple or None
            The limits of the x axis. Default = None, so no limits are imposed


        Returns
        -------
        tuple (fig, axis)
            Handle to the figure and the axis
        """

        if not plot_index_selection:
            # if the plot_index_selection is empty, just plot all dof_components
            n_sub_plots = len(self.dof_components)
        else:
            n_sub_plots = len(plot_index_selection)

        if n_sub_plots == 1:
            n_cols = 1
            n_rows = 1
        else:
            if n_rows is None:
                n_rows = n_sub_plots // n_cols + (n_sub_plots % n_cols > 0)
            elif n_cols is None:
                n_cols = n_sub_plots // n_rows + (n_sub_plots % n_rows > 0)

        if figsize is not None:
            size = figsize
        elif sub_plot_size is not None:
            size = (sub_plot_size[0] * n_cols, sub_plot_size[1] * n_rows)
        else:
            size = None

        fig, axis = plt.subplots(nrows=n_rows, ncols=n_cols, sharex=True, sharey=True, figsize=size)

        axis = sub_plot_axis_to_2d(axis, n_rows=n_rows, n_cols=n_cols)

        # get the frequency and direction axis
        if x_axis_type == "frequency":
            data_x = np.array(self.freq_in_hz)
            x_label = "Frequency [Hz]"
        elif x_axis_type == "period":
            data_x = np.array(self.peak_periods)
            x_label = "Peak Period [s]"
        else:
            raise AssertionError("'x_axis_type' must be either 'frequency' or 'period'. '{}' given"
                                 "".format(x_axis_type))

        direction_deg = np.rad2deg(np.append(self.directions, [self.directions[0] + 2 * pi]))

        window_title = ""

        plot_index = 0
        for j_col in range(n_cols):
            for i_row in range(n_rows):
                # set a short reference to the current axis
                ax = axis[i_row][j_col]

                # set the label frequency along the direction axis
                ax.set_ylim(0, 360.1)
                ax.set_yticks(range(0, 361, 90))

                if x_axis_lim is not None:
                    ax.set_xlim(x_axis_lim)

                if plot_index_selection:
                    try:
                        component_index = plot_index_selection[plot_index]
                    except IndexError:
                        self.logger.warning("plot_index_selection list out of bounds. Skipping {}"
                                            "".format(plot_index))
                        continue
                else:
                    component_index = plot_index

                # select the data for the current RAO
                try:
                    component = self.dof_components[component_index]
                except IndexError:
                    self.logger.warning("Component_index list out of bounds. Skipping {}"
                                        "".format(component_index))
                    continue

                # the long title is stored in the header in the TEXVER key field. Get it here
                title_long = component.header["TEXVER"]

                # Use a regular expression to extract a short part of the title + the units
                match = re.search("(A[RXYZ].*)\s+\((.*)\).*", title_long)
                if bool(match):
                    title_short = match.group(1)
                    units = match.group(2)
                else:
                    title_short = title_long
                    units = None

                self.logger.debug("Plotting {}  with units".format(title_short, units))

                # get the data magnitude and plot it
                data_mag = abs(component.data)

                # the data is periodic in the direction axis. Enforce that to make the plot nicely
                # round from 0 ~ 360
                data_first_row = data_mag[:, 0].reshape(data_mag.shape[0], 1)
                data_mag = np.hstack((data_mag, data_first_row))

                # finally create the contour plot wit the RAO magnitude
                cs = ax.contourf(data_x, direction_deg, data_mag.T, cmap=color_map, zorder=0)

                # set the axis labels
                if i_row == n_rows - 1:
                    ax.set_xlabel(x_label)
                if j_col == 0:
                    ax.set_ylabel(self.y_label)

                # create a contour bar
                cbar = fig.colorbar(cs, ax=ax)
                cbar.ax.set_ylabel("{} [{}]".format(title_short, units))

                # update the counter for the next plot
                plot_index += 1

        yp = y_plot_title
        if plot_title is None:
            for i_tit, title in enumerate(component.header["TITLES"]):
                if max_title_lines is not None and i_tit == max_title_lines:
                    break
                plt.figtext(x_plot_title, yp, title, fontsize=title_font_size,
                            verticalalignment="top")
                yp -= delta_y_titles
                window_title += title
        else:
            plt.figtext(x_plot_title, yp, plot_title, fontsize=title_font_size,
                        verticalalignment="top")
            window_title = plot_title

        fig.canvas.set_window_title(window_title)

        return fig, axis

    def resample_data(self, new_directions, new_frequencies):
        """Resample all `P` components of size `NxM` to a new mesh `N'xM'`

        Parameters
        ----------
        new_directions : ndarray
            M'x1 array with new directions in radians
        new_frequencies : ndarray
            N'x1 array with new frequencies in rad/s

        Notes
        -----
        * In general, when a RAO has been read into memory we need to interpolate it on a new mesh
          in order to be able to multiply it with a spectral density function. The method take care
          of the resampling of the data.
        * Special care it taken to make the data cyclic in the direction axis as we know the data is
          cyclic
        """

        self.logger.debug("User double direction")
        # get the extended direction axis of both the original and the new direction axis.
        direction_2 = np.hstack((self.directions, self.directions + 2 * np.pi))
        new_dirs_2 = np.hstack((new_directions, new_directions + 2 * np.pi))

        # the size of the double direction axis
        n_new_dirs = new_directions.size

        # a list to keep the resampled raos per dof
        rao_list = list()

        # loop over the 6 dof
        for i_dof in range(self.data.shape[0]):

            # interpolate for the real and the image in the same way, so loop over real and image
            rao_complex = dict()
            for part in ["real", "imag"]:
                # take the current part of the complex number: real or imag and store in data
                if part == "real":
                    data = self.data[i_dof].real
                else:
                    data = self.data[i_dof].imag

                # double the data in the theta direction axis as explained above
                data = np.hstack((data, data))

                # interpolate the 2D plane with the doubled direction axis
                f_inter = interp2d(direction_2, self.frequencies, data)
                data_resampled = f_inter(new_dirs_2, new_frequencies)

                # collect the real and imaginary part, only take the first half part of the
                # direction axis
                rao_complex[part] = data_resampled[:, :n_new_dirs]

            # create new complex array from the two real ones (with the real and imaginary part of
            # the RAO)
            new_rao = rao_complex["real"] + 1j * rao_complex["imag"]

            # append to the list
            rao_list.append(new_rao)

        # update the data and the direction and frequencies
        self.directions = new_directions
        self.directions_deg = np.rad2deg(self.directions)
        self.frequencies = new_frequencies
        self.freq_in_hz = self.frequencies / (2 * np.pi)
        self.peak_periods = 1 / self.freq_in_hz
        self.data = np.array(rao_list)
        for i_dof, dof_comp in enumerate(self.dof_components):
            dof_comp.data = rao_list[i_dof]
            dof_comp.directions = self.directions
            dof_comp.directions_deg = self.directions_deg
            dof_comp.frequencies = self.frequencies
            dof_comp.freq_in_hz = self.freq_in_hz
            dof_comp.peak_periods = self.peak_periods

    def calculate_rao_per_velocity(self, n_velocity=20, min_velocity=0.0, max_velocity=5.0):
        """
        Shift the rao per velocity component

        Parameters
        ----------
        n_velocity :  int, optional
            Number of velocities to shift for. Default = 20
        min_velocity :  float,  optional
            Minimum velocity in m/s to shift for. Default = 0 m/s
        max_velocity : float,  optional
            Maximum velocity in m/s to shift for. Default = 5 m/s

        Notes
        -----
        * After the zero speed RAO has been read into memory, in this function the velocity shifts
          of the zero speed RAO are applied for `n_velocity` velocities in the range range
          min_velocity:max_velocity. The shifted RAO's are stored in a list `data_vs_velocity`
        * The velocity shifts of the RAO only need to be calculate one time. After that, the
          `pick_rao_for_speed` method can be used to replace the current RAO data stored in the
          `data` field with the RAO for the nears velocity stored in `current_velocity`
        """

        self.n_velocity = n_velocity
        self.min_velocity = min_velocity
        self.max_velocity = max_velocity  # maximum velocity in m/s, we have a range up to 8 knots
        if self.n_velocity > 0:
            self.velocity_range = np.linspace(self.min_velocity, self.max_velocity, self.n_velocity,
                                              endpoint=True)

        for velocity in self.velocity_range:
            # create a new roa per for based on the original one. Set to zero and then put the last
            # row with the unity back to one
            new_data = self.data.copy()

            # loop over the directions of the 2D RAO
            for i_rao in range(self.data.shape[0]):

                data_2d = self.data[i_rao]

                for i_dir, direction in enumerate(self.directions):
                    # the new frequencies for this velocity in m/s. Note that these omega_new are
                    # the 'true' frequencies in the fixed domain, the frequencies of the RAO are the
                    # encountered frequencies of the moving ref the omega_new is not equidistant
                    omega_new = omega_e_vs_omega(self.frequencies, velocity * np.cos(direction))

                    f_inter = interp1d(self.frequencies, data_2d[:, i_dir], bounds_error=False,
                                       fill_value=complex(0, 0))

                    # obtain the RAO data as a function of the true frequencies and interpolate on a
                    # regular grid
                    new_data[i_rao, :, i_dir] = f_inter(omega_new)

            self.data_vs_velocity.append(new_data)

    def pick_rao_for_speed(self, sailing_speed=0.0):
        """
        Make the RAO for speed `sailing_seed` (m/s) active

        Parameters
        ----------
        sailing_speed: float
            speed in m/s to pick the RAO for

        Notes
        -----

        By calling this routine the current rao data field is replace with the rao belonging to the
        velocity in meter per second
        """

        # get index of velocity
        i_max = get_nearest_index(self.velocity_range, sailing_speed)

        self.current_speed_in_meter_per_second = self.velocity_range[i_max]

        self.logger.debug("picking rao {} for speed {} ".format(i_max, sailing_speed))

        # set the rao data to the current velocity
        self.data = self.data_vs_velocity[i_max]

        # also put the the stored velocity to the component data
        for cnt, component in enumerate(self.dof_components):
            component.data = self.data[cnt]


class WaveSpectralDensity2DRecord(object):
    """Class to read and store one spectrum density record from the Spectrum2D or Waverider data file.
    
    Parameters
    ----------
    n_frequencies:  int
        The number of frequencies bins used for the regular mesh. Default = 123
    n_directions:  int
        The number of direction used for the regular mesh. Default = 180
    min_freq_in_hz: float, optional
        Minimum frequency in Hz. Default = 0.02 Hz
    max_freq_in_hz: float, optional
        Maximum frequency in Hz. Default = 2.0 Hz
    min_dir_in_deg: float, optional
        Min direction in degrees. Default = 0 deg
    max_dir_in_deg: float, optional
        Min direction in degrees. Default = 360 deg (not closed interval is used
    freq_in_hz: ndarray, optional
        If this arrays with frequencies in hz is passed, this will be taking leading: the
        n_frequencies, min_freq_in_hz, and max_freq_in_hz will be imposed based on this array.
        Default is None. Note that the frequencies must be in Hz
    directions_deg: ndarray, optional
        If this arrays with directions in deg is passed, this will be taking leading: the
        *n_directions*, *min_dir_in_deg*, and *max_dir_in_deg* will be imposed based on this array.
        Default is None. Note that the direction must be in degrees.
    netcdf: :obj:`NetCDFInfo`
        object holding all the information of the NetCDF file which can be used to retrieve or store
        the data from/to
    hours_since: float
        Numerical representation of the date/time of the current spectral record in # hours since
        0001-00-00
    date_time: :obj:`date_time`
        Date/time of the current spectral record

    Attributes
    ----------
    record_data_name : str
        Name of the data of this record. Defaults to 'psd'
    psd_2d : ndarray
        A `n_frequencies` x `n_diretions` array holding the power spectral density values
    psd_vs_ang : ndarray
        A `n_directions` array  holding the psd vs the direction (ang) on a regular mesh. Note that
        this is the 1D psd, so a integral over the frequency axis has been done.
    psd_vs_freq : ndarray
        A `n_frequencies` array holdnig the psd vs the frequency (Hz) on a regular mesh. Now the
        integral over the direction axis has been done
    psd_vs_freq_n :
        A `n_frequencies_nu` array holding the psd vs the frequency (Hz) on a non-uniform mesh.
        Directly obtained from the spectral data file
    freq_in_hz : ndarray
        A `n_frequencies` array holding the frequency of the uniform mesh in Hz
    directions_deg : ndarray
        A  `n_directions` array holding the directions in degrees
    header_df : DataFrame
        A pandas DataFrame  containing the header of this spectral records with the Hs Tz, etc as
        obtained from the 2d spectrum. The following quantities are stored in the columns of the
        data frame

            - Hs : Significant wave height
            - Tp : peak_period
            - Tp2 : mean_period_sqr
            - Tm : mean_period
            - Theta0 : theta_zero
            - HsTp2 : hs_tpspec_sqr

        See the `set_psd_properties` method for a more detailed description on how the quantities
        are obtained from the 2D spectrum

    Notes
    -----
    * Fugro has put the spectra as a power energy per bin on a non-uniform mesh. The spectral record
      is holding the power spectral density (psd), .ie. the spectral energy divided by the bin size
      of the mesh
    * Frequencies and directions are put in Hz and degrees
    """

    def __init__(self,
                 n_frequencies=123,
                 n_directions=180,
                 min_dir_in_deg=0,
                 max_dir_in_deg=360,
                 min_freq_in_hz=0.02,
                 max_freq_in_hz=2.0,
                 freq_in_hz=None,
                 directions_deg=None,
                 netcdf=None,
                 record_data_name=None,
                 hours_since=None,
                 date_time=None,
                 show_read_time=True
                 ):

        self.logger = get_logger(__name__)
        if freq_in_hz is not None:
            # if the frequencies are passed as an 1D array in Hz, use these values and base the
            # min/max and number of points on the array values
            self.freq_in_hz = freq_in_hz
            self.min_freq_in_hz = self.freq_in_hz.min()
            self.max_freq_in_hz = self.freq_in_hz.max()
            self.n_frequencies = self.freq_in_hz.size
        else:
            # not array is based, so create the array here based on the min/max/number of points
            self.min_freq_in_hz = min_freq_in_hz
            self.max_freq_in_hz = max_freq_in_hz
            self.n_frequencies = n_frequencies
            self.freq_in_hz = np.linspace(self.min_freq_in_hz, self.max_freq_in_hz,
                                          self.n_frequencies)
        if directions_deg is not None:

            self.directions_deg = directions_deg
            self.min_dir_in_deg = self.directions_deg.min()
            self.max_dir_in_deg = self.directions_deg.max()
            self.n_directions = self.directions_deg.size
        else:
            self.min_dir_in_deg = min_dir_in_deg
            self.max_dir_in_deg = max_dir_in_deg
            self.n_directions = n_directions
            self.directions_deg = np.linspace(self.min_dir_in_deg, self.max_dir_in_deg,
                                              self.n_directions, endpoint=False)

            # calculate the frequencies in rad/s and directions in rad as well.
        self.frequencies = 2 * np.pi * self.freq_in_hz  # using eq omega = 2 * pi / T = 2 * pi * f
        self.directions = np.deg2rad(self.directions_deg)

        dd, ff = np.meshgrid(self.directions_deg, self.freq_in_hz)
        self.freq_in_hz_2d = ff
        self.directions_deg_2d = dd

        self.hours_since = hours_since
        self.date_time = date_time

        self.netcdf = netcdf

        self.record_data_name = record_data_name

        # create the data arrays to store the data
        self.psd_2d = np.zeros((self.n_frequencies, self.n_directions))
        self.psd_vs_ang = np.zeros(self.n_directions)
        self.psd_vs_freq = np.zeros(self.n_frequencies)
        self.psd_vs_freq_n = None

        self.polar_radius_frequency = np.zeros(self.n_frequencies)
        self.polar_radius_period = np.zeros(self.n_frequencies)
        self.polar_theta = np.zeros(self.n_directions)

        self.delta_freq_in_hz = None
        self.delta_directions_deg = None
        self.delta_area_per_bin = None  # delta area in deg x 1/s  (ie. based on directions in deg)
        self.freq_in_hz_2d = None  # frequencies on 2d mesh in Hz
        self.directions_deg_2d = None  # direcitons on 2d mesh in deg

        self.frequencies_2d = None  # stores the frequencies on 2d mesh in rad/s
        self.directions_2d = None  # stores direction on 2d mesh in rad
        self.peak_period_2d = None  # stores periiods on 2d mesh in rad

        # the head_df is going to contain the header of this spectral records with the Hs Tz, etc
        self.header_df = None

    def report(self):
        """
        Make a short report of the current record
        """

        self.logger.info("{} : DateTime {} header_df =\n{}".format(self.__class__.__name__,
                                                                   self.date_time,
                                                                   self.header_df.info))

    def plot(self,
             figsize=None,
             r_axis_type="frequency",
             plot_title=None,
             x_plot_title=0.05,
             y_plot_title=0.99,
             min_data_value=0,
             max_data_value=None,
             number_of_contour_levels=10,
             title_font_size=10,
             r_axis_lim=None,
             x_time_label=0.05,
             y_time_label=0.95,
             x_hs_label=0.05,
             y_hs_label=0.89,
             make_data_cyclic=False,
             polar_projection=True,
             color_map=cc.m_rainbow,
             zorder=0
             ):
        """ Create a polar plot of the current spectral data

        Parameters
        ----------
        figsize: tuple
            x and y size of the total figure. Default is None, so figure size is not imposed
        r_axis_type: {"frequency", "period"}, optional
            quantity to use along the radial axis. Either frequency [Hz] or period [s].
            Default = "frequency"
        plot_title: str, optional
            Title to put in th figure. If None, use the titles found in the liftdyn file
        x_plot_title: float, optional
            x position (as a fraction of the sub plot index_title_axis). Default = 0.0
        y_plot_title: float, optional
            y position (as a fraction of the sub plot index_title_axis). Default = 1.1
        delta_y_titles: float, optional
            spacing between the lines of thet title lines. Default = 0.05
        max_title_lines: int or None
            Maxinmum number of title lines to plot. Default = None, ie. plot all
        title_font_size: int
            Size of the title font
        r_axis_lim: tuple or None
            The limits of the x axis. Default = None, so no limits are imposed
        zorder:  int, optional
            Position of the contour plot. Default = 0, meaning that the contours are placed at the
            bottom and the grid and labels are visible

        Returns
        -------
        tuple (fig, axis)
            Handle to the figure and the axis
        """

        if figsize is not None:
            size = figsize
        else:
            size = None

        if polar_projection:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size,
                                   subplot_kw=dict(projection="polar"))
            ax.set_theta_zero_location("N")
            ax.set_theta_direction(-1)
        else:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size)

        if r_axis_lim is not None:
            # the radial limits are but on the ylim
            ax.set_ylim(r_axis_lim)

        if r_axis_type == "frequency":
            x_label = "Frequency [Hz]"
        elif r_axis_type == "period":
            x_label = "Peak Period [s]"
        else:
            raise AssertionError("r_axis_type can only be frequency or period. Found {}"
                                 "".format(r_axis_type))

        if make_data_cyclic:
            data_x_2d = make_2d_array_cyclic(self.freq_in_hz_2d)
            data_y_2d = make_2d_array_cyclic(self.directions_2d, add_constant=2 * np.pi)
            psd_2d = make_2d_array_cyclic(self.psd_2d)
        else:
            data_x_2d = self.freq_in_hz_2d
            data_y_2d = self.directions_2d
            psd_2d = self.psd_2d

        if min_data_value is None:
            v_min = np.nanmin(psd_2d)
        else:
            v_min = min_data_value

        if max_data_value is None:
            v_max = np.nanmax(psd_2d)
        else:
            v_max = max_data_value

        # set the contour levels belonging to this subplot
        if min_data_value is None and min_data_value is None:
            # if both limits where not given, assume that we want matplotlib decide on the limits so 
            # set levels=None
            levels = None
        else:
            levels = linspace(v_min, v_max, number_of_contour_levels + 1, endpoint=True)

        time_label = self.date_time.strftime("%Y-%m-%d %H:%M")

        # finally create the contour plot wit the RAO magnitude
        cs = ax.contourf(data_y_2d, data_x_2d, psd_2d, cmap=color_map, levels=levels, zorder=zorder)
        if levels is not None:
            cs.cmap.set_under("k")
            cs.cmap.set_over("k")
            cs.set_clim(v_min, v_max)

        if time_label is not None:
            txt = plt.figtext(x_time_label, y_time_label, time_label, fontsize=title_font_size,
                              verticalalignment="top")

        hs_txt = plt.figtext(x_hs_label, y_hs_label,
                             "Hs = {:5.2f} m".format(self.header_df.ix[self.date_time, "Hs"]),
                             fontsize=title_font_size, verticalalignment="top")

        # set the axis labels
        ax.set_xlabel(x_label)

        if polar_projection:
            ax.set_rlabel_position(180)

        if r_axis_type == "period":
            label_values = ax.get_yticks()
            label_strings = []
            for label in label_values:
                try:
                    label_strings.append("{:.1f}".format(1 / float(label)))
                except (ValueError, ZeroDivisionError):
                    pass

            ax.set_yticks(label_values)
            ax.set_yticklabels(label_strings)

        # create a contour bar
        cbar = fig.colorbar(cs, ax=ax)
        cbar.ax.set_ylabel("{} [{}]".format("PSD", "m2s"))

        if plot_title is not None:
            plt.figtext(x_plot_title, y_plot_title, plot_title, fontsize=title_font_size,
                        verticalalignment="top")
            window_title = plot_title
            fig.canvas.set_window_title(window_title)

        return fig, ax

    def get_time_index_if_available(self):
        """
        See if a record is available with the current time hours_since
        :return: integer with tht index in the netcdf file or None if not available
        """

        time_var = self.netcdf.dataset.variables["time"][:]
        time_index_int = None
        if self.hours_since is not None:
            time_index = np.where(time_var == self.hours_since)[0]
            if time_index.size > 0:
                time_index_int = time_index[0]

        return time_index_int

    def read_data_from_netcdf(self):

        self.date_time = nc.num2date(self.hours_since,
                                     units=self.netcdf.dataset.variables["time"].units,
                                     calendar=self.netcdf.dataset.variables["time"].calendar)

        self.logger.debug("Get record at time {} {}".format(self.hours_since, self.date_time))

        time_index = self.get_time_index_if_available()

        if time_index is not None:
            # this time was create before. Just store the 2D at this index
            self.logger.debug("Storing data at index ={} hrs={}".format(time_index,
                                                                        self.hours_since))
            self.psd_2d = self.netcdf.dataset.variables[self.record_data_name][time_index, :, :]
            self.freq_in_hz = self.netcdf.dataset.variables["freq_in_hz"][:]
            self.directions_deg = self.netcdf.dataset.variables["direction_deg"][:]
        else:
            self.logger.debug("Could not find data belongin to time ={} hrs={}"
                              "".format(time_index, self.hours_since))

    def create_netcdf_frequency_and_direction_dimensions(self):

        # in case we have passe the netcdf object, initialise the dimensions in case they are still
        # empty
        if self.netcdf is not None:
            if "freq_in_hz" not in list(self.netcdf.dataset.dimensions.keys()):
                self.logger.debug("Creating NETCDF frequency dimension")
                self.netcdf.add_dimension("freq_in_hz", units="1/s", values=self.freq_in_hz)
            if "direction_deg" not in list(self.netcdf.dataset.dimensions.keys()):
                self.logger.debug("Creating direction dimension")
                self.netcdf.add_dimension("direction_deg", units="deg", values=self.directions_deg)

    def set_delta_grid(self):
        """
        get the deltas for the mesh. Assumes a uniform mesh
        """
        self.delta_directions_deg = np.diff(self.directions_deg)[0]
        self.delta_freq_in_hz = np.diff(self.freq_in_hz)[0]

        self.delta_area_per_bin = self.delta_directions_deg * self.delta_freq_in_hz
        dd, ff = np.meshgrid(self.directions_deg, self.freq_in_hz)
        self.freq_in_hz_2d = ff
        self.directions_deg_2d = dd

        self.directions = np.deg2rad(self.directions_deg)
        dd_r, pp = np.meshgrid(self.directions, 1 / self.freq_in_hz)
        self.directions_2d = dd_r
        self.peak_period_2d = pp

    def set_psd_properties(self, debug_plot=False):
        """ Obtain the sea state properties such as Hs/Tp  from the 2D spetrum

        Parameters
        ----------
        debug_plot :  bool
            If true make a plot to show the 2D spectrum with the peak location as found by the
            function. For debugging and check-printing only

        Notes
        -----
        The following quantities are obtained from the spectrum and stored in the `header_df` field
        of the object

        * variance: The variance :math:`\sigma^2` of the Power Spectral Density (PSD) defined as

          .. math::

              \sigma^2 = \int S(f, \\theta) d f d \\theta

          where :math:`\sigma` is the  standard deviation, :math:`S(f, \\theta)` is the PSD in
          :math:`m^2s` as stored in `psd_2d`, :math:`f` is the frequency in Hz (`freq_in_hz`),
          :math:`\\theta` is the direction in degrees (`direction_deg`) and :math:`d f d\\theta` is
          the bin size stored in `delta_area_per_bin`.

        * `Hs`: The significant wave height calculated as

          .. math::

                H_s = 4 \sigma

        * `f_mean` : the mean frequency, i.e.

          .. math::

              f_m = \int f S(f, \\theta) d f d \\theta

        * `f_peak` : the peak frequency, i.e. the frequency of the location of the maximum value in
          the 2d spectrum

        * `Theta0` : the direction of to the location of the maximum value in the 2D spectrum psd_2d

        * `Tp` : the peak period defined as 1 / `f_peak`.

        * `HsTp2` : The spectral definition of HsTp2 defined as

          .. math::

               H_s T^2_p = 4 \sqrt{\int S(f,\\theta) f^{-4} d\\theta df}

        * `Tp_spec` : The period obtained from the spectral `HsTp2` definition as `sqrt(HsTp2/Hs)`


        """

        variance = self.psd_2d.sum() * self.delta_area_per_bin
        wave_height = 4 * np.sqrt(variance)
        i_max, j_max = np.unravel_index(self.psd_2d.argmax(), self.psd_2d.shape)
        f_peak = self.freq_in_hz[i_max]
        peak_period = 1.0 / f_peak
        f_mean = np.sum(self.psd_2d * self.freq_in_hz_2d * self.delta_area_per_bin) / variance
        mean_period = 1.0 / f_mean
        theta_zero = self.directions_deg[j_max]
        hs_tp_sqr_true = wave_height * peak_period ** 2
        # THE HsTp2 definition as used within HMC
        hs_tp_sqr_spec = 4 * np.sqrt(np.sum((self.psd_2d / self.freq_in_hz_2d ** 4) *
                                            self.delta_area_per_bin))
        tp_spec = np.sqrt(hs_tp_sqr_spec / wave_height)

        if self.header_df is None:
            self.header_df = pd.DataFrame(index=pd.DatetimeIndex([self.date_time]))

        self.header_df.ix[self.date_time, "variance"] = variance
        self.header_df.ix[self.date_time, "Hs"] = wave_height
        self.header_df.ix[self.date_time, "Tp"] = peak_period
        self.header_df.ix[self.date_time, "Tm"] = mean_period
        self.header_df.ix[self.date_time, "Theta0"] = theta_zero
        self.header_df.ix[self.date_time, "HsTp2true"] = hs_tp_sqr_true
        self.header_df.ix[self.date_time, "HsTp2"] = hs_tp_sqr_spec
        self.header_df.ix[self.date_time, "Tp_spec"] = tp_spec

        if debug_plot:
            # the sea_state_db contains all loaded spectrum time series (for multiple forecasts).
            # Select the first
            fig, axis = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True)
            # xi, yi = np.meshgrid(spectrum.freq_in_hz, spectrum.directions_deg)
            # rxi, ryi = np.meshgrid(rao_db.freq_in_hz, rao_db.directions_deg)
            axis.contourf(self.directions_deg, self.freq_in_hz, self.psd_2d, interpolation='none')
            axis.scatter(theta_zero, f_peak, marker='o', s=20, zorder=10)
            axis.set_title("Hs={:.1f} Tp={:.1f} fp={:.1f} theta={:.1f} hstp2={:.1f}"
                           "".format(wave_height, peak_period, f_peak, theta_zero, hs_tp_sqr_spec))
            plt.ioff()
            plt.show()

    def get_psd_2d_in_omega_dir_rad_domain(self):
        """
        Covert PSD from frequency_in_hz (Hz) - direction_deg (deg) domain to frequency (rad/s)
        direction (rad) domain

        Returns
        -------
        ndarray:
            Scaled PSD in the frequency (rad/s)-direction (rad) domain. The returned units are
            rad ^2 / s
        """

        return self.psd_2d * 360 / (2 * pi) ** 2


class WaveSpectralComponent2DRecord(WaveSpectralDensity2DRecord):
    """
    A Reader class to turn a spectral component file with Hs, Tp, and direction for wind and swell
    into a 2D spectrum

    Parameters
    ----------
    current_date_time : date_time, optional
    current_sea_state_values : DataFrame
        a DataFrame row with the sea state vales for Hs, Tp, Theta0 and heading
    n_frequencies : int, optional
        Number of bins on the frequency axis. Default = 180
    n_directions : int, optional
        Number of bins on the direction axis. Default = 123
    min_dir_in_deg: float, optional
        Minimum value on direction range. Default = 0
    max_dir_in_deg : float, optional
        Maximum value on direction range in degrees. Default = 360
    min_freq_in_hz : float, optional
        Minimum value on frequency range in Hz. Default = 0.01
    max_freq_in_hz : float, optional
        Maximum value on frequency range in Hz. Default = 2.5
    freq_in_hz: ndarray, optional
        If this arrays with frequencies in hz is passed, this will be taking leading: the
        n_frequencies, min_freq_in_hz, and max_freq_in_hz will be imposed based on this array.
        Default is None. Note that the frequencies must be in Hz
    directions_deg: ndarray, optional
        If this arrays with directions in deg is passed, this will be taking leading: the
        *n_directions*, *min_dir_in_deg*, and *max_dir_in_deg* will be imposed based on this array.
        Default is None. Note that the direction must be in degrees.
    data_base_name: str, optional
        Name for the data base to store. Default = None
    netcdf: :obj:`NetCDFInfo`
        Reference to the netcdf class in case the data read need to be stored to netCDF file or read
        from a NetCDF file in case it was stored in the previous run
    hours_since : float
        Numerical data/time expressed in the number of hours since 0001-01-01 00:00:00
    record_data_name : str, optional
        Name of the data field as stored to the netcdf file. Default = "psd"
    spectral_components=None

    """

    def __init__(self,
                 current_date_time=None,
                 current_sea_state_values=None,
                 n_frequencies=180,
                 n_directions=123,
                 min_dir_in_deg=0,
                 max_dir_in_deg=360,
                 min_freq_in_hz=0.02,
                 max_freq_in_hz=2.0,
                 freq_in_hz=None,
                 directions_deg=None,
                 netcdf=None,
                 hours_since=None,
                 record_data_name=None,
                 spectral_components=None,
                 column_prefix=""
                 ):

        # inherit the SeaStateData object and create a uniform mesh to store the PSD
        super(WaveSpectralComponent2DRecord, self).__init__(n_directions=n_directions,
                                                            n_frequencies=n_frequencies,
                                                            netcdf=netcdf,
                                                            record_data_name=record_data_name,
                                                            hours_since=hours_since,
                                                            date_time=current_date_time,
                                                            min_dir_in_deg=min_dir_in_deg,
                                                            max_dir_in_deg=max_dir_in_deg,
                                                            min_freq_in_hz=min_freq_in_hz,
                                                            max_freq_in_hz=max_freq_in_hz,
                                                            freq_in_hz=freq_in_hz,
                                                            directions_deg=directions_deg
                                                            )

        self.current_sea_state_values = current_sea_state_values

        self.spectral_components = spectral_components

        # the head_df is going to contain the header of this spectral records with the Hs Tz, etc
        self.header_df = None
        self.column_prefix = column_prefix  # this prefix is added to all the columns. default = ""

        self.create_netcdf_frequency_and_direction_dimensions()

        self.n_added_components = 0

        self.delta_area_per_bin = None

        if self.spectral_components is not None:
            self.logger.debug("Creating the spectral from the components")
            self.make_spectrum_from_components()
        elif self.netcdf is not None:
            # we are reading from a netcdf data fram
            self.read_data_from_netcdf()
        else:
            self.logger.warning("Could not create a spectrum for {} as spectral_components is None "
                                "and also we don't have a netcdf file".format(self.date_time))

        self.set_delta_grid()
        if self.n_added_components > 0:
            self.set_psd_properties()

    def calculate_directional_distribution(self, directional_distribution_info, wave_direction):
        """
        Calculate the distribution function over the directions in degrees

        Parameters
        ----------
        directional_distribution_info: dict
            Dictionary holding the properties of the distribution function to use
        wave_direction: float
            Principle wave direction

        Returns
        -------
        ndarray
            1D array with the distibution for the directions in deg

        Notes
        -----
        The spreading functions are returning the distributions vs the direction in radians. In
        order to express the distribution function as a fuction of direction in degrees the
        results of the spreading functions is multiplied with (2 pi / 360)
        """

        # get the directional distribution
        try:
            s_spreading_factor = directional_distribution_info["s_spreading_factor"]
        except KeyError:
            s_spreading_factor = None
        try:
            # we can also use the n spreading factor, defined as s = 2 * n + 1
            # by default the spreading functions take the s value (with 5 for wind and 13 for swell)
            n_spreading_factor = directional_distribution_info["n_spreading_factor"]
        except KeyError:
            n_spreading_factor = None
        version = directional_distribution_info["version"]

        if version == "dnv":
            # take the  DNS definition of the directional distribution
            d_spread = spreading_function(theta=self.directions, theta0=np.deg2rad(wave_direction),
                                          s_spreading_factor=s_spreading_factor,
                                          n_spreading_factor=n_spreading_factor)
        elif version == "hmc":
            # take the  matlab implementation of the directional distribution
            d_spread = spreading_function2(self.directions, np.deg2rad(wave_direction),
                                           s_spreading_factor=s_spreading_factor,
                                           n_spreading_factor=n_spreading_factor)
        else:
            raise AssertionError("Directional distribution only 'dnv' or 'hmc'. You gave: {}"
                                 "".format(version))

        # we want the spreading in degrees, but we passed the direction in radians. Note that
        # 1.  sum(D1(theta)) * dtheta == 1
        # 2.  sum(D2(f)) * d f == 1
        # the number of bins remains the same. To fullfill both requirements 1 and 2
        # you can see that D2 = D1 * dtheta / df = D1 * 2 pi / 360
        d_spread *= 2 * np.pi / 360.0

        return d_spread

    def calculate_psd1_vs_freq(self, energy_spectrum_info, hs, tp):
        """
        Calculate the 1D Power spectral density as a function of the frequency in Hz for a given
        Wave type and hs/tp

        Parameters
        ----------
        energy_spectrum_info: dict
            Dictionary holding the wave component properties
        hs: float
            Significant wave height
        tp: float
            Peak period

        Returns
        -------
        ndarray:
            1D array with the PSD for the given frequencies in Hz
        """

        spectrum_type = energy_spectrum_info["type"]
        if spectrum_type == "jonswap":
            gamma = energy_spectrum_info["gamma"]
            version = energy_spectrum_info["version"]
            self.logger.debug("Calculating Jonswap spectrum with gamma={} using version {}"
                              "".format(gamma, version))
            psd = spectrum_jonswap(self.frequencies, Hs=hs, Tp=tp, gamma=gamma,
                                   spectral_version=version)
        elif spectrum_type == "gauss":
            version = energy_spectrum_info["version"]
            try:
                sigma = energy_spectrum_info["sigma"]
            except KeyError:
                sigma = None
            self.logger.debug("Calculating gauss spectrum version {}".format(version))
            psd = spectrum_gauss(self.frequencies, Hs=hs, Tp=tp, spectral_version=version,
                                 sigma=sigma)
        else:
            # only gauss and jonswap implemented. Raise an error
            raise AssertionError("Only jonswap and gauss spectrum implemented. You gave: {}"
                                 "".format(spectrum_type))

        # the spectrum routine took omega (rad/s) as input but in enviview they work in frequency in
        # Hz. Convert psd from rad/s - > /s
        psd *= (2 * np.pi)

        return psd

    def make_spectrum_from_components(self):
        """
        Build a 2d spectrum at the data2d mesh based on the Hs, Tp, theta values stored in the
        current sea state values and the prescription of the components stored in the
        spectral_components dictionary
        """

        for name, component_values in self.spectral_components.items():

            try:
                hs_name = component_values["Hs"]
                tp_name = component_values["Tp"]
                theta0_name = component_values["Theta0"]
            except KeyError as err:
                raise KeyError("Spectral Component '{}' Does not have one of the mandatory field Hs"
                               ", Tp, Theta0\n {}".format(name, err))

            hs = self.current_sea_state_values[hs_name]
            tp = self.current_sea_state_values[tp_name]
            direction_wave = self.current_sea_state_values[theta0_name]
            rel_theta0_name = "_".join([RELATIVE_DIRECTION_PREFIX, theta0_name])
            rel_direction_wave = self.current_sea_state_values[rel_theta0_name]

            # see if all the wave components are given
            if np.isnan(hs) or np.isnan(tp) or np.isnan(direction_wave) or \
                    np.isnan(rel_direction_wave) or \
                    (None in (hs, tp, direction_wave, rel_direction_wave)):
                self.logger.debug("At least one of the sea state components is not given. Skipping")
                continue

            # get the spectrum and directional information
            try:
                directional_distribution_info = component_values["directional_distribution"]
            except KeyError as err:
                raise KeyError("directional_distribution is not defined for {}\n"
                               "{}".format(name, err))
            try:
                energy_spectrum_info = component_values["energy_spectrum"]
            except KeyError as err:
                raise KeyError("energy_spectrum is not defined for {}\n{}".format(name, err))

            # calculate the spectrum
            psd_vs_freq = self.calculate_psd1_vs_freq(energy_spectrum_info, hs, tp)

            d_spreading = self.calculate_directional_distribution(directional_distribution_info,
                                                                  rel_direction_wave)

            # calculate the 2D psd based on the 1d psd and the spreading function
            ss, dd = np.meshgrid(psd_vs_freq, d_spreading, indexing='ij')
            psd_2d_component = ss * dd

            # add this component to the total
            self.psd_2d += psd_2d_component

            # keep how many components where added
            self.n_added_components += 1

            if self.header_df is None:
                self.header_df = pd.DataFrame(index=pd.DatetimeIndex([self.date_time]))
            self.header_df[self.column_prefix + hs_name] = hs
            self.header_df[self.column_prefix + tp_name] = tp
            self.header_df[self.column_prefix + theta0_name] = direction_wave

    def report_components(self, component_to_return=None, quite=False):
        """
        Make a summary of the components values Hs, Tp, and Theta0 of this records for all
        components found in the spectral components dictionary

        Parameters
        ----------
        component_to_return: str
            Name of the component values to return
        quite: bool
            Do not report to screen, only return the component values

        Returns tuple  of 3 floats or Nones
            The sea state components (Hs, Tp, Theta0) or (None, None, Non) in case no component to
            return was requested
        """

        component_values_to_return = (None, None, None)
        for name, component_values in self.spectral_components.items():

            try:
                hs_name = component_values["Hs"]
                tp_name = component_values["Tp"]
                theta0_name = component_values["Theta0"]
            except KeyError as err:
                raise KeyError("Spectral Component '{}' Does not have one of the mandatory field Hs"
                               ", Tp, Theta0\n{}".format(name, err))

            hs = self.current_sea_state_values[hs_name]
            tp = self.current_sea_state_values[tp_name]
            direction_wave = self.current_sea_state_values[theta0_name]
            if not quite:
                self.logger.info("Sea state values of {:20s} Hs={:.2f} m, Tp = {:.1f} s, "
                                 "Theta0 = {:.1f}".format(name, hs, tp, direction_wave))

            if name == component_to_return:
                component_values_to_return = (hs, tp, direction_wave)

        return component_values_to_return


class WaveSpectralEnergy2DRecord(WaveSpectralDensity2DRecord):
    """A reader class for one record out of the a file carrying 2D spectral energy records.

    Parameters
    ----------
    file_pointer: FileIO buffer
        A pointer to the file which is already open
    n_directions: int, optional
        Number of directional bins for the uniform mesh. Default = 123
    n_frequencies: int, optional
        Number of frequency bins for the uniform mesh. Default = 180
    n_directions_nu: int, optional
        Number of directional bins as used in the fugro file for the non-uniform mesh.
        Default = None. Is obtained from the data file used to store the psd
    n_frequencies_nu: int, optional
        Number of frequency bins as used in the fugro file for the non-uniform mesh.
        Defatult = None. Is obtained from the data file used to store the psd
    netcdf: :obj:`NetCDFInfo`
        Reference to the netcdf class in case the data read need to be stored to netCDF file or
        read from a NetCDF file in case it was stored in the previous run
    hours_since : float
        Numerical data/time expressed in the number of hours since 0001-01-01 00:00:00

    Notes
    -----
    * The class is used by the WaveSpectralEnergy2DReader and should not be called directly.
    * The WaveSpectralEnergy2Reader opens the date file with the time series of spectral records,
      where each records is stored in this class
    * The data is this class is called 'SpectralEnergy2D' because it stores the value of the total
      energy per bin, which means that the spectral density is multiplied with th bin area already
    * The spectral density follow by dividing by the area of the bin. This is stored in the class
      :obj:`WaveSpectralDensity2DRecord`, which is the ancestor of this class
    """

    def __init__(self, file_pointer,
                 n_directions=123,
                 n_frequencies=180,
                 n_directions_nu=None,
                 n_frequencies_nu=None,
                 netcdf=None,
                 hours_since=None,
                 record_data_name=None
                 ):

        # inherit the SeaStateData object and create a uniform mesh to store the PSD
        super(WaveSpectralEnergy2DRecord, self).__init__(n_directions=n_directions,
                                                         n_frequencies=n_frequencies,
                                                         netcdf=netcdf,
                                                         record_data_name=record_data_name,
                                                         hours_since=hours_since)

        self.n_directions_nu = n_directions_nu
        self.n_frequencies_nu = n_frequencies_nu

        if n_directions_nu is None:
            self.data_2d_nu = None
            self.psd_vs_ang_nu = None
            self.psd_vs_freq_nu = None
            self.psd_vs_freq_n_nu = None
            self.freq_in_hz_nu = None
            self.directions_deg_nu = None
        else:
            self.data_2d_nu = np.zeros((self.n_frequencies_nu, self.n_directions_nu))
            self.psd_vs_ang_nu = np.zeros(self.n_directions_nu)
            self.psd_vs_freq_nu = np.zeros(self.n_frequencies_nu)
            self.psd_vs_freq_n_nu = np.zeros(self.n_frequencies_nu)

            self.freq_in_hz_nu = np.zeros(self.n_frequencies_nu)
            self.directions_deg_nu = np.zeros(self.n_directions_nu)

        self.data_2d = None  # can contain later the data_2d non-uniform on the uniform mesh

        # the head_df is going to contain the header of this spectral records with the Hs Tz, etc
        self.header_df = None

        self.delta_dir_deg_nu = None
        self.delta_freq_hz_nu = None
        self.delta_area_per_bin_nu = None
        self.delta_area_deg_hz_nu = None
        self.delta_area_rad_nu = None

        self.file_pointer = file_pointer

        self.delta_area_per_bin = None

        if file_pointer is not None:
            # we are reading from a file, so get the record here
            self.read_spectral_data_record()

            # create the new frequencies based on the non-uniform frequencies
            self.freq_in_hz = np.linspace(self.freq_in_hz_nu[0], self.freq_in_hz_nu[-1],
                                          self.n_frequencies)
            self.directions_deg = np.linspace(0, 360, self.n_directions, endpoint=False)

            self.create_netcdf_frequency_and_direction_dimensions()

            # calculate the grid bin size for the non-uniform mesh
            self.set_delta_grid_non_uniform()

            self.convert_power_energy_to_psd()
        else:
            # we are reading from a netcdf data frame
            self.read_data_from_netcdf()

        self.set_delta_grid()
        self.set_psd_properties()

    def set_delta_grid_non_uniform(self):
        """
        Get the non-uniform delta's of the mesh
        :return: 
        """

        self.delta_dir_deg_nu = np.diff(self.directions_deg_nu)
        self.delta_dir_deg_nu = np.hstack((self.delta_dir_deg_nu, [self.delta_dir_deg_nu[-1]]))
        self.delta_freq_hz_nu = np.diff(self.freq_in_hz_nu)
        self.delta_freq_hz_nu = np.hstack(([self.delta_freq_hz_nu[0]], self.delta_freq_hz_nu))

        dd, ff = np.meshgrid(self.delta_dir_deg_nu, self.delta_freq_hz_nu)
        self.delta_area_per_bin_nu = np.array(ff * dd)

    def read_spectral_data_record(self):
        """
        read routine for fugro files
        """

        # set a alias to the file pointer
        fp = self.file_pointer

        # here the two header lines are read
        self.header_df = self.read_header_line_of_record(fp)

        # now the line with frequencies is read. The last item is a string with AngSpec label
        line = fp.readline().strip().split(",")[1:]
        self.freq_in_hz_nu = np.array([float(val) for val in line[:-1]])

        for j_dir in range(self.n_directions_nu):
            # read the next line of psd values at this direction
            line = fp.readline().strip().split(",")
            try:
                line.remove("")  # remove the empty fields as metocean ends with a ,
            except ValueError:
                pass
            self.data_2d_nu[:, j_dir] = [float(val) for val in line[1:-1]]
            self.directions_deg_nu[j_dir] = float(line[0])
            self.psd_vs_ang_nu[j_dir] = float(line[-1])

        # now the line with 1d spectrum vs frequency
        line = fp.readline().strip().split(",")[1:]
        try:
            line.remove("")
        except ValueError:
            pass
        self.psd_vs_freq_nu = [float(val) for val in line]
        line = fp.readline().strip().split(",")[1:]
        try:
            line.remove("")
        except ValueError:
            pass
        self.psd_vs_freq_n_nu = [float(val) for val in line]

        # last line is a blank line
        fp.readline()

    def read_header_line_of_record(self, fp):
        """
        this routine only grabs the two header lines of a spectral record
        :param fp: 
        :return: data frame with on row with the header line data
        """
        header_names = fp.readline().strip().split(",")
        header_values = fp.readline().strip().split(",")
        date_time_string = "".join(header_values[:2])
        self.date_time = pd.to_datetime(date_time_string, format="%Y%m'%d%H%M")
        header_data = np.array([float(val) for val in header_values[3:]])
        # create data frame with one row
        df = pd.DataFrame(index=pd.DatetimeIndex([self.date_time]))
        for cnt, name in enumerate(header_names[3:]):
            # important dat you can not set the data column name in one time via the columns argument
            df.ix[self.date_time, name] = float(header_data[cnt])

        return df

    def convert_power_energy_to_psd(self):
        """
        the fugro spectral data is put on a non-uniform mesh. We want to put it on a uniform mesh and turn the
        power energy per bin (as used by fugro) to a power spectral density 
        """

        self.resample_data_to_new_mesh(self.directions_deg, self.freq_in_hz)

    def resample_data_to_new_mesh(self, new_directions, new_frequencies):

        # get the extended direction axis of both the original and the new direction axis.
        direction_3 = np.hstack((self.directions_deg_nu - 360, self.directions_deg_nu,
                                 self.directions_deg_nu + 360))
        new_dirs_3 = np.hstack((new_directions - 360, new_directions, new_directions + 360))

        # the size of the double direction axis
        n_new_dirs = new_directions.size

        # convert the data on the non-uniform mesh to a psd by dividing with the bin size
        data = self.data_2d_nu / self.delta_area_per_bin_nu

        # triple the data in the theta direction axis as explained above
        data_3 = np.hstack((data, data, data))

        # interpolate the 2D plane with the doubled direction axis
        f_inter = interp2d(direction_3, self.freq_in_hz_nu, data_3)
        data_resampled = f_inter(new_dirs_3, new_frequencies)

        # collect the real and imaginary part, only take the first half part of the direction axis
        self.psd_2d = data_resampled[:, n_new_dirs: 2 * n_new_dirs]

        self.directions_deg = new_directions
        self.freq_in_hz = new_frequencies


class WaveSpectralMomentsQ2DRecord(WaveSpectralDensity2DRecord):
    """
    Class for importing wave rider buoy data files.

    Parameters
    ----------
    file_name : str
        Name of the Wave rider buoy data to import. End with stp or nc
    record_data_name: str, optional
        Name of the stored spectral data
    hours_since: float,
        Numerical date time of the current record to import
    n_directions : int
        Number of direction bins to use. Default = 540. The reason that is large is that the direction axis is
        running over 3 periods of 2pi to take care of the spectral periodicy along the direction axis
    n_frequencies : int, optional
        Number of frequency bins to use. Default = 123
    min_degrees : float, optional
        Minimu value of the direction domain in degrees. Default = -360
    max_degrees : float, optional
        Maximum value of the directional domain in degrees. Default = 720
        The object holding all information for the netcdf file
    time_zone: str, optional
        The time zone used to report the data. Default="UTC"

    Notes
    -----

    * The wave rider buy data files store the 2D spectra in a quasi 2D manner: per frequency bin the
      4 moments of the distribution function are given, describing the mean direction, width,
      skewness and flatness. It is quasi 2D as only one peak per frequency bin is allowed. For this
      reason, this class is called WaveSpectralMomentsQ2D, where the Q stands for quasi-2D
    * The `read_spt_file` method is based on the Python script distributed by Datawell

      https://gist.github.com/seumasmorrison/5935386
    """

    def __init__(self,
                 file_name,
                 record_data_name=None,
                 hours_since=None,
                 netcdf=None,
                 n_directions=540,
                 n_frequencies=123,
                 time_zone="UTC",
                 min_degrees=-360,
                 max_degrees=720,
                 use_nn_triangulation=False,
                 show_read_time=True
                 ):

        # analyse file name and check extension

        self.file_name = file_name
        try:
            self.date_time = dparser.parse(os.path.splitext(self.file_name)[0], fuzzy=True)
        except ValueError:
            self.date_time = None
        self.file_base, self.file_extension = os.path.splitext(self.file_name)

        self.show_read_time = show_read_time

        # inherit the SeaStateData object
        super(WaveSpectralMomentsQ2DRecord, self).__init__(n_directions=n_directions,
                                                           n_frequencies=n_frequencies,
                                                           netcdf=netcdf,
                                                           record_data_name=record_data_name,
                                                           hours_since=hours_since,
                                                           date_time=self.date_time,
                                                           )

        self.n_header_lines = 12
        self.header_data = OrderedDict()
        self.logger = get_logger(__name__)
        self.time_zone = time_zone
        self.total_degrees = 3 * self.n_directions
        self.min_degrees = min_degrees
        self.max_degrees = max_degrees

        self.use_nn_triangulation = use_nn_triangulation

        self.sea_states_df = None

        self.response_spectra = OrderedDict()

        if self.file_extension == ".spt":
            # create the netcdf file name
            # self.net_cfd_file_name = os.path.join(self.net_cdf_output_directory,
            #                                      os.path.split(self.file_base)[1] + ".nc")
            # self.logger.debug("Create filename for NETCDF {}".format(self.net_cfd_file_name))

            # read the current fugro csv file
            if self.netcdf is None or self.get_time_index_if_available() is None:
                with Timer(verbose=False, units="s") as tt:
                    self.read_spt_file_heading()
                    self.read_spt_file()

                if self.show_read_time:
                    self.logger.info("Read the WRB spectral moments from {} in {:.1f} s"
                                     "".format(self.file_name, tt.secs))
                self.header_df = self.make_header_line_of_record()

                self.create_netcdf_frequency_and_direction_dimensions()
            else:
                self.logger.info("Retrieving the wave rider data from the netcdf data base")

                self.read_data_from_netcdf()

            self.set_psd_properties()

        elif self.file_extension == ".nc":
            self.logger.info("Reading the NETCDF file".format(self.file_name))
            raise AssertionError("Not yet implemented")
        else:
            raise AssertionError("File type should be either .csv or .nc")

        self.logger.debug("Done reading the spectra")
        # if self.logger.getEffectiveLevel() == logging.DEBUG:
        #    self.logger.debug("Sea state data frame:\n{}".format(self.sea_states_df.info()))

    @staticmethod
    def pdf(norm_psd, x):
        return (100 * norm_psd) / sqrt(2 * pi) * exp(-x ** 2 / 2.0)

    @staticmethod
    def cdf(x):
        return (1 + erf(x / sqrt(2.0))) / 2

    def skew(self, x, e=0, w=1, a=0, norm_psd=1):
        t = (x - e) / w
        return 2 / w * self.pdf(norm_psd, t) * self.cdf(a * t)
        # You can of course use the scipy.stats.norm versions
        # return 2 * norm.pdf(t) * norm.cdf(a*t)

    def distribution(self, direction, spread, norm_psd, skewness):
        # modification on original: total_degrees + 1 since we have added one too in the calling
        # routing
        x = linspace(self.min_degrees, self.max_degrees, self.total_degrees + 1)

        p = self.skew(x, direction, spread, skewness, norm_psd)
        return p

    def read_spt_file_heading(self):

        self.logger.debug("Reading the SPT heading file")

        lines = list()
        i_line = self.n_header_lines
        with open(self.file_name) as fp:
            while i_line:
                lines.append(fp.readline().strip())
                i_line -= 1

        self.header_data["tn"] = int(lines[0])
        self.header_data["Hs_in_cm"] = float(lines[1])
        self.header_data["Tz"] = float(lines[2])
        self.header_data["Smax"] = float(lines[3])
        self.header_data["TRef"] = float(lines[4])
        self.header_data["TSea"] = float(lines[5])
        self.header_data["Bat"] = int(lines[6])
        self.header_data["Av"] = float(lines[7])
        self.header_data["Ax"] = float(lines[8])
        self.header_data["Ay"] = float(lines[9])
        self.header_data["Ori"] = float(lines[10])

    def read_spt_file(self):
        """
        :return: 
        """
        self.logger.debug(
            "Read the spt data and resample to mesh {} x {}".format(self.n_frequencies,
                                                                    self.n_directions))
        max_psd = self.header_data["Smax"]
        spt = np.genfromtxt(self.file_name, delimiter=',', skip_header=self.n_header_lines)
        complete_spt = spt
        # y is frequency
        yi = np.linspace(min(spt[:, 0]), max(spt[:, 0]), self.n_frequencies)
        # x is direction. Add 1 to get a nice distribution on 0, 2, 4 etc,
        xi = np.array(np.radians(np.linspace(self.min_degrees, self.max_degrees,
                                             3 * self.n_directions + 1)))
        # x is direction
        dd, ff = np.meshgrid(xi, yi)
        orig_frequencies = spt[:, 0]
        orig_norm_psds = spt[:, 1] * max_psd
        orig_direction = spt[:, 2]
        orig_spread = spt[:, 3]
        orig_skew = spt[:, 4]
        all_spreads = np.array([[], [], []])
        for index, frequency in enumerate(orig_frequencies):
            norm_psd = orig_norm_psds[index]
            direction = orig_direction[index]
            spread = orig_spread[index]
            skewness = orig_skew[index]
            spread_distribution = np.array(self.distribution(direction, spread, norm_psd, skewness))
            spread_plus_dir = np.array(
                [spread_distribution, xi, np.linspace(frequency, frequency,
                                                      self.total_degrees + 1)])
            all_spreads = np.concatenate((all_spreads, spread_plus_dir), axis=1)
        Z = None
        if self.use_nn_triangulation:
            # for some readon this natural neighbour algorithm is extremely slow, so be default use
            # linear interpolation
            try:
                self.logger.debug("Start gridding the data with the NN algorithm")
                Z = mlab.griddata(all_spreads[1], all_spreads[2], all_spreads[0], xi, yi)
            except RuntimeError as err:
                self.logger.debug("err: {}\nNo problem. Continue with interp='linear'".format(err))

        if Z is None:
            # Z is none is use_nn_triangulation was false or when it failed. In both cases, try
            # again with linear
            self.logger.debug("Start gridding the data with the linear interpolation algorithm")
            Z = mlab.griddata(all_spreads[1], all_spreads[2], all_spreads[0], xi, yi,
                              interp='linear')

        self.logger.debug("Done gridding the data")
        min_index = self.n_directions
        max_index = 2 * self.n_directions
        max_index_3 = 3 * self.n_directions
        subset_Z = Z[:, np.arange(min_index, max_index)]
        Z_less_than_0 = Z[:, np.arange(0, min_index)]
        z_over_360 = Z[:, np.arange(max_index, max_index_3)]
        overlapping_z = subset_Z + Z_less_than_0 + z_over_360

        # we are convertig from rad to deg -> x 180 / pi
        self.directions_deg = np.rad2deg(xi[min_index:max_index])
        self.freq_in_hz = yi

        self.set_delta_grid()

        # scale the psd to with pi / 180 due to the rad to deg conversion
        self.psd_2d = overlapping_z * np.pi / 180.0

        # calculate the Hs based on the spectra and scale the psd with the given HSig
        self.header_data["HsPSD"] = 4 * np.sqrt(self.psd_2d.sum() * self.delta_area_per_bin)
        scale_factor = self.header_data["Hs_in_cm"] / 100.0 / self.header_data["HsPSD"]
        self.psd_2d *= scale_factor * scale_factor

        # store the scaled energy in m (by definition the same as Hs_in_cm divided by 100)
        self.header_data["Hs"] = 4 * np.sqrt(self.psd_2d.sum() * self.delta_area_per_bin)

    def make_header_line_of_record(self):
        """
        Create a header line for this record
        """
        # self.header_names.append("Hs")
        # header_data = np.array([float(val) for val in header_values])
        # create data frame with one row
        df = pd.DataFrame(index=pd.DatetimeIndex([self.date_time]))
        for name, value in self.header_data.items():
            # important: you can not set the data column name in one time via the columns argument
            df.ix[self.date_time, name] = float(value)

        return df


class WaveSpectralDensity2DReader(object):
    """ A reader class for fugro data files

    Parameters
    ----------

    data_base_name : str, optional
        Name of the current data base
    date_time: :obj:`datetime`, optional
        Date/Time used to create a new netcdf variable entry for a spectral time series
    netcdf: :obj:`NetCDFInfo`, optional
        The object holding all information for the netcdf file
    time_zone: str, optional
        The time zone used to report the data. Default="UTC"
    n_directions: int, optional
        Number of direction in grid
    n_frequencies: int, optional
        Number of frequencies in grid
    freq_in_hz: ndarray
        Array with frequencies in Hz. Optional. Default = None, which means that the frequencies
        are based on the *min_freq_in_hz* and *max_freq_in_hz* arguments
    directions_deg: ndarray
        Array with direction in degrees. Optional, default = None which means that the directions
        are based on the *min_dir_in_deg* and *max_dir_in_deg* input arguments
    min_dir_in_deg: float
        Minimum direction in degrees. Optional, default = 0
    max_dir_in_deg: float
        Maximum direction in degrees. Optional, default = 360
    min_freq_in_hz: float
        Minimum frequency in Hz. Optional. Default = 0.02
    max_freq_in_hz: float
        Maximum frequency in Hz. Optional. Default = 2.0
    record_data_name : str, optional
        Name of the record data
    """

    def __init__(self,
                 data_base_name=None,
                 date_time=None,
                 netcdf=None,
                 time_zone="UTC",
                 n_directions=None,
                 n_frequencies=None,
                 min_dir_in_deg=0,
                 max_dir_in_deg=360,
                 min_freq_in_hz=0.02,
                 max_freq_in_hz=2.0,
                 freq_in_hz=None,
                 directions_deg=None,
                 record_data_name=None,
                 ):
        """
        """

        self.logger = get_logger(__name__)

        self.time_zone = time_zone
        self.record_data_name = record_data_name

        self.n_frequencies = n_frequencies
        self.n_directions = n_directions
        self.min_freq_in_hz = min_freq_in_hz
        self.max_freq_in_hz = max_freq_in_hz
        self.min_dir_in_deg = min_dir_in_deg
        self.max_dir_in_deg = max_dir_in_deg
        self.freq_in_hz = freq_in_hz
        self.directions_deg = directions_deg
        self.directions = None
        self.frequencies = None

        self.date_time = date_time
        self.netcdf = netcdf
        self.data_base_name = data_base_name

        self.directions_2d = None
        self.freq_in_hz_2d = None

        self.valid_for = None
        self.n_records = None
        self.latitude = None
        self.longitude = None
        self.depth = None

        self.sea_states_df = None

        self.netcdf_variable_name = None

        if self.date_time is not None:
            date_time_str = self.date_time.strftime("%Y%m%dT%H%M")
        else:
            date_time_str = "00010101000000"
        if self.data_base_name is not None:
            self.netcdf_variable_name = "_".join([self.data_base_name, date_time_str])

        # this ordered dict is going to hold the spectra per timestamp. The timestamp are used as a
        # key, the values are the SpectralRecords classes
        self.spectral_records = OrderedDict()

        self.sailing_heading_and_speed_df = None
        self.sailing_speeds = OrderedDict()
        self.sailing_headings = OrderedDict()

        # this dictionary can hold response spectra for multiple RAO, which in its turn can add
        # multiple channels
        self.response_spectra = OrderedDict()

        self.logger.debug("Done reading the spectra")

    def open_net_cdf_data_base(self):

        # this name is constructed from the database name + the start date/time of the current
        # data serie, thus references to one single spectral file with multiple spectra in time
        if self.netcdf is not None:
            self.netcdf.init_net_cdf_database(self.netcdf_variable_name)
            self.netcdf.add_dimension("time")
            # groups cannot be read by paraview
            # self.netcdf.add_dimension("time", group=self.netcdf.groups[self.netcdf.group_name])

    def read_spectral_records_from_netcdf(self, close_after_reading=True):
        """Try to read records from netcdf

        Parameters
        ----------
        close_after_reading: bool
            For fugro files we need to close the dataset, for the waverider not. Default = True.
        """

        self.logger.debug("Getting data from NetCDF")
        sea_state_list = list()
        for hours_since in self.netcdf.dataset.variables["time"]:

            if np.isnan(hours_since):
                self.logger.debug("This time is not valid")
                continue

            record = WaveSpectralDensity2DRecord(n_frequencies=self.n_frequencies,
                                                 n_directions=self.n_directions,
                                                 min_freq_in_hz=self.min_freq_in_hz,
                                                 max_freq_in_hz=self.max_freq_in_hz,
                                                 min_dir_in_deg=self.min_dir_in_deg,
                                                 max_dir_in_deg=self.max_freq_in_hz,
                                                 freq_in_hz=self.freq_in_hz,
                                                 directions_deg=self.directions_deg,
                                                 netcdf=self.netcdf,
                                                 hours_since=hours_since,
                                                 record_data_name=self.record_data_name
                                                 )
            record.read_data_from_netcdf()
            record.set_delta_grid()
            record.set_psd_properties()

            if self.freq_in_hz is None:
                # base the values of the directions on the netcdf values
                self.freq_in_hz = record.freq_in_hz
                self.directions_deg = record.directions_deg
                self.directions_2d = record.directions_2d
                self.freq_in_hz_2d = record.freq_in_hz_2d
                self.directions_deg = record.directions_deg
                self.directions = np.deg2rad(self.directions_deg)

            record_date_time = pd.Timestamp(record.header_df.index[0], tz=self.time_zone)

            # store this record to the dictionary
            self.spectral_records[record_date_time] = record

            sea_state_list.append(record.header_df)

        # done with reading. No collect all the sea states
        if os.path.exists(self.netcdf.file_name_mp):
            self.logger.debug("Reading {}".format(self.netcdf.file_name_mp))
            self.sea_states_df = pd.read_msgpack(self.netcdf.file_name_mp)
            self.sea_states_df.index = [pd.Timestamp(tme, tz=self.time_zone) for tme in
                                        self.sea_states_df.index]
        elif sea_state_list:
            self.sea_states_df = pd.concat(sea_state_list)
        #
        # if self.sea_states_df is not None:
        #    self.sea_states_df = pd.concat([sea_states_df, self.sea_states_df])
        # else:
        #    self.sea_states_df = sea_states_df
        #
        if close_after_reading:
            self.netcdf.dataset.close()

    @staticmethod
    def make_statistics_header(date_time, response_spec2d):
        """
        Create a data frame of one line with the current statistics of the response spectrum

        Parameters
        ----------
        date_time: :obj:`datetime`
            Date/time  of the current record

        Returns
        -------
        DataFrame:
            A date frame with one row and the following columns:

                - STD : Standard deviation
                - SDA : Significant double amplitude (= 4 * STD)
                - SMPM3h : Single sided Most Probable Maximum for a 3 hour storm
        """

        statistics_names = ["STD", "SDA", "SMPM3h"]
        response_std = np.sqrt(response_spec2d.sum())  # standard deviation sigma
        response_sda = 4 * response_std  # significant double amplitude
        response_mpm = response_sda * 0.5 * 1.86

        # create a data empty data frame
        df = pd.DataFrame(index=pd.DatetimeIndex([date_time]), columns=statistics_names)
        df.iloc[0] = [response_std, response_sda, response_mpm]

        return df

    def calculate_this_operation(self, operation_name, operator):
        """Apply an operation on the sea_states_quantity using an eval

        Parameters
        ----------
        operation_name: str
            Name of the operator, used also to store a new data field
        operator: str
            The operator to carry out
        """

        eval_str = "{} = {}".format(operation_name, operator)
        self.logger.debug("info: {}".format(self.sea_states_df.info()))
        self.logger.debug("eval: {}".format(eval_str))
        self.sea_states_df[operation_name] = self.sea_states_df["HSig"]
        df = self.sea_states_df.copy()
        print(df)
        df.eval(eval_str, inplace=True)
        # self.sea_states_df.eval(eval_str, inplace=False)
        self.logger.debug("Now new coliumn {}".format(self.sea_states_df[operation_name]))
        self.logger.debug("TEST  {}".format(df))

    def set_mru_sailing_properties(self, mru_time_series):
        """Set the sailing properties as obtained from the mru time series data frame

        Parameters
        ----------
        mru_time_series: DataFrame
            Pandas DataFrame carrying the MRU information

        Attributes
        ----------
        sailing_heading_and_speed_df :
            DataFrame carrying the heading and speed obtained from the input MRU, sampled on the
            time grid of the spectral records

        """

        speed_name = mru_time_series.speed_name
        heading_name = mru_time_series.heading_name

        # just take the first time series as for the mru there should be only one
        first_key = list(mru_time_series.data_dict.keys())[0]
        mru_dataframe = mru_time_series.data_dict[first_key]

        # self.logger.debug("HIER {}".format(mru_dataframe))
        # self.logger.debug("speeds {}".format(mru_dataframe[speed_name]))

        column_selection = [speed_name, heading_name]

        mru_dataframe_at_spectral_times = pd.DataFrame(index=list(self.spectral_records.keys()),
                                                       columns=column_selection)

        self.logger.debug(mru_dataframe.head(5))
        self.logger.debug(mru_dataframe_at_spectral_times.head(5))
        # mru_dataframe_at_spectral_times.join(mru_dataframe, how='outer')

        # create a combined data frame of the mr_data_frame time and monitor trajectory with the distance
        df_inter = pd.concat(
            [mru_dataframe_at_spectral_times[column_selection],
             mru_dataframe[column_selection]]).sort_index()
        self.logger.debug("SUCEES\n{}".format(df_inter.head(10)))

        # .sort_index().interpolate().dropna()

        # the concat dataframe has double indexes in case the sea state time was equal to the
        # monitor time. We would like to use drop_duplicate to remove the double indices, however,
        # does only works on columns. Therefore, first set a normal counter as index, which
        # creates a new column with the datetimes (with the duplicates) call 'index', remove the
        # duplicates on this 'index' and then set is back as index
        df_inter = df_inter.reset_index().drop_duplicates(keep="last", subset="index").set_index(
            "index")
        self.logger.debug("SUCEES 2\n{}".format(df_inter.head(10)))
        df_inter = df_inter.interpolate().dropna()

        self.logger.debug("SUCEES 3\n{}".format(df_inter.head(10)))

        # this command selects only the times at the trajectory_sea_states again
        df_inter = df_inter.reindex(mru_dataframe_at_spectral_times.index).dropna()
        self.logger.debug("DFINTER:\n{}".format(df_inter))

        self.sailing_heading_and_speed_df = df_inter

    def calculate_response_spectrum(self,
                                    rao_db,
                                    mru_time_series=None,
                                    fixed_heading=None,
                                    fixed_speed=None,
                                    use_fixed_speed=False,
                                    use_fixed_heading=False,
                                    debug_plot=False
                                    ):
        """
        For the current spectral time series calculate the response spectra and store the values in
        the response_df

        Parameters
        ----------
        rao_db: :obj:`LiftDynReader`
            The object holding the RAO's used to calculate the response. For now only tested with an
            LiftDyn ROA class
        mru_time_series: :obj:`DataFrame`, optional
            A data frame containing information on the sailing speed and heading. Only needed for
            applying a velocity correction
        fixed_heading:  float, optional
            Value for the fixed heading if we are not using the MRU value.
        fixed_speed: float, optional
            Value for the fixed speed if we are not using  the MRU value.
        use_fixed_speed: bool, optional
            Use the fixed speed value even if we have the MRU data. Default = False
        use_fixed_heading: bool, optional
            Use the fixed heading value even if we have the MRU data. Default = False
        debug_plot: bool, optional
            Create some debug plots. For developers and checkprinters only. Default = False
        """
        if mru_time_series is not None:
            # get the heading and speed name of the current time series. Also set the dataframe
            speed_name = mru_time_series.speed_name
            heading_name = mru_time_series.heading_name
            # just take the first time series as for the mru there should be only one
            first_key = list(mru_time_series.data_dict.keys())[0]
        else:
            speed_name = None
            heading_name = None

        # add a new item to the response spectra and statistics
        response_spectra = ResponseSpectra(rao_db.name)
        self.response_spectra[rao_db.name] = response_spectra

        # if mru_time_series is None or use_fixed_speed:
        #    # in case we are not passing the mru dataframe (so we don't have the speed/heading) and
        # we want to set a fixed speed correction, do that only one time here
        #    if fixed_speed > 0.0:
        #        # take the rao belonging to the fixed speed. If it is 0.0, do nothing, so just take
        # the zero speed rao
        #        rao_db.pick_rao_for_speed(fixed_heading)

        # loop over the rao's of  this rao_db
        # This routine below would be more efficient of we would loop over time first and then
        # over the RAO in case we have a fixed speed correction or heading correction as we only
        # have to shift one time. Fix this later, but for now it is ok
        for rao_name, rao_index in rao_db.dof_names.items():
            self.logger.debug("Processing RAO component {}".format(rao_name))

            # create a new entry for this RAO to store the 2D response spectra per time +
            # the statistics
            response_spectrum = OrderedDict()
            response_spectra.spectra_per_rao_comp[rao_name] = response_spectrum

            statistics_df_list = list()

            if heading_name is not None:
                heading_at_spectral_times = self.sailing_heading_and_speed_df[heading_name]
            else:
                heading_at_spectral_times = None

            if speed_name is not None:
                speed_at_spectral_times = self.sailing_heading_and_speed_df[speed_name]
            else:
                speed_at_spectral_times = None

            # now loop over the sea state and calculate the response
            for record_date_time, record in self.spectral_records.items():

                if debug_plot:
                    fig, axis = plt.subplots(nrows=2, ncols=2)
                    plot_list = list()

                if heading_name is not None and not use_fixed_heading:
                    # in case we have a mru heading and do not want to impose a fixed heading, set
                    # the heading based on the mru heading
                    this_heading = heading_at_spectral_times.ix[record_date_time]
                    self.logger.debug(
                        "Using the heading from the base base {}".format(this_heading))
                else:
                    # we don't have a MRU heading and we also do not want to use a fixed heading,
                    # so use the fixed heading
                    this_heading = fixed_heading
                    self.logger.debug("Using the fixed heading {}".format(this_heading))

                if speed_name is not None and not use_fixed_speed:
                    # pick the speed from the speed_at_spectral_time dataframe
                    this_speed = speed_at_spectral_times.ix[record_date_time]
                    self.logger.debug("Using the speed from the base base {}".format(this_speed))
                else:
                    # in case we want a fixed speed we are already set it once above here outside
                    # the loop
                    this_speed = fixed_speed
                    self.logger.debug("Using the fixed speed from the base base {}"
                                      "".format(this_speed))

                if debug_plot:
                    plot_list.append(axis[0][0].contourf(rao_db.directions, rao_db.frequencies,
                                                         np.abs(rao_db.data[rao_index])))
                    axis[0][0].set_title(
                        "Zero heading ROA of previous round (may be speed-corrected)")

                if this_speed > 0:
                    self.logger.debug("Shifting RAO with speed {}".format(this_speed))
                    rao_db.pick_rao_for_speed(this_speed)  # this line replace the RAO for the speed
                else:
                    self.logger.debug("No time shift of the RAO needed for a zero speed")

                # set the heading on the current data ( shifted for the speed if this was required)
                data_heading = set_heading(data=rao_db.data, directions=rao_db.directions,
                                           heading=this_heading)

                rao_data_2d = data_heading[rao_index]
                rao_data_2d_sqr = abs(rao_data_2d * rao_data_2d)

                self.logger.debug("Processing at {} {} {}".format(record_date_time, this_heading,
                                                                  this_speed))

                sea_state_spec_2d = record.psd_2d

                # the area per bin of the current record
                delta_area = record.delta_area_per_bin

                resp_spec_2d = sea_state_spec_2d * rao_data_2d_sqr * delta_area

                if debug_plot:
                    plot_list.append(axis[1][0].contourf(rao_db.directions, rao_db.frequencies,
                                                         np.abs(data_heading[rao_index])))
                    axis[1][0].set_title("RAO shifted with speed and heading")
                    plot_list.append(axis[0][1].contourf(rao_db.directions, rao_db.frequencies,
                                                         np.abs(sea_state_spec_2d)))
                    axis[0][1].set_title("Sea state spectrum")
                    plot_list.append(axis[1][1].contourf(rao_db.directions, rao_db.frequencies,
                                                         np.abs(resp_spec_2d)))
                    axis[1][1].set_title("Response spectrum")
                    axis[1][1].set_xlabel("Direction [rad]")
                    axis[0][1].set_xlabel("Direction [rad]")
                    axis[0][0].set_ylabel("Frequency [rad/s]")
                    axis[1][0].set_xlabel("Frequency [rad/s]")
                    cnt = 0
                    for i in range(2):
                        for j in range(2):
                            axis[i][j].grid(True)
                            plt.colorbar(plot_list[cnt], ax=axis[i][j])
                            cnt += 1

                    plt.show()

                statistics_header_df = self.make_statistics_header(record_date_time, resp_spec_2d)

                statistics_df_list.append(statistics_header_df)

                response_spectrum[record_date_time] = resp_spec_2d

            response_spectra.statistics_per_rao_comp[rao_name] = pd.concat(statistics_df_list)

    def make_report(self):
        """
        Create a report of the current header
        :return: 
        """

        msgs = "{:20s} : {}"
        msgd = "{:20s} : {:10d}"
        msgf = "{:20s} : {:10.1f}"
        self.logger.info(msgs.format("Valid For", self.valid_for))
        self.logger.info(msgd.format("# Frequencies", self.n_frequencies))
        self.logger.info(msgd.format("# Directions ", self.n_directions))
        self.logger.info(msgd.format("# Records ", self.n_records))
        try:
            self.logger.info(msgf.format("Latitude", self.latitude))
        except TypeError:
            pass
        try:
            self.logger.info(msgf.format("Longitude", self.longitude))
        except TypeError:
            pass
        try:
            self.logger.info(msgf.format("Depth", self.depth))
        except TypeError:
            pass
        self.logger.info(msgs.format("Frequencies", self.freq_in_hz))
        self.logger.info(msgs.format("Directions", self.directions_deg))

    @staticmethod
    def _update_plot(
            frame_index=-1,
            data_x_2d=None,
            data_y_2d=None,
            psd_2d_list=list(),
            fig=None,
            ax=None,
            r_axis_lim=None,
            r_axis_type="period",
            x_label=None,
            x_plot_title=None,
            y_plot_title=None,
            title_font_size=None,
            plot_title=None,
            time_labels=[],
            hs_labels=[],
            x_time_label=0.05,
            y_time_label=0.95,
            x_hs_label=0.05,
            y_hs_label=0.89,
            changed_list=list(),
            min_data_value=0,
            max_data_value=None,
            number_of_contour_levels=10,
            polar_projection=True,
            color_map=cc.m_rainbow
    ):

        if frame_index < 0:

            if polar_projection:
                ax.set_theta_zero_location("N")
                ax.set_theta_direction(-1)

            if r_axis_lim is not None:
                # the radial limits are but on the ylim
                ax.set_ylim(r_axis_lim)

            changed_list = list()

            psd_2d = psd_2d_list[0]
        else:
            psd_2d = psd_2d_list[frame_index]
            clean_up_artists(axis=ax, artist_list=changed_list)

        psd_2d = np.nan_to_num(psd_2d)

        if min_data_value is None:
            v_min = np.nanmin(psd_2d)
        else:
            v_min = min_data_value

        if max_data_value is None:
            v_max = np.nanmax(psd_2d)
        else:
            v_max = max_data_value

        if min_data_value is not None and max_data_value is not None:
            # set the contour levels belonging to this subplot
            levels = linspace(v_min, v_max, number_of_contour_levels + 1, endpoint=True)
        else:
            levels = None

        # finally create the contour plot wit the RAO magnitude
        cs = ax.contourf(data_y_2d, data_x_2d, psd_2d, levels=levels, cmap=color_map, zorder=0)
        cs.cmap.set_under("k")
        cs.cmap.set_over("k")
        cs.set_clim(v_min, v_max)

        # store the contours in the changed_list so we can delete it the next round
        changed_list.append(cs)

        if time_labels:
            if frame_index < 0:
                time_label = time_labels[0]
            else:
                time_label = time_labels[frame_index]
            txt = plt.figtext(x_time_label, y_time_label, time_label, fontsize=title_font_size,
                              verticalalignment="top")

            changed_list.append(txt)

        if hs_labels:
            if frame_index < 0:
                hs_label = hs_labels[0]
            else:
                hs_label = hs_labels[frame_index]
            hs_txt = plt.figtext(x_hs_label, y_hs_label, hs_label,
                                 fontsize=title_font_size, verticalalignment="top")
            changed_list.append(hs_txt)

        if frame_index < 0:
            # set the axis labels
            ax.set_xlabel(x_label)
            if polar_projection:
                ax.set_rlabel_position(180)

            if r_axis_type == "period":
                # ax.set_yticklabels(["A"])
                new_labels = list()
                label_values = list()
                for label in ax.get_yticks():
                    try:
                        new_labels.append("{:.1f}".format(1 / float(label)))
                        label_values.append(label)
                    except (ValueError, ZeroDivisionError):
                        pass

                ax.set_yticks(label_values)
                ax.set_yticklabels(new_labels)

            # create a contour bar
            cbar = fig.colorbar(cs, ax=ax)
            cbar.ax.set_ylabel("{} [{}]".format("PSD", "m2s"))

            if plot_title is not None:
                plt.figtext(x_plot_title, y_plot_title, plot_title, fontsize=title_font_size,
                            verticalalignment="top")
                window_title = plot_title
                fig.canvas.set_window_title(window_title)

        return changed_list

    def plot_frame(self, frame_index=0, frame_datetime=None,
                   figsize=None,
                   r_axis_type="period",
                   plot_title=None,
                   x_plot_title=0.05,
                   y_plot_title=0.99,
                   delta_y_titles=0.03,
                   max_title_lines=None,
                   title_font_size=10,
                   r_axis_lim=None,
                   x_time_label=0.05,
                   y_time_label=0.94,
                   x_hs_label=0.05,
                   y_hs_label=0.89,
                   min_data_value=0,
                   max_data_value=None,
                   number_of_contour_levels=10,
                   make_data_cyclic=True,
                   polar_projection=True,
                   color_map=cc.m_rainbow,
                   zorder=0
                   ):
        """Create a polar plot of the spectral data of a single record

        Parameters
        ----------
        frame_index : int, optional
            The frame number to plot. Default = 0
        frame_datetime : :obj:`date_time`, optional
            If given, select the frame date. Default = None, i.e. the frame is selected by index
        figsize: tuple (width, height), optional
            Width and height of the frame passed to subplots. Default = None (i.e. default
            matplotlib size is taken)
        r_axis_type: {"frequency", "period"}, optional
            Type of data to plot along the radius axis.
        plot_title : str, optional
            Title of the plot. Default is None
        x_plot_title : float, optional
            x location of the title
        y_plot_title : float, optional
            y location of the title
        delta_y_titles : float, optional
            distance between the y title lines
        max_title_lines : int, optional
            Maximum of title lines to take
        title_font_size: int, optional
        r_axis_lim : (min, max)
            Limits of the radial axis
        x_time_label : float, optional
            x location of the time label
        y_time_label : float, optional
            y location of the time label
        min_data_value : float, optional
            minimum value of the contours
        max_data_value : float, optional
            maximum value of the contours
        number_of_contour_levels : int, optional
            Number of contour levels used in the plot
        make_data_cyclic: bool
            Make sure that the data is cyclic over the direction. This only works well for
            non-polar plots. With polar plot this option may cause triangulation errors in the plot
        polar_projection: bool
            Use a polar projection for the plot
        zorder:  int, optional
            Position of the contour plot. Default = 0, meaning that the contours are placed at the
            bottom and the grid and labels are visible

        Returns
        -------
        (fig, axis)
            Reference to the figure and the axis
        """

        if frame_datetime is not None:
            date_time = pd.Timestamp(frame_datetime, tz="UTC")
            min_delta_time = None
            index_delta_time = None
            for cnt, record_date_time in enumerate(self.spectral_records.keys()):
                delta_time = abs((date_time - record_date_time) / pd.Timedelta(1, "s"))
                if min_delta_time is None or delta_time < min_delta_time:
                    min_delta_time = delta_time
                    index_delta_time = cnt
            date_time = list(self.spectral_records.keys())[index_delta_time]
        else:
            try:
                date_time = list(self.spectral_records.keys())[frame_index]
            except IndexError:
                self.logger.warning("Frame index {} out of range. Skipping this plot"
                                    "".format(frame_index))
                return

        try:
            self.logger.info("Getting data at {} {}".format(date_time, type(date_time)))
            spectral_data = self.spectral_records[date_time]
        except KeyError:
            self.logger.warning(
                "Frame date/time {} not found. Skipping this plot".format(date_time))
            return

        fig, axis = spectral_data.plot(
            figsize=figsize,
            r_axis_type=r_axis_type,
            plot_title=plot_title,
            x_plot_title=x_plot_title,
            y_plot_title=y_plot_title,
            title_font_size=title_font_size,
            r_axis_lim=r_axis_lim,
            x_time_label=x_time_label,
            y_time_label=y_time_label,
            x_hs_label=x_hs_label,
            y_hs_label=y_hs_label,
            min_data_value=min_data_value,
            max_data_value=max_data_value,
            number_of_contour_levels=number_of_contour_levels,
            make_data_cyclic=make_data_cyclic,
            polar_projection=polar_projection,
            color_map=color_map,
            zorder=zorder
        )

        return fig, axis

    def animate_frames(self, figsize=None,
                       r_axis_type="period",
                       plot_title=None,
                       x_plot_title=0.05,
                       y_plot_title=0.99,
                       delta_y_titles=0.03,
                       max_title_lines=None,
                       title_font_size=10,
                       r_axis_lim=None,
                       delay_of_frames=100,
                       x_time_label=0.05,
                       y_time_label=0.94,
                       x_hs_label=0.05,
                       y_hs_label=0.89,
                       min_data_value=0,
                       max_data_value=None,
                       number_of_contour_levels=10,
                       make_data_cyclic=True,
                       polar_projection=True,
                       color_map=cc.m_rainbow
                       ):
        """ Create a polar plot of the current spectral data

        Parameters
        ----------
        figsize: tuple
            x and y size of the total figure. Default is None, so figure size is not imposed
        r_axis_type: {"frequency", "period"}, optional
            quantity to use along the radial axis. Either frequency [Hz] or period [s].
            Default = "frequency"
        plot_title: str
            Title to put in th figure. If None, use the titles found in the liftdyn file
        x_plot_title: float, optional
            x position (as a fraction of the sub plot index_title_axis). Default = 0.0
        y_plot_title: float, optional
            y position (as a fraction of the sub plot index_title_axis). Default = 1.1
        delta_y_titles: float, optional
            spacing between the lines of thet title lines. Default = 0.05
        max_title_lines: int or None
            Maximum number of title lines to plot. Default = None, ie. plot all
        title_font_size: int
            Size of the title font
        r_axis_lim: tuple or None
            The limits of the x axis. Default = None, so no limits are imposed
        make_data_cyclic: bool
            Make sure that the data is cyclic over the direction. This only works well for non-polar
            plots. With polar plot this option may cause triangulation errors in the plot
        polar_projection: bool
            Use a polar projection for the plot


        Returns
        -------
        tuple (fig, axis)
            Handle to the figure and the axis
        """

        if figsize is not None:
            size = figsize
        else:
            size = None

        if polar_projection:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size,
                                   subplot_kw=dict(projection="polar"))
        else:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size)

        if r_axis_type == "frequency":
            x_label = "Frequency [Hz]"
        elif r_axis_type == "period":
            x_label = "Peak Period [s]"
        else:
            raise AssertionError("r_axis_type can only be frequency or period. Found {}"
                                 "".format(r_axis_type))

        data_x_2d = self.freq_in_hz_2d
        data_y_2d = self.directions_2d

        if make_data_cyclic:
            data_x_2d = make_2d_array_cyclic(data_x_2d)
            data_y_2d = make_2d_array_cyclic(data_y_2d, add_constant=2 * np.pi)

        psd_2d_list = list()
        time_labels = list()
        hs_labels = list()
        changed_artists = list()
        for date_time, record in self.spectral_records.items():
            time_label = date_time.strftime("%Y-%m-%d %H:%M")
            time_labels.append(time_label)

            hs_labels.append("Hs = {:.2f} m".format(record.header_df.ix[date_time, "Hs"]))
            if make_data_cyclic:
                psd_2d = make_2d_array_cyclic(record.psd_2d)
            else:
                psd_2d = record.psd_2d
            psd_2d_list.append(psd_2d)

        number_of_frames = len(time_labels)

        # initial call to _update_plot to put of the axis. Return changed_artists with a list of all
        # items which need to be deleted every frame
        changed_artists = self._update_plot(
            frame_index=-1,
            data_x_2d=data_x_2d,
            data_y_2d=data_y_2d,
            psd_2d_list=psd_2d_list,
            fig=fig,
            ax=ax,
            r_axis_lim=r_axis_lim,
            r_axis_type=r_axis_type,
            x_label=x_label,
            x_plot_title=x_plot_title,
            y_plot_title=y_plot_title,
            x_time_label=x_time_label,
            y_time_label=y_time_label,
            title_font_size=title_font_size,
            plot_title=plot_title,
            time_labels=time_labels,
            hs_labels=hs_labels,
            changed_list=changed_artists,
            min_data_value=min_data_value,
            max_data_value=max_data_value,
            number_of_contour_levels=number_of_contour_levels,
            color_map=color_map
        )

        # the fagrs list below must exactly match the arguments of the _update_plot function, except
        # for the first argument which is the frame_index
        ani = animation.FuncAnimation(fig, self._update_plot, frames=number_of_frames,
                                      fargs=(
                                          data_x_2d,
                                          data_y_2d,
                                          psd_2d_list,
                                          fig,
                                          ax,
                                          r_axis_lim,
                                          r_axis_type,
                                          x_label,
                                          x_plot_title,
                                          y_plot_title,
                                          title_font_size,
                                          plot_title,
                                          time_labels,
                                          hs_labels,
                                          x_time_label,
                                          y_time_label,
                                          x_hs_label,
                                          y_hs_label,
                                          changed_artists,
                                          min_data_value,
                                          max_data_value,
                                          number_of_contour_levels,
                                          polar_projection,
                                          color_map
                                      ),
                                      interval=delay_of_frames, blit=False, repeat=True)

        return ani


class WaveSpectralMomentsQ2DReader(WaveSpectralDensity2DReader):
    """
    Reader class for the wave rider buy data

    Parameters
    ----------
    file_names: list
        List of file names to read.
    netcdf : :obj:`NetCDF` file link, optional
        The data read from the Fugro immediately immediately stored to a NetCDF file. The next time
        the reader is called the data can be first retrieved from the NetCDF file, which
        significantly speeds up the reading of the data
    data_base_name: str, optional
        Name of the netcdf data base. Default = "wrb" because this class most likely is used for
        Wave rider buoy data
    date_time : :obj:`date_time`, optional
        Date time object used to build the netcdf data base name. If not given, try to get it from
        the first filename
    record_data_name : str, optional
        Name of the data field as stored to the netcdf file. Default = "psd"
    n_directions : int
        Number of direction bins to use. Default = 540. The reason that is large is that the
        direction axis is running over 3 periods of 2pi to take care of the spectral periodic along
        the direction axis
    n_frequencies : int, optional
        Number of frequency bins to use. Default = 123
    time_zone: str, optional
        The time zone used to report the data. Default="UTC"
    show_read_time: bool, optional
        Show the time needed to read the data file. Default = True. Must have a logger to print

    Examples
    -------
    In order to read a series of spt file, first but the file names into a list. To demonstrate this
    with 3 files

    >>> data_location = os.path.join("..", "data", "WRB")
    >>> wrb_files = scan_base_directory(data_location, file_has_string_pattern="Mur.*",
    ...                                 extension=".spt")[:3]
    >>> wrb = WaveSpectralMomentsQ2DReader(file_names=wrb_files, n_frequencies=60, n_directions=360,
    ...                                    data_base_name="Mur", show_read_time=False)

    At this point, wrb object contains the Datawell spectra. The data can be accessed via the
    spectral_records field

    >>> for date_time, record in wrb.spectral_records.items():
    ...     print("wrb spectrum at {} has Hs = {} m".format(date_time, record.header_df["Hs"].values[0]))
    wrb spectrum at 2017-05-10 00:01:00+00:00 has Hs = 1.29 m
    wrb spectrum at 2017-05-10 00:31:00+00:00 has Hs = 1.24 m
    wrb spectrum at 2017-05-10 01:02:00+00:00 has Hs = 1.21 m

    Notes
    -----
    * The WaveSpectralMomentsQ2Reader is a  class for reading the quasi-2D wave rider buy data files
      from Datawell
    * For this data type, each individual spt file is one spectral record measured over 30 min. To
      form a time series of spectral data the spectra or collected from multiple stp files. This in
      contrast to the WaveSpectralEnergy2DReader, which reads one file of the Fugro spectral data
      containing multiple spectra
    * The method to read the spt file is based on  a Python script distributed by Datawell
    * The reader of the wrb data is quite slow, however, all constructed spectra can be dump to a
      netcdf file which can be used to read the data about 100 x faster the next run.
      Please have a look at the examples for a demonstration

      https://gist.github.com/seumasmorrison/5935386
    """

    def __init__(self,
                 file_names,
                 time_zone="UTC",
                 n_frequencies=123,
                 n_directions=540,
                 data_base_name="wrb",
                 netcdf=None,
                 date_time=None,
                 record_data_name="psd",
                 show_read_time=True
                 ):

        if date_time is None:
            first_file = file_names[0]
            try:
                date_time = dparser.parse(os.path.splitext(first_file)[0], fuzzy=True)
            except ValueError:
                date_time = None

        self.show_read_time = show_read_time

        # inherit the SeaStateData object
        super(WaveSpectralMomentsQ2DReader, self).__init__(
            data_base_name=data_base_name,
            netcdf=netcdf,
            date_time=date_time,
            time_zone=time_zone,
            n_frequencies=n_frequencies,
            n_directions=n_directions,
            record_data_name=record_data_name,
        )

        # analyse file name and check extension
        self.file_names = file_names
        # set the file name based on the first file in the list
        self.file_name = file_names[0]
        self.file_base, self.file_extension = os.path.splitext(self.file_name)
        if self.file_extension not in WRB_FILE_EXTENSION:
            raise IOError("File type of file {} is {}. Not of type {}"
                          "".format(self.file_name, self.file_names, WRB_FILE_EXTENSION))

        self.open_net_cdf_data_base()

        # read the current fugro csv file

        if self.netcdf is not None and self.netcdf.read_stored_data:
            get_records_from_netcdf = True
        else:
            get_records_from_netcdf = False
        if get_records_from_netcdf and self.record_data_name in list(
                self.netcdf.dataset.variables.keys()):
            with Timer(verbose=False, units="s") as t:
                self.read_spectral_records_from_netcdf(close_after_reading=False)
            if not self.no_info:
                self.logger.info("Read the wave spectral moments from {} in {:.1f} s"
                                 "".format(self.netcdf.file_name_ds, t.secs))

        # read the stp files only if we have not loaded the data yet
        if self.file_extension == ".spt":
            self.read_stp_files()

        self.n_records = len(self.spectral_records.keys())

    def read_stp_files(self):
        """
        Loop over the file name list containing the wave rider buy data. Each file contains one
        single spectrum.
        """

        sea_state_list = list()
        for file_name in self.file_names:

            self.file_name = file_name
            try:
                self.date_time = dparser.parse(os.path.splitext(self.file_name)[0], fuzzy=True)
            except ValueError:
                self.date_time = None
            self.file_base, self.file_extension = os.path.splitext(self.file_name)
            if self.file_extension not in WRB_FILE_EXTENSION:
                raise IOError("File type of file {} is {}. Not of type {}"
                              "".format(self.file_name, self.file_name, WRB_FILE_EXTENSION))

            if self.netcdf is not None:
                hours_since = nc.date2num(pd.to_datetime(self.date_time),
                                          units=self.netcdf.dataset.variables["time"].units,
                                          calendar=self.netcdf.dataset.variables["time"].calendar)
                time_var = self.netcdf.dataset.variables["time"][:]
                time_index = np.where(time_var[:] == hours_since)[0]
                if time_index.size > 0:
                    time_value = time_var[time_index[0]]
                    dt = nc.num2date(time_value,
                                     units=self.netcdf.dataset.variables["time"].units,
                                     calendar=self.netcdf.dataset.variables["time"].calendar)
                    self.logger.debug("Time index {}  ({}/{}) already in data set. Skipping file {}"
                                      "".format(time_index[0],
                                                time_value,
                                                dt,
                                                file_name))
                    continue
            else:
                hours_since = None

            self.logger.debug("Reading wave spectral moment data from file {}".format(file_name))
            record = WaveSpectralMomentsQ2DRecord(file_name, n_frequencies=self.n_frequencies,
                                                  n_directions=self.n_directions,
                                                  netcdf=self.netcdf,
                                                  record_data_name=self.record_data_name,
                                                  hours_since=hours_since,
                                                  show_read_time=self.show_read_time)

            if self.freq_in_hz is None:
                self.freq_in_hz = record.freq_in_hz
                self.directions_deg = record.directions_deg
                self.freq_in_hz_2d = record.freq_in_hz_2d
                self.directions_2d = record.directions_2d
            sea_state_list.append(record.header_df)
            record_date_time = pd.Timestamp(record.header_df.index[0], tz=self.time_zone)

            # store this record to the dictionary
            self.spectral_records[record_date_time] = record

            if self.netcdf is not None and self.netcdf.write_new_data:
                self.add_spectral_record_to_netcdf(record_date_time, record)

        if sea_state_list:
            sea_state_df = pd.concat(sea_state_list)
            if self.sea_states_df is None:
                # we have an empty sea state data frame, so just impose the new list if available
                self.sea_states_df = sea_state_df
            else:
                # we have an sea state data frame already, so just append the new list if available
                self.sea_states_df = pd.concat([self.sea_states_df, sea_state_df])

        if self.sea_states_df is not None and self.netcdf is not None:
            self.sea_states_df.index = \
                [pd.Timestamp(tme, tz=self.time_zone) for tme in self.sea_states_df.index]
            self.sea_states_df.sort_index(inplace=True)
            self.logger.debug("Updating {}".format(self.netcdf.file_name_mp))
            self.sea_states_df.to_msgpack(self.netcdf.file_name_mp, compress="zlib")

    def add_spectral_record_to_netcdf(self, record_date_time, record):
        """
        Add a spectral record to the netcdf file

        record_date_time: :obj:`date_time`
            Date time of the current record to add to the netcdf file
        record: :obj:`WaveSpectralMomentsQ2DRecord`
            Reference to the wave recods with spectrum to store
        """

        if "psd" not in list(self.netcdf.dataset.variables.keys()):
            self.logger.debug("Creating new CDF variable {}".format(self.netcdf_variable_name))
            self.netcdf.dataset.createVariable("psd", "f8", ("time", "freq_in_hz", "direction_deg"))

        self.logger.debug("appending {} to {}".format(record_date_time, self.record_data_name))
        self.netcdf.append_data(record_date_time=record_date_time,
                                record_data=record.psd_2d,
                                record_data_name=self.record_data_name)


class WaveSpectralEnergy2DReader(WaveSpectralDensity2DReader):
    """ Reader Class for a Fugro/MetOcean spectral time series.

    Parameters
    ----------
    file_name : str
        Name of the file to read with the spectral data. Must be of type .csv (Fugro), .nc (netcdf)
        or .oct (MetOcean)
    data_base_name: str, optional
        Name to impose to the data base. Default = None
    time_zone : str, optional
        Name of the time zone of the date/time in the data base.  Default = "UTC". In case an
        different time zone is needed, the official Time zone naming should be taken. For example
        "Europe/Amsterdam". For a complete list of time zone see
        https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

    n_frequencies: int, optional
        Number of frequency bins to use in the resampled spectrum. Default = None
    n_directions : int, optional
        Number of direction bins to use in the resampled spectrum. Default = None
    netcdf : :obj:`NetCDF` file link, optional
        The data read from the Fugro immediately immediately stored to a NetCDF file. The next time
        the reader is called the data can be first retrieved from the NetCDF file, which
        significantly speeds up the reading of the data
    date_time : :obj:`date_time`, optional
        Date time object
    record_data_name : str, optional
        Name of the data field as stored to the netcdf file. Default = "psd" (Power Spectral Density)
    location_check : :obj:`LocationCheck`, optional
        Fugro sometimes yields data files with a different location then requested. Default = None,
        which means no location check it carried out
        
        This argument allows to pass the :obj:`LocationCheck` object from the `hmc_utils` packages
        and check the location of the current Fugro file with the anticipated location stored in the
        :obj:`LocationCheck` object. If the location of the Fugro file is out of the specified
        range, raise a ValueError

    Notes
    -----
    * A data csv file contains a series of spectra, which are read by the SpectralRecord class.
    * The Fugro data is put on a non-uniform mesh and the values are power spectral energies
      (because the value is the psd x bin_size in the Fugro file already).
      We take care of this by converting the Fugro data to a a power spectral density and then
      interpolating the data on a uniform mesh

    Raises
    ------
    ValueError
        In case that a Location Check failed
    """

    def __init__(self,
                 file_name,
                 data_base_name="default",
                 time_zone="UTC",
                 n_frequencies=123,
                 n_directions=181,
                 netcdf=None,
                 date_time=None,
                 record_data_name="psd",
                 location_check=None):

        if date_time is None:
            # if the date time is not give try to get is from te filename
            try:
                date_time = dparser.parse(os.path.splitext(file_name)[0], fuzzy=True)
            except ValueError:
                date_time = None

        # inherit the SeaStateData object
        super(WaveSpectralEnergy2DReader, self).__init__(
            data_base_name=data_base_name,
            netcdf=netcdf,
            date_time=date_time,
            time_zone=time_zone,
            n_frequencies=n_frequencies,
            n_directions=n_directions)

        self.record_data_name = record_data_name

        self.location_check = location_check

        # analyse file name and check extension
        self.file_name = file_name
        self.file_base, self.file_extension = os.path.splitext(self.file_name)
        if self.file_extension not in TRANS_SPEC2D_FILE_EXTENSION:
            raise IOError("File type of file {} is {}. Not of type {}"
                          "".format(self.file_name, self.file_name, TRANS_SPEC2D_FILE_EXTENSION))

        if self.location_check is not None:
            # for the Fugro data we may want to do a check on the location. For that, start with
            # reading the header
            with open(self.file_name, "r") as fp:

                # start with reading the header of a Fugro spectral forecast file
                self.read_header(fp)

                # do the location check in order to prevent unnecessary making of a netcdf  file
                if None not in (self.longitude, self.latitude):
                    if self.location_check.out_of_range(self.latitude, self.longitude):
                        raise ValueError("Location {} out of range with {} km"
                                         "".format(self.location_check.current_location,
                                                   self.location_check.distance))

        # we have passed the location check. Now we can open the netcdf file
        self.open_net_cdf_data_base()

        # the frequencies and directions of the non-form mesh
        self.n_frequencies_nu = None
        self.n_directions_nu = None
        self.frequencies_nu = None
        self.directions_nu = None

        # read the current fugro csv file

        if self.netcdf is not None and self.netcdf.read_stored_data:
            get_records_from_netcdf = True
        else:
            get_records_from_netcdf = False
        if get_records_from_netcdf and self.record_data_name in list(
                self.netcdf.dataset.variables.keys()):
            self.logger.info("Reading the wave spectral energy from netcdf {}"
                             "".format(self.netcdf.file_name_ds))
            self.read_spectral_records_from_netcdf()
        else:
            self.logger.info("Reading the wave spectral energy from csv {}".format(self.file_name))
            self.read_wave_spectrum_energy2d_csv_file()

    def add_spectral_record_to_netcdf(self, record_date_time, record):
        """
        add a spectral record to the netcdf file
        :param record_date_time: 
        :param record: 
        :return: 
        """

        if "psd" not in list(self.netcdf.dataset.variables.keys()):
            self.logger.debug("Creating new CDF variable {}".format(self.netcdf_variable_name))
            self.netcdf.dataset.createVariable("psd", "f8", ("time", "freq_in_hz", "direction_deg"))

        self.netcdf.append_data(record_date_time=record_date_time,
                                record_data=record.psd_2d,
                                record_data_name=self.record_data_name)

    def read_wave_spectrum_energy2d_csv_file(self):
        """ The implementation of the fugro spectral data reader"""

        # keep a list of the sea state data frames
        sea_state_list = list()

        with open(self.file_name, "r") as fp:

            # read the header again to make sure the file pointer is put on the right position
            self.read_header(fp)

            # make a report in debugging mode
            if self.logger.getEffectiveLevel() == logging.DEBUG:
                self.make_report()

            # loop over all the records and collect them in a list
            for i_rec in range(self.n_records):
                record = WaveSpectralEnergy2DRecord(fp,
                                                    n_frequencies=self.n_frequencies,
                                                    n_directions=self.n_directions,
                                                    n_directions_nu=self.n_directions_nu,
                                                    n_frequencies_nu=self.n_frequencies_nu,
                                                    netcdf=self.netcdf,
                                                    record_data_name=self.record_data_name)
                if self.frequencies_nu is None:
                    self.frequencies_nu = record.freq_in_hz_nu
                    self.directions_nu = record.directions_deg_nu
                if self.freq_in_hz is None:
                    self.freq_in_hz = record.freq_in_hz
                    self.directions_deg = record.directions_deg
                if self.freq_in_hz_2d is None:
                    self.freq_in_hz_2d = record.freq_in_hz_2d
                    self.directions_2d = record.directions_2d
                sea_state_list.append(record.header_df)
                record_date_time = pd.Timestamp(record.header_df.index[0], tz=self.time_zone)

                # store this record to the dictionary
                self.spectral_records[record_date_time] = record

                if self.netcdf is not None and self.netcdf.write_new_data:
                    self.add_spectral_record_to_netcdf(record_date_time, record)

        # done with reading. No collect all the sea states
        self.sea_states_df = pd.concat(sea_state_list)
        self.sea_states_df.index = \
            [pd.Timestamp(tme, tz=self.time_zone) for tme in self.sea_states_df.index]
        if self.netcdf is not None:
            # only if we have a netcdf file opended we should close it now
            self.netcdf.dataset.close()
            # also dump the sea states to a message pack binary file
            self.logger.debug("Updating {}".format(self.netcdf.file_name_mp))
            self.sea_states_df.to_msgpack(self.netcdf.file_name_mp)

    def read_header(self, fp):
        """
        Read the header of the fugro file
        :param fp: file pointer
        :return: 
        """

        # get date time of first line
        line = fp.readline().strip()
        match = re.search("(\d+)", line)
        if bool(match):
            date_time_string = match.group(1)
            self.valid_for = dparser.parse(date_time_string, fuzzy=True)

        self.n_frequencies_nu = int(fp.readline().strip().split(",")[1])
        self.n_directions_nu = int(fp.readline().strip().split(",")[1])
        self.n_records = int(fp.readline().strip().split(",")[1])
        self.latitude = float(fp.readline().strip().split(",")[1])
        self.longitude = float(fp.readline().strip().split(",")[1])
        self.depth = float(fp.readline().strip().split(",")[1])

        # empty line at the end of the header
        fp.readline()


class WaveSpectralComponentsReader(WaveSpectralDensity2DReader):
    """Reader class to the wave spectral components

    Parameters
    ----------
    file_name : str, optional
        Name of the excel file containing the wave spectral components Hs, Tp, theta for wind and
        swell waves per time step
    sea_state_data_base: :obj:`DataFrame`, optional
        The DataFrame containing the sea states as would be read from the file_name. This give the
        possibility to read the data base outside of the reader and pass the reference to this
        reader class to create the spectra
    header_name: str, optional
        Name of the heading field. It is also used to calculate it from the LAt/Lon
    latitude_name: str, optional
        Name of latitude column. Default = Latitude
    longitude_name: str, optional
        Name of longitude column. Default = Lontitude
    time_zone : str, optional
        Time zone name. Default = "UTC",
    n_frequencies : int, optional
        Number of frequency bins to use for the frequecy axis of 2D spectrum. Default = 123
    n_directions : int, optional
        Number of direction bins to use for the direction axis of the 2D spectrum. Default = 60
    min_freq_in_hz: float, optional
        Minimum frequency in Hz. Default = 0.02 Hz
    max_freq_in_hz: float, optional
        Maximum frequency in Hz. Default = 2.0 Hz
    min_dir_in_deg: float, optional
        Min direction in degrees. Default = 0 deg
    max_dir_in_deg: float, optional
        Min direction in degrees. Default = 360 deg (not closed interval is used
    freq_in_hz: ndarray, optional
        If this arrays with frequencies in hz is passed, this will be taking leading: the
        n_frequencies, min_freq_in_hz, and max_freq_in_hz will be imposed based on this array.
        Default is None. Note that the frequencies must be in Hz
    directions_deg: ndarray, optional
        If this arrays with directions in deg is passed, this will be taking leading: the
        *n_directions*, *min_dir_in_deg*, and *max_dir_in_deg* will be imposed based on this array.
        Default is None. Note that the direction must be in degrees.
    data_base_name: str, optional
        Name for the data base to store. Default = None
    netcdf: :obj:`NetCDFInfo`
        object holding all the information of the NetCDF file which can be used to retrieve or store
        the data from/to
    date_time: :obj:`date_time`
        Date/time of the current spectral record
    record_data_name: str
        Name to give to the stored data in the netcfd file. Default = "psd"
    data_sheet_name: str, optional
        Name of the sheet in the excel file `file_name` that holds the data. Default = "General"
    spectral_components : dict, optional
        Spectral component data base. This dictionary may contain information on each component
        required to turn it into an 2D spectrum. Default = None, which means that the default values
        are assumed at stored in the *SPECTRAL_COMPONENTS* dictionary stored as a global variable in
        the module. You can have a look at this dictionary to see how the spectral components can
        be defined
    column_prefix : str, optional
        All the components that are found in the input excel file are stored in the header_df data
        frame in the column with the same name as in the excel file. In the case that this name
        happens to have the same name as the internally created values by the set_psd_properties
        method ("Hs", "Tp", "Theta0", "HsTp2"), you may change the column names by defining a
        column_prefix. Default = "", so no prefix is added

    Attributes
    ----------

    directions: ndarray
        After the directions_deg ndarray is created, the direction in rad is stored in this array
        Note that the PSD is based on the frequency and direction in Hz and degrees.
    freq_in_hz: ndarray
        After the freqyencies_in_hz ndarray is created, the frequencies in rad/s is stored in this
        array. Note that the PSD is based on the frequency and direction in Hz and degrees.
    psd_2d: ndarray
        A *n_directions* x *n_frequencies* array with the Power spectral denstiy of the constructed
        wave spectrum based on the *wave_components*
    """

    def __init__(self,
                 file_name=None,
                 sea_state_data_base=None,
                 time_zone="UTC",
                 wave_direction_coming_from=True,
                 wave_direction_relative_to_heading=True,
                 n_frequencies=123,
                 n_directions=60,
                 min_freq_in_hz=0.02,
                 max_freq_in_hz=2.0,
                 min_dir_in_deg=0,
                 max_dir_in_deg=360,
                 freq_in_hz=None,
                 directions_deg=None,
                 data_base_name=None,
                 netcdf=None,
                 date_time=None,
                 record_data_name="psd",
                 data_sheet_name="General",
                 heading_name="Heading",
                 latitude_name="Latitude",
                 longitude_name="Longitude",
                 travel_distance_name="travel_distance",
                 spectral_components=None,
                 column_prefix=""
                 ):

        if date_time is None and file_name is not None:
            try:
                # if the date time is not give try to get is from te filename
                date_time = dparser.parse(os.path.splitext(file_name)[0], fuzzy=True)
            except ValueError:
                date_time = None

        # inherit the SeaStateData object
        super(WaveSpectralComponentsReader, self).__init__(
            data_base_name=data_base_name,
            netcdf=netcdf,
            date_time=date_time,
            time_zone=time_zone,
            n_frequencies=n_frequencies,
            n_directions=n_directions,
            min_freq_in_hz=min_freq_in_hz,
            max_freq_in_hz=max_freq_in_hz,
            min_dir_in_deg=min_dir_in_deg,
            max_dir_in_deg=max_dir_in_deg,
            freq_in_hz=freq_in_hz,
            directions_deg=directions_deg,
            record_data_name=record_data_name
        )

        # analyse file name and check extension
        self.file_name = file_name
        self.sea_state_data_base = sea_state_data_base

        self.wave_direction_coming_from = wave_direction_coming_from
        self.wave_direction_relative_to_heading = wave_direction_relative_to_heading
        self.heading_name = heading_name
        self.longitude_name = longitude_name
        self.latitude_name = latitude_name
        self.travel_distance_name = travel_distance_name

        self.data_sheet_name = data_sheet_name
        if spectral_components is not None:
            self.spectral_components = spectral_components
        else:
            self.logger.info("No spectral components are supplied by the user. Imposing defaults")
            self.logger.debug("{}".format(SPECTRAL_COMPONENTS))
            self.spectral_components = SPECTRAL_COMPONENTS
        # set the file name based on the first file in the list

        if self.file_name is not None:
            self.file_base, self.file_extension = os.path.splitext(self.file_name)
            if self.file_extension not in SPEC_COMP_EXTENSION:
                raise IOError("File type of file {} is {}. Not of type {}"
                              "".format(self.file_name, self.file_extension, SPEC_COMP_EXTENSION))

        self.column_prefix = column_prefix

        # spectral buy does not have a location check. open it now
        if netcdf is not None:
            self.open_net_cdf_data_base()

        # read the current fugro csv file
        if self.netcdf is not None and self.netcdf.read_stored_data:
            get_records_from_netcdf = True
        else:
            get_records_from_netcdf = False
        if get_records_from_netcdf and self.record_data_name in list(
                self.netcdf.dataset.variables.keys()):
            self.logger.info("Reading the wave spectral moments from netcdf {}"
                             "".format(self.netcdf.file_name_ds))
            self.read_spectral_records_from_netcdf(close_after_reading=False)
        else:
            self.read_spectral_components()
            self.calculate_relative_wave_direction()
            self.create_spectral_records()

    def read_spectral_components(self):
        """
        The implementation of the spectral component reader
        """

        if self.sea_state_data_base is None:
            # if the state state data base was not passed as an argument -> read it from file
            self.sea_state_data_base = \
                pd.read_excel(self.file_name, sheet_name=self.data_sheet_name, index_col=0)

        # set a time zone to the date time stamps
        self.logger.debug("Found index {}".format(self.sea_state_data_base.index))
        self.sea_state_data_base.index = [pd.Timestamp(dt, tz=self.time_zone) for dt in
                                          self.sea_state_data_base.index]
        self.logger.debug("Now index {}".format(self.sea_state_data_base.index))
        start_date_time = self.sea_state_data_base.index[0]
        self.date_time = start_date_time

        if self.heading_name not in self.sea_state_data_base.columns.values:
            # if the heading field is not present, calculate it now
            self.sea_state_data_base = \
                travel_distance_and_heading_from_coordinates(self.sea_state_data_base,
                                                             latitude_name=self.latitude_name,
                                                             longitude_name=self.longitude_name,
                                                             heading_name=self.heading_name,
                                                             travel_distance_name=
                                                             self.travel_distance_name)

    def calculate_relative_wave_direction(self):
        """
        Store the relative direction with the respect to the heading

        Notes
        -----
        * In savetrans the wave direction means 'going_to' in argos 'coming_from',
        * the argos # wave direction should be shifted 180 to get a normal definition
        * Also, note that the RAO direction definition is defined via the counter-clockwise angle
        * A relative wave direction of 90 means that the waves are going to the west (90) if the
          bow is to the north (0)
        * In this method the relative wave direction is calculated
        * If the *wave_relative_to_headings* flag is False we just assume we want the wave direction
          w.r.t. the north (heading =0)
        """
        if self.wave_direction_coming_from:
            offset = 0
        else:
            offset = 180

        df = self.sea_state_data_base
        for name, component_values in self.spectral_components.items():

            try:
                theta0_name = component_values["Theta0"]
            except KeyError as err:
                raise KeyError("Spectral component '{}' does not have one of have its defined "
                               "direction field {}. Skipping".format(name, err))
            else:
                rel_theta_name = "_".join([RELATIVE_DIRECTION_PREFIX, theta0_name])
                if self.wave_direction_relative_to_heading:
                    headings = df[self.heading_name]
                else:
                    # just take the heading 0 if you don't want to have a relative wave direction
                    headings = 0.0
                try:
                    df[rel_theta_name] = (headings - (df[theta0_name] - offset)) % 360
                except KeyError:
                    self.logger.debug("rel_direction name not calculated for wave {} {}"
                                      "".format(name, theta0_name))

    def create_spectral_records(self):
        """
        The implementation of the spectral component reader
        """
        # keep a list of the sea state data frames
        sea_state_list = list()

        for date_time, row in self.sea_state_data_base.iterrows():

            if self.netcdf is not None:
                hours_since = nc.date2num(pd.to_datetime(date_time),
                                          units=self.netcdf.dataset.variables["time"].units,
                                          calendar=self.netcdf.dataset.variables["time"].calendar)
                time_var = self.netcdf.dataset.variables["time"][:]
                time_index = np.where(time_var[:] == hours_since)[0]
                if time_index.size > 0:
                    # time index contains the location of the current hourse_since in the netcdf
                    # file if it was found. This means we have already processed this time set so we
                    # can continue
                    time_value = time_var[time_index[0]]
                    dt = nc.num2date(time_value,
                                     units=self.netcdf.dataset.variables["time"].units,
                                     calendar=self.netcdf.dataset.variables["time"].calendar)
                    self.logger.debug("Time index {}  ({}/{}) already in data set. Skipping {}"
                                      "".format(time_index[0], time_value, dt, date_time))
                    continue
            else:
                hours_since = None

            # If we get here, no entry exist yet of the current date time in the netcdf file. Add
            # the spectrum
            record = WaveSpectralComponent2DRecord(current_date_time=date_time,
                                                   current_sea_state_values=row,
                                                   n_frequencies=self.n_frequencies,
                                                   n_directions=self.n_directions,
                                                   min_freq_in_hz=self.min_freq_in_hz,
                                                   max_freq_in_hz=self.max_freq_in_hz,
                                                   min_dir_in_deg=self.min_dir_in_deg,
                                                   max_dir_in_deg=self.max_dir_in_deg,
                                                   freq_in_hz=self.freq_in_hz,
                                                   directions_deg=self.directions_deg,
                                                   netcdf=self.netcdf,
                                                   record_data_name=self.record_data_name,
                                                   hours_since=hours_since,
                                                   spectral_components=self.spectral_components,
                                                   column_prefix=self.column_prefix
                                                   )

            if record.n_added_components == 0:
                self.logger.debug("No valid spectral components were added. Do not store but skip")
                continue

            self.logger.debug("Created 2D spectrum based on Hs/Tp info at {}".format(date_time))

            if self.freq_in_hz is None:
                self.freq_in_hz = record.freq_in_hz
                self.directions_deg = record.directions_deg
                self.directions = record.directions
                self.freq_in_hz_2d = record.freq_in_hz_2d
                self.directions_2d = record.directions_2d
            sea_state_list.append(record.header_df)
            record_date_time = pd.Timestamp(record.header_df.index[0], tz=self.time_zone)

            # store this record to the dictionary
            self.spectral_records[record_date_time] = record

            if self.netcdf is not None and self.netcdf.write_new_data:
                has_started_writing_to_netcdf = True
                self.add_spectral_record_to_netcdf(record_date_time, record)

        if sea_state_list:
            sea_state_df = pd.concat(sea_state_list)
            if self.sea_states_df is None:
                # we have an empty sea state data frame, so just impose the new list if available
                self.sea_states_df = sea_state_df
            else:
                # we have an sea state data frame already, so just append the new list if available
                self.sea_states_df = pd.concat([self.sea_states_df, sea_state_df])

        if self.sea_states_df is not None:
            self.sea_states_df.index = \
                [pd.Timestamp(tme, tz=self.time_zone) for tme in self.sea_states_df.index]
            self.sea_states_df.sort_index(inplace=True)
            # self.logger.debug("Updating {}".format(self.netcdf.file_name_mp))
            # self.sea_states_df.to_msgpack(self.netcdf.file_name_mp, compress="zlib")

    def add_spectral_record_to_netcdf(self, record_date_time, record):
        """
        add a spectral record to the netcdf file
        :param record_date_time: 
        :param record: 
        :return: 
        """

        if "psd" not in list(self.netcdf.dataset.variables.keys()):
            self.logger.debug("Creating new CDF variable {}".format(self.netcdf_variable_name))
            self.netcdf.dataset.createVariable("psd", "f8", ("time", "freq_in_hz", "direction_deg"))

        self.logger.debug("appending {} to {}".format(record_date_time, self.record_data_name))
        self.netcdf.append_data(record_date_time=record_date_time,
                                record_data=record.psd_2d,
                                record_data_name=self.record_data_name)

    def report_all_components(self, record_index=None, record_datetime=None,
                              component_to_return=None, quiet=False):
        """
        Make a report of all the sea state values per date/time

        Parameters
        ----------
        record_index: int
            If given, only report the sea state components for this index. Default = None, which means that
            all components are given
        record_datetime: date_time
            If given, only report the sea state components for this date_time. Default = None, which
            means that either the record with record_index is report or all records are reported
        component_to_return: str
            Name of the component to return
        quiet: bool
            Do not report to screen, only return the values

        Returns:
        DataFrame or None
            DataFrame containing the values for Hs, Tp, and Theta0 in the columns for the
            *component_to_return* specified or None if not requested


        """

        component_sea_states_list = list()
        date_time_list = list()
        for cnt, (date_time, record) in enumerate(self.spectral_records.items()):
            if record_datetime is not None and date_time != record_datetime:
                continue
            elif record_index is not None and cnt != record_index:
                continue
            if not quiet:
                self.logger.info("Properties at {}".format(date_time))
            returned_values = record.report_components(component_to_return, quiet)
            if None not in returned_values:
                component_sea_states_list.append(returned_values)
                date_time_list.append(date_time)

        if len(component_sea_states_list) > 0:
            components_df = pd.DataFrame(index=date_time_list, columns=["Hs", "Tp", "Theta0"],
                                         data=np.array(component_sea_states_list))
        else:
            components_df = None

        return components_df


class ResponseSpectra(object):
    """
    Class to hold the response spectra

    Parameters
    ----------
    name: str
        Name to give to this response spectrum

    Attributes
    ----------
    spectra_per_rao_comp : :obj:`OrderedDict`
        This dictionary holds the response spectra per RAO component
    statistics_per_rao_comp = :obj:`OrderedDict`
        This dictionary holds the statistics per RAO component
    """

    def __init__(self, name):
        self.name = name

        # this is going to hold the spectra time series PER RAO
        self.spectra_per_rao_comp = OrderedDict()
        self.statistics_per_rao_comp = OrderedDict()


class SACSForceMomentVs6DOFReader(object):
    """
    A class to read and hold the data sacs output file with the forces and moments vs
    the 6 degree of freedom accelerations

    Parameters
    ----------
    file_name: str
        Name of the SACS output file
    n_header_lines: int
        Number of header lines before the elements start. Default = 8

    Attributes
    ----------
    header_lines: list
        List of the header lines  of the data file
    elements:  dict
        Dictionary containing the properites per element stored in a namedtuple *SACS_ELEMENT*

    Examples
    --------

    In order to read a SACS output file containing the forces and moments per loading condition
    *AX*, *AY*, *AZ*, *RX*, *RY*, *RZ* do the following

    >>> logger = create_logger(console_log_format_clean=True, console_log_level=logging.INFO)
    >>> file_name = os.path.join("..", "data", "AccelCoeffHEXTravBlockhigh75.txt")
    >>> sacs_obj = SACSForceMomentVs6DOFReader(file_name=file_name)

    The object *sacs_obj* contains all the information stored in the data file at this moment. Note
    that by creating a logger first we can use the reporting methods as well

    The *elements* attribute is a dictionary with the properties per element dictionary.
    The dictionary keys are the *member_number* fields

    >>> elements = sacs_obj.elements
    >>> print(elements.keys())
    odict_keys(['SADP_5240', 'SADS_5110', 'SHZ3_WT18', '2140_WT2K', '2210_WT2J', 'SQ02_Q109', 'SRX4_3975', 'SSW5_1330'])

    For example we can extract one element with the number *SADP_5240*

    >>> element_adp = elements["SADP_5240"]

    This element is a *namedtuple* with the following fields

    >>> print(element_adp.member_number)
    SADP-5240
    >>> print(element_adp.member_end)
    SADP
    >>> print(element_adp.group_id)
    AD4

    Each elements contains the forces and moments per loading which are stored in the dictionary
    *forces* and *moments*. To access the forces belonging to the case loading RX for instance
    we can do

    >>> forces = element_adp.forces["RX"]
    >>> print(forces)
    [-6975.7212   -23.4294    21.529 ] kilonewton

    Note that *forces* contains a ndarray with a Pint quantity attached to it so we can see that
    the forces are specified in kN. If you want to have bare numpy array with the force you can
    add the magnitude attribute

    >>> print(forces.magnitude)
    [-6975.7212   -23.4294    21.529 ]

    The forces per loading case are stored in the same way for the key fields

    >>> print(element_adp.forces.keys())
    odict_keys(['AX', 'AY', 'AZ', 'RX', 'RY', 'RZ'])

    The moments per load case can be retrieved in a the same way. If you want to have the moments
    of the AZ load case you can do

    >>> moments = element_adp.moments["AZ"]
    >>> print(moments)
    [  -3.6621  -20.4896  817.5861] kilonewton * meter

    The dimension is now kN*m. The bare numpy array without dimension is again obtained with the
    *magnitude*  attribute.

    If you want to report all the information of the elemement you can do

    >>> sacs_obj.report_element("SADP_5240")
    ******************** SADP_5240 ********************
    Member Number        : SADP-5240
    Member End           : SADP
    Group ID             : SADP
    Forces for loading AX : [ -1.17716377e+04   4.67898300e+02  -9.64140000e+00] kilonewton
    Forces for loading AY : [  6.65330860e+03   3.23400000e-01   2.43444100e+02] kilonewton
    Forces for loading AZ : [-2168.4492  -186.2302     5.7168] kilonewton
    Forces for loading RX : [-6975.7212   -23.4294    21.529 ] kilonewton
    Forces for loading RY : [ -1.26253115e+04   5.87595000e+01  -7.27590000e+00] kilonewton
    Forces for loading RZ : [-921.2621  -58.2444  -99.3361] kilonewton
    Moment for loading AX : [    3.0504    38.8341 -2058.1665] kilonewton * meter
    Moment for loading AY : [   48.7468 -1178.3005    -5.1257] kilonewton * meter
    Moment for loading AZ : [  -3.6621  -20.4896  817.5861] kilonewton * meter
    Moment for loading RX : [ -59.9805   32.7362  107.7579] kilonewton * meter
    Moment for loading RY : [   2.3286   29.5492 -303.5936] kilonewton * meter
    Moment for loading RZ : [ -16.1804  453.7821  257.3843] kilonewton * meter
    <BLANKLINE>
    <BLANKLINE>

    In case you want to have an overview of all the load cases of all the elements loaded you can
    use the *make_report* method, which loops over all the loaded elements and runs the
    *report_element* method per elememt

"""

    def __init__(self, file_name, n_header_lines=8):
        self.logger = get_logger(__name__)

        self.file_name = file_name

        # analyse file name and check extension
        self.file_name = file_name
        self.file_base, self.file_extension = os.path.splitext(self.file_name)

        self.n_header_lines = n_header_lines
        self.header_lines = list()

        self.elements = OrderedDict()

        self.read_sacs_file()

    def read_sacs_file(self):
        """
        Read the sacs file
        """
        with open(self.file_name, "r") as fp:
            # first store the header lines
            self.read_header(fp)

            # now continue to loop over the elements
            read_elements = True
            while read_elements:
                try:
                    element = self.read_element(fp)
                except IndexError:
                    self.logger.debug("Found last element. Stop reading after {} "
                                      "elements".format(len(self.elements.keys())))
                    read_elements = False
                else:
                    key_name = re.sub("\s+|-", "_", element.member_number)
                    self.elements[key_name] = element

    def read_header(self, fp):
        """
        Read the header of the sacs data file and store it in the *header* attribute field

        Parameters
        ----------
        fp: TextIOWrapper
            Reference to the sacs file buffer open with the *open* method
        """

        for nr in range(self.n_header_lines):
            line = fp.readline().strip()
            self.header_lines.append(line)
            self.logger.debug("{}".format(line))

    def read_element(self, fp):
        """
        Function to read one element with 12 lines (2 x 6 DOF).

        Parameters
        ----------
        fp: TextIOWrapper
            Reference to the sacs file buffer open with the *open* method

        Returns
        -------
        namedtuple:
            The properties of this element are stored in the named tuple *SACS_ELEMENT* with the
            fields:

            - *member_number* : str
                Unique member name
            - *member_end* : str
                Name of the end
            - *group_id*:  str
                Name of the group
            - *forces*:  ndarray
                The force vector *(FX, FY, FZ)*
            - *moments*: ndarray
                The moment vector *(MX, MY, MZ)*

        Notes
        -----

        In the file to read the element has the following structure::

         SADP-5240    SADP     AD4      AX   -11771.6377      467.8983       -9.6414        3.0504       38.8341    -2058.1665
                                        AY     6653.3086        0.3234      243.4441       48.7468    -1178.3005       -5.1257
                                        AZ    -2168.4492     -186.2302        5.7168       -3.6621      -20.4896      817.5861
                                        RX    -6975.7212      -23.4294       21.5290      -59.9805       32.7362      107.7579
                                        RY   -12625.3115       58.7595       -7.2759        2.3286       29.5492     -303.5936
                                        RZ     -921.2621      -58.2444      -99.3361      -16.1804      453.7821      257.3843
                      5240              AX   -11748.7910      525.4674       -9.6414        3.0504       -1.6515       27.4725
                                        AY     6653.3086        0.3233      305.3813       48.7468      -26.0041       -3.7679
                                        AZ    -2110.8799     -209.0779        5.7168       -3.6621        3.5162      -12.3902
                                        RX    -6968.5703      -26.2676      -14.7362      -59.9805       48.5036        3.4157
                                        RY   -12598.2412       87.0323       -7.2758        2.3286       -1.0030        0.8865
                                        RZ     -924.1003      -65.3959     -114.0690      -16.1804        5.1261       -2.2070

        The  first line contains the *member_number* as used in sacs, followed by the *member_end*
        and the *groud_id*. Then the next columns contains the *load_condition* (*AX*, *AY*, *AZ*,
        *RX*, *RY*, or *RZ*)

        The next 6 columns contain the 3 forces *FX*, *FY*, *FZ* in kN followed by the 3 moments
        *MX*, *MY*, *MZ* in kN-m belong to the *load_condition*

        The name and force and moment vectors are stored in the dictionary of the named tuple
        *SACS_ELEMENT* *forces* and *moment*

        Note that each element has 2 x 6 DOF: one for each side of a element. In this rouitine
        only the first side is read because that is the only relevant side. The last 6 rows are
        therefore discarded

        """
        forces_per_load_condition = OrderedDict()
        moments_per_load_condition = OrderedDict()
        member_number = None
        member_end = None
        group_id = None

        # loop over the 2 x 6 DOF rows
        for ii in range(12):
            line = fp.readline()
            columns = line.split()
            if ii == 0:
                # the member information is stored in the first row
                member_number = columns[0]
                member_end = columns[1]
                group_id = columns[2]
                col_offset = 3
            elif ii == 6:
                # starting a row 7 (ii==6) we are reading the second side
                col_offset = 1
            else:
                col_offset = 0

            if ii < 6:
                # only store the first side of the element
                load_condition = columns[col_offset]
                forces = [float(f) for f in columns[col_offset + 1:col_offset + 4]]
                moments = [float(m) for m in columns[col_offset + 4:]]
                forces_per_load_condition[load_condition] = Q_(np.array(forces), "kN")
                moments_per_load_condition[load_condition] = Q_(np.array(moments), "kN*m")

        # also read the last empty line
        fp.readline()

        # store all the information in theA namedtuple *SACS_ELEMENT* defined at the top of this
        # module
        element = SACS_ELEMENT(member_number=member_number,
                               member_end=member_end,
                               group_id=group_id,
                               forces=forces_per_load_condition,
                               moments=moments_per_load_condition)
        return element

    def report_element(self, member_number):
        """
        Make a report of the element *member_number*

        Parameters
        ----------
        member_number: str
            Name of the element to report.

        Notes
        -----
        * In case the element does not exist an message will be displayed via the logger to
          notify the user, but no error is raised
        """
        try:
            element = self.elements[member_number]
        except KeyError as err:
            self.logger.info("No such element: {}".format(member_number))
        else:

            self.logger.info(" ".join(["*" * 20, member_number, "*" * 20]))
            self.logger.info("{:20s} : {}".format("Member Number", element.member_number))
            self.logger.info("{:20s} : {}".format("Member End", element.member_end))
            self.logger.info("{:20s} : {}".format("Group ID", element.member_end))
            for load_condition, forces in element.forces.items():
                self.logger.info(
                    "{:20s} : {}".format("Forces for loading {}".format(load_condition), forces))
            for load_condition, moments in element.moments.items():
                self.logger.info(
                    "{:20s} : {}".format("Moment for loading {}".format(load_condition), moments))
            self.logger.info("\n")

    def make_report(self):
        """
        Make a report of the data
        """
        for member_number in self.elements.keys():
            self.report_element(member_number)


if __name__ == "__main__":
    # Force to run the doc test when the data_reader.py script is directly called as a main function
    import doctest

    doctest.testmod()
