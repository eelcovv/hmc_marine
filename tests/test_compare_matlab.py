#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from glob import glob

import numpy as np
import scipy.io as sio
from numpy.testing import (assert_almost_equal, assert_equal)

from hmc_marine.wave_spectra import (spectrum_jonswap, spreading_function, spreading_function2)
from hmc_utils.misc import create_logger

DATA_DIR = "data"
if not os.path.isdir(DATA_DIR):
    # correct path to data dir in case we are running from command line
    DATA_DIR = os.path.join("..", "data")


def test_matlab_jonswap():
    file_list = glob(os.path.join(DATA_DIR, "jonswap1d", "*.mat"))

    logger = create_logger(console_log_format_clean=True, console_log_level=logging.CRITICAL)
    logger.debug(file_list)

    for file_in in file_list:
        logger.info("reading file {}".format(file_in))

        mat_in = sio.loadmat(file_in)
        structure = mat_in["T"][0, 0]
        omega = structure["omega"].reshape(-1)
        spectrum_1d = structure["S"].reshape(-1)

        gamma = structure["gamma"].reshape(-2)
        Hs = structure["Hs"].reshape(-2)
        Tp = structure["Tp"].reshape(-2)

        logger.debug("omega: {}".format(omega.shape))
        logger.debug("spectrum: {}".format(spectrum_1d.shape))
        logger.debug("Hs: {}".format(Hs))
        logger.debug("Tp: {}".format(Tp))
        logger.debug("gamma: {}".format(gamma))

        spec_pyt = spectrum_jonswap(omega=omega, Hs=Hs, Tp=Tp, gamma=gamma, spectral_version="hmc")

        assert_almost_equal(spectrum_1d, spec_pyt, decimal=2)


def test_matlab_spreading_function():
    file_list = glob(os.path.join(DATA_DIR, "spreading", "*.mat"))
    logger = create_logger(console_log_format_clean=True, console_log_level=logging.INFO)

    logger.debug(file_list)
    fig = None
    line1, line2 = None, None
    n_spreading = None

    for file_in in file_list:
        logger.debug("reading file {}".format(file_in))

        mat_in = sio.loadmat(file_in)
        structure = mat_in["T"][0, 0]
        heading = structure["heading"].flatten()
        g_value = structure["G"].flatten(-1)

        spreading_matlab = structure["spreading"].flatten()[0]
        if n_spreading is None:
            n_spreading = 2 * spreading_matlab + 1  # check equation
            s_spreading = 2 * n_spreading + 1  # check equation

        theta_0_deg = structure["comingFrom"].flatten()[0]

        theta = np.deg2rad(heading)
        theta_0 = np.deg2rad(theta_0_deg)
        delta_theta = theta[1] - theta[0]

        # scale sum_g to theta domain as well
        sum_g = g_value[:-1].sum() * delta_theta
        g_value /= sum_g

        logger.debug("heading: {}".format(heading.shape))
        logger.debug("coming_from: {}".format(theta_0))

        d_pyt_n_1 = spreading_function(theta, theta0=theta_0, n_spreading_factor=n_spreading)
        d_pyt_s_1 = spreading_function(theta, theta0=theta_0, s_spreading_factor=s_spreading)
        d_pyt_n_2 = spreading_function2(theta, theta0=theta_0, n_spreading_factor=n_spreading)
        d_pyt_s_2 = spreading_function2(theta, theta0=theta_0, s_spreading_factor=s_spreading)

        # check if integral is one
        spread_sum = d_pyt_n_1[:-1].sum() * delta_theta
        assert_almost_equal([spread_sum], [1.0], decimal=4)

        # check if the same as matlab spreading
        assert_almost_equal(d_pyt_n_1, g_value, decimal=4)

        # the spreading function based on the s spreading factor should be identical to the 
        # one based on the n spreading factor as the s factor is converted to n internally
        assert_equal(d_pyt_n_1, d_pyt_s_1)

        # the two implementation are not identical, so set decimal to 1, for a rough check
        assert_almost_equal(d_pyt_n_1, d_pyt_n_2, decimal=1)

        # the version with the s parameter as input must be the same as the n parameter as input
        assert_equal(d_pyt_n_2, d_pyt_s_2)
