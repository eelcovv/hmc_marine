import os
import pickle

from hmc_utils.misc import compare_objects
from numpy.testing import (assert_almost_equal, assert_equal)

from hmc_marine.sequence_tool_utils import (SequenceToolSummary)

SEQUENCE_TOOL_DIR = "data/SequenceTool/Sanha_Deep"
SEQUENCE_TOOL_FILE = "pipelay_summary.seq_out"


def write_seq_data():
    out_dir = SEQUENCE_TOOL_DIR
    if not os.path.isdir(out_dir):
        out_dir = os.path.join("..", out_dir)

    file_name = os.path.join(out_dir, SEQUENCE_TOOL_FILE)
    seq = SequenceToolSummary(file_name)
    wow = seq.wow_time.per_month_in_days
    # dump the spectra only to the data file
    out_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(out_file, "wb") as out_stream:
        pickle.dump(seq, out_stream, pickle.HIGHEST_PROTOCOL)


def test_sequence_tool_read():
    out_dir = SEQUENCE_TOOL_DIR
    if not os.path.isdir(out_dir):
        out_dir = os.path.join("..", out_dir)
    file_name = os.path.join(out_dir, SEQUENCE_TOOL_FILE)
    seq = SequenceToolSummary(file_name)
    wow = seq.wow_time.per_month_in_days
    wow_exp = [82.8, 106.1, 122.2, 129.5, 120.7, 103.8, 83.1, 66.6, 58.2, 51.2, 50.5, 59.5]

    assert_almost_equal(wow, wow_exp)

    p90 = seq.cumulative_wow_probability.p90_per_month
    p90_exp = [158.9, 199.8, 199.5, 197.1, 180.9, 151.9, 127.9, 103.1, 79.8, 76.8, 122.9, 109.8]

    assert_almost_equal(p90, p90_exp)

    vessel_name = seq.VesselName
    assert_equal(vessel_name, "Aegir")

    in_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(in_file, "rb") as in_stream:
        seq_exp = pickle.load(in_stream)

    # check if the seq loaded and written to pickle file are the same
    try:
        compare_objects(seq, seq_exp)
    except AssertionError:
        compare_objects(os.path.join("..", seq), seq_exp)


def main():
    write_seq_data()


if __name__ == "__main__":
    # in case we run the test_mdf_parser as a script from the command line like
    # python.exe tests/test_mdf_parser
    # we call the main routine which will call the routine to create the pkl data from the header.
    # This pickle data is used later by the 'test_header' unit test in order to see if we read the
    # header correctly
    main()
