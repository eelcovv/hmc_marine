#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import pickle

import hmc_marine.data_readers as dr
from hmc_utils.misc import compare_objects
from hmc_utils.misc import (create_logger)

DATA_DIR = "data"

SACS_FILE_NAME = "AccelCoeffHEXTravBlockhigh75.txt"

logger = create_logger(console_log_level=logging.DEBUG)


def test_sacs_data_reader():
    file_name = os.path.join(DATA_DIR, SACS_FILE_NAME)
    if not os.path.exists(file_name):
        file_name = os.path.join("..", file_name)

    sacs_obj = dr.SACSForceMomentVs6DOFReader(file_name=file_name)

    out_file = os.path.splitext(file_name)[0] + ".pkl"
    logger.info("Reading from {}".format(out_file))

    with open(out_file, "rb") as in_stream:
        sacs_obj_exp = pickle.load(in_stream)

    # in order to be able to compare the object we have to simplify it a bit. Remove the logger
    # and remove the dimensions from the forces and moments for both objects. After that we
    # can use the compare_objects function to see if all the fields are the same
    for sacs_o in (sacs_obj, sacs_obj_exp):
        sacs_o.logger = None
        for name, element in sacs_o.elements.items():
            for load_name, forces in element.forces.items():
                # over write the force of this element and loading with an array without dimension
                sacs_o.elements[name].forces[load_name] = forces.magnitude
            for load_name, moments in element.moments.items():
                # over write the moment of this element and loading with an array without dimension
                sacs_o.elements[name].moments[load_name] = moments.magnitude

    # compare the objects
    try:
        compare_objects(sacs_obj, sacs_obj_exp)
    except AssertionError:
        sacs_obj.file_name = os.path.join("..", sacs_obj.file_name)
        sacs_obj.file_base = os.path.join("..", sacs_obj.file_base)
        compare_objects(sacs_obj, sacs_obj_exp)



def write_sacs_data():
    out_dir = DATA_DIR
    if not os.path.isdir(out_dir):
        out_dir = os.path.join("..", out_dir)

    file_name = os.path.join(out_dir, SACS_FILE_NAME)
    sacs_obj = dr.SACSForceMomentVs6DOFReader(file_name=file_name)
    sacs_obj.make_report()
    # dump the object
    out_file = os.path.splitext(file_name)[0] + ".pkl"
    logger.info("Writing to {}".format(out_file))
    sacs_obj.logger = None
    with open(out_file, "wb") as out_stream:
        pickle.dump(sacs_obj, out_stream, protocol=pickle.HIGHEST_PROTOCOL)


def main():
    write_sacs_data()


if __name__ == "__main__":
    # in case we run the test_mdf_parser as a script from the command line like
    # python.exe tests/test_mdf_parser
    # we call the main routine which will call the routine to create the pkl data from the header.
    # This pickle data is used later by the 'test_header' unit test in order to see if we read the
    # header correctly
    main()
