#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import pickle

import pandas as pd
from numpy import (array, linspace, rad2deg)
from numpy.testing import (assert_almost_equal, assert_equal)
from pandas.testing import (assert_frame_equal, assert_series_equal)

import hmc_marine.data_readers as dr
from hmc_utils.misc import (create_logger)

DATA_DIR = "data"

FUGRO_FILE_NAME = "Fugro_I0450_20170510_06.csv"
WRB_FILE_NAME = "example 2017}2017-05-10T00h01Z.spt"
SEASTATES_FILE_NAME = "murchison_seastates_20160807.xls"

logger = create_logger(console_log_level=logging.CRITICAL)


def test_lift_dyn_reader():
    file_name = os.path.join("data", "hermod_11_deep_POI1-acc-RAO.stf_plt")
    if not os.path.exists(file_name):
        file_name = os.path.join("..", file_name)

    lift = dr.LiftDynReader(file_name=file_name)

    # we should have 6 components
    assert_equal(len(lift.dof_components), 6)

    # the rao flag should be set to True
    assert_equal(lift.is_rao, True)

    # expected keys to import
    key_expected = list(
        [
            "RAO_Acceleration_incl_horz_gravity_component_AX-g*sin",
            "RAO_Acceleration_incl_horz_gravity_component_AY+g*sin",
            "RAO_Acceleration_incl_horz_gravity_component_AZ",
            "RAO_Acceleration_incl_horz_gravity_component_ARX",
            "RAO_Acceleration_incl_horz_gravity_component_ARY",
            "RAO_Acceleration_incl_horz_gravity_component_ARZ"
        ]
    )

    # test the key names
    for cnt, key in enumerate(lift.dof_names.keys()):
        assert_equal([key], [key_expected[cnt]])

    # test the first 10 values of the first RAO
    value_expected = array(
        [-0.00363122 - 1.00613724e-04j, -0.00362449 - 5.44434701e-04j,
         -0.00366878 - 1.04953159e-03j, -0.00367171 - 2.21459445e-03j,
         -0.00382295 - 3.73427160e-03j, -0.00396330 - 6.91262138e-03j,
         -0.00459263 - 1.23180153e-02j, -0.00695509 - 2.61024378e-02j,
         -0.04854975 - 0.104365910986j, -0.0176741481122 + 0.0724381457087j
         ]
    )

    assert_almost_equal(lift.dof_components[0].data[:10, 0], value_expected)


def write_fugro_spectrum():
    """
    Create a pickled dump of the fugro spectrum which we can use to test the data reader
    """
    # define the data location with respect to the location of the test file
    data_location = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", DATA_DIR))
    file_name = os.path.join(data_location, FUGRO_FILE_NAME)

    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"

    fugro = dr.WaveSpectralEnergy2DReader(file_name=file_name)

    data_frame = fugro.sea_states_df
    data_frame.to_msgpack(pd_out_file)

    # dump the spectra only to the data file
    out_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(out_file, "wb") as out_stream:
        for date_time, record in fugro.spectral_records.items():
            pickle.dump(record.psd_2d, out_stream, pickle.HIGHEST_PROTOCOL)


def test_fugro_reader():
    data_location = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", DATA_DIR))
    file_name = os.path.join(data_location, FUGRO_FILE_NAME)
    out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"
    fugro = dr.WaveSpectralEnergy2DReader(file_name=file_name)
    data_frame = fugro.sea_states_df

    data_frame_exp = pd.read_msgpack(out_file)

    assert_frame_equal(data_frame, data_frame_exp)

    # dump the spectra only to the data file
    pkl_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(pkl_file, "rb") as in_stream:
        for date_time, record in fugro.spectral_records.items():
            data = record.psd_2d
            data_exp = pickle.load(in_stream)
            assert_almost_equal(data, data_exp)


def write_wrb_spectrum():
    """
    Create a pickled dump of the fugro spectrum which we can use to test the data reader
    """
    # define the data location with respect to the location of the test file
    data_location = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", DATA_DIR))
    file_name = os.path.join(data_location, WRB_FILE_NAME)

    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"

    wrb = dr.WaveSpectralMomentsQ2DReader(file_names=[file_name])

    data_frame = wrb.sea_states_df
    data_frame.to_msgpack(pd_out_file)

    # dump the spectra only to the data file
    out_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(out_file, "wb") as out_stream:
        for date_time, record in wrb.spectral_records.items():
            pickle.dump(record.psd_2d, out_stream, pickle.HIGHEST_PROTOCOL)


def test_wrb_reader():
    data_location = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", DATA_DIR))
    file_name = os.path.join(data_location, WRB_FILE_NAME)
    out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"
    # read the wave rider buoy data
    wrb = dr.WaveSpectralMomentsQ2DReader(file_names=[file_name])
    data_frame = wrb.sea_states_df

    data_frame_exp = pd.read_msgpack(out_file)

    # check of the data frame with the sea states is the same as the stored data in the msg_pack
    assert_frame_equal(data_frame, data_frame_exp)

    # check if the spectral data in the records is the same as stored in the pickled data
    pkl_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(pkl_file, "rb") as in_stream:
        for date_time, record in wrb.spectral_records.items():
            data = record.psd_2d
            data_exp = pickle.load(in_stream)
            assert_almost_equal(data, data_exp)


def test_spectral_components_reader():
    file_name = os.path.join(DATA_DIR, SEASTATES_FILE_NAME)
    if not os.path.exists(file_name):
        file_name = os.path.join("..", file_name)
    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"

    spectrum = dr.WaveSpectralComponentsReader(file_name=file_name,
                                               wave_direction_relative_to_heading=False)

    data_frame = spectrum.sea_states_df

    data_frame_exp = pd.read_msgpack(pd_out_file)

    # for some reason the assert_frame_equal fails as the dictionary does not keep the same order of
    # columns. Just loop over the columns explicitly
    for col in data_frame.columns:
        assert_series_equal(data_frame[col], data_frame_exp[col])

    # check if the spectral data in the records is the same as stored in the pickled data
    pkl_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(pkl_file, "rb") as in_stream:
        for date_time, record in spectrum.spectral_records.items():
            data = record.psd_2d
            data_exp = pickle.load(in_stream)
            assert_almost_equal(data, data_exp)


def test_spectral_components_reader_2():
    # test the spectral components reader with an external data frame as input in stead of a
    # filename. Also an explicit array of frequencies and directions is passed
    file_name = os.path.join(DATA_DIR, SEASTATES_FILE_NAME)
    if not os.path.exists(file_name):
        file_name = os.path.join("..", file_name)
    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states2.msg_pack"

    frequencies = linspace(0.01, 2.5, endpoint=True, num=250)
    frequencies_hz = rad2deg(frequencies)
    directions_deg = linspace(0.0, 360, endpoint=False, num=24)

    sea_state_data = pd.read_excel(file_name, sheet_name="General", index_col=0)
    spectrum = dr.WaveSpectralComponentsReader(sea_state_data_base=sea_state_data,
                                               freq_in_hz=frequencies_hz,
                                               directions_deg=directions_deg,
                                               wave_direction_relative_to_heading=False)

    data_frame = spectrum.sea_states_df

    data_frame_exp = pd.read_msgpack(pd_out_file)

    # for some reason the assert_frame_equal fails as the dictionary does not keep the same order
    # of columns. Just loop over the columns explicitly
    for col in data_frame.columns:
        assert_series_equal(data_frame[col], data_frame_exp[col])

    # check if the spectral data in the records is the same as stored in the pickled data
    pkl_file = os.path.splitext(file_name)[0] + "_2.pkl"
    with open(pkl_file, "rb") as in_stream:
        for date_time, record in spectrum.spectral_records.items():
            sea_state_data = record.psd_2d
            data_exp = pickle.load(in_stream)
            assert_almost_equal(sea_state_data, data_exp)


def write_components_spectrum():
    """
    Create a pickled dump of the spectral components file
    """
    # define the data location with respect to the location of the test file
    data_location = DATA_DIR
    if not os.path.isdir(data_location):
        data_location = os.path.join("..", data_location)
    file_name = os.path.join(data_location, SEASTATES_FILE_NAME)

    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states.msg_pack"

    spectrum = dr.WaveSpectralComponentsReader(file_name=file_name, 
                                               wave_direction_relative_to_heading=False)

    data_frame = spectrum.sea_states_df
    data_frame.to_msgpack(pd_out_file)

    # dump the spectra only to the data file
    out_file = os.path.splitext(file_name)[0] + ".pkl"
    with open(out_file, "wb") as out_stream:
        for date_time, record in spectrum.spectral_records.items():
            pickle.dump(record.psd_2d, out_stream, pickle.HIGHEST_PROTOCOL)


def write_components_spectrum_2():
    """
    Create a pickled dump of the spectral components file
    """
    # define the data location with respect to the location of the test file
    data_location = DATA_DIR
    if not os.path.isdir(data_location):
        data_location = os.path.join("..", data_location)
    file_name = os.path.join(data_location, SEASTATES_FILE_NAME)
    pd_out_file = os.path.splitext(file_name)[0] + "_sea_states2.msg_pack"

    frequencies = linspace(0.01, 2.5, endpoint=True, num=250)
    frequencies_hz = rad2deg(frequencies)
    directions_deg = linspace(0.0, 360, endpoint=False, num=24)

    sea_state_data = pd.read_excel(file_name, sheet_name="General", index_col=0)
    spectrum = dr.WaveSpectralComponentsReader(sea_state_data_base=sea_state_data,
                                               freq_in_hz=frequencies_hz,
                                               directions_deg=directions_deg,
                                               wave_direction_relative_to_heading=False)

    data_frame = spectrum.sea_states_df
    data_frame.to_msgpack(pd_out_file)

    # dump the spectra only to the data file
    out_file = os.path.splitext(file_name)[0] + "_2.pkl"
    with open(out_file, "wb") as out_stream:
        for date_time, record in spectrum.spectral_records.items():
            pickle.dump(record.psd_2d, out_stream, pickle.HIGHEST_PROTOCOL)


def main():
    write_fugro_spectrum()
    write_wrb_spectrum()
    write_components_spectrum()
    write_components_spectrum_2()


if __name__ == "__main__":
    # in case we run the test_mdf_parser as a script from the command line like
    # python.exe tests/test_mdf_parser
    # we call the main routine which will call the routine to create the pkl data from the header.
    # This pickle data is used later by the 'test_header' unit test in order to see if we read the header correctly
    main()
