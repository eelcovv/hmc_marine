=========
Changelog
=========

Version 0.1.5
===========

- Update to Python 3.6
- Update to pyscaffold 3.0

Version 0.1
===========

- Initialised the reposistory
