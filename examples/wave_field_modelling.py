"""
A script to make a 2D power spectrum of a Johnswap wave field and show the traveling waves

This script is based on the spectral functions as can be found in the hmc_marine.wave_spectra.
In the hmc_marine.wave_field some higher level Wave1D and Wave2D classes are defined in order to simplify transient
wave modeling. See the example_wave_fields.py for that

to make the movies, two methods are used

First: the Animator of matplot lib. Works ok but is slow and not very nice for real 3D surfaces

Second: mayavi. Install mayavi and also moviepy
The latter is used to immediately write movies to file

"""

import platform

import matplotlib.animation as animation
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np  # all numpy stuff
from hmc_utils.misc import Timer
from matplotlib import rcParams  # some matplotlib modules for making nice contour plots
from matplotlib.colors import LightSource
from numpy.fft import fftfreq, fftshift  # two functions to manipulate frequencies
from scipy.constants.constants import g as g0  # the gravity constant

# this module is required to calculate the Johnswap spectrum and should be located in
import hmc_marine.wave_spectra as ms

if platform.system() == "Linux":
    print("Switching off blitting for linux")
    # use(u'agg')
    blitting = False
else:
    blitting = True

matplotlib_animation = True  # create animation with matplot lib
mayavi_animation = True  # create animation with mayavi

animate_the_scene = False  # if this is false, only the first mayavi frame is create
movie_name_base = "wave_surface"

# set the global settings of matplotlib
rcParams['xtick.direction'] = 'out'
rcParams['ytick.direction'] = 'out'

# some plot settings for the lighting
ls = LightSource(azdeg=315, altdeg=30)
vert_eg = 1
add_contour_lines = True
cmapsea = cm.ocean

# this prevent that the image block the script after popping up
plt.ion()

# Wave properties
Hs = 4  # [m] significant wave height
Tp = 10  # [s] Peak period
gamma = 3.3  # broadness of the peak around Tp
Theta_marine = 40  # direction of the wave field (in marine definition: 0 is positive y-axis, 90 positive x)
Theta_nspreadkx = 5.0  # The amount of spreading (1 is a lot of spreading, large values less spreading)
n_kp = 5  # for plotting purpose, limit the range of the k wave domain

# size of domain in x and y direction
Lx = 1000
Ly = 1000

# number of points in x and y direction
Nx = 256
Ny = 256

# create a wave number mesh based on the space settings above
dx = Lx / Nx
dy = Ly / Ny

# time settings
repeat_movie = False  # repeat the movie cycle
time_start = 0
delta_t = 0.5  # [s] the time step between to movie frames
fps = 10  # number of frames per second in the movie
duration = 20  # the real play time of the movie
speedup = delta_t * fps  # the speedup of the movie compared to the real time
time_end = duration * speedup  # the end time after the movie is done playing
n_frames = int((time_end - time_start) / delta_t)

# show some information
fmstr = "{:20s} : {}"
print(fmstr.format("Time Start", time_start))
print(fmstr.format("Time End", time_end))
print(fmstr.format("Delta T", delta_t))
print(fmstr.format("fps", fps))
print(fmstr.format("Duration", duration))
print(fmstr.format("Speedup", speedup))
print(fmstr.format("Number of Frames", n_frames))

# calcute the wave vectors given de number of point and sample spacing in X domain
kx_nodes = 2 * np.pi * fftfreq(Nx, dx)
ky_nodes = 2 * np.pi * fftfreq(Ny, dy)

# create the 2D mesh field
k_mesh = np.meshgrid(kx_nodes, ky_nodes)

# boundaries of the k_mesh, used for the contour plot later
k_boundaries = (k_mesh[0].min(), k_mesh[0].max(), k_mesh[1].min(), k_mesh[1].max())
i_boundaries = (0, Nx - 1, 0, Ny - 1)

# get the k-vector magnitude over the mesh
kk = np.sqrt(k_mesh[0] ** 2 + k_mesh[1] ** 2)

# create some plots of the k-domain
# create plot of kx_nodes
fig = plt.figure(figsize=(6, 3))
title = "k_x  nodes vs index"
plt.title(title)
fig.canvas.set_window_title(title)
plt.xlabel("index")
plt.ylabel("$k_x$ [rad/s]")
plt.plot(range(kx_nodes.shape[0]), kx_nodes, linewidth=2)

# wave vector contour plot
title = "Wave vector magnitude contour plot"
fig, axis = plt.subplots(1, 2)
titles = ["kk", "fftshift(kk)"]
kk_data = [kk, fftshift(kk)]
fig.canvas.set_window_title(title)
fig.subplots_adjust(hspace=1)
fig.set_size_inches(10, 5, forward=True)

# loop over the axis and add the axis labels and the plot
for i, ax in enumerate(axis):
    ax.set_xlabel("index $i$ [-]")
    ax.set_ylabel("index $j$ [-]")
    ax.set_title(titles[i])
    if i == 1:
        # remove the y labels on the second graph to make it clearer
        ax.set_ylabel("")
        plt.setp(ax.get_yticklabels(), visible=False)
    ax.imshow(kk_data[i], origin='lower', extent=i_boundaries, interpolation=None)

# convert the marine angle into a normal mathematical definition
Theta_0 = np.deg2rad(90 - Theta_marine)

# calculate the complex amplitudes belonging to this wave field
S_tilde, Osign = ms.spectrum2d_complex_amplitudes(kx_nodes, ky_nodes, Hs=Hs, Tp=Tp, gamma=gamma,
                                                  Theta_0=Theta_0,
                                                  Theta_s_spread_kx=Theta_nspreadkx)

# calculate the dispersion wave frequency belonging to the k-wave numbers. The Osign is return from
# the jonswap routine and is required to take into account the direction of the traveling wave
omega_dispersion = np.sqrt(g0 * abs(kk)) * Osign

# get the magnitude and phase from the complex spectrum
S_magnitude = np.abs(S_tilde)
S_phase = np.rad2deg(np.angle(S_tilde))

# for plotting purpose the k=0 point is shifted to the centre of the domain make sure the the k
# vectors and S have there center in the middle (not the case for normal fftfreq output)
kx_shift = fftshift(k_mesh[0])
ky_shift = fftshift(k_mesh[1])
Sm_shift = fftshift(S_magnitude)
Sp_shift = fftshift(S_phase)
S_data = [Sm_shift, Sp_shift]

##########################
# First plot: a contour plot of the spectrum magnitude
##########################

# set up contour levels for the spectrum plot
Smin = 0
Smax = S_magnitude.max()
n_levels = 7
delta_S = (Smax - Smin) / n_levels
levels = np.arange(Smin, Smax, delta_S)
norm = cm.colors.Normalize(vmax=Smax, vmin=Smin)
cmap = cm.PRGn

# create the first plot of the Johnswap spectum
fig1 = plt.figure()
title = "Power spectral density of Johnswap Wave field"
plt.title(title)
fig1.canvas.set_window_title(title)
plt.xlabel("$k_x$ [rad/m]")
plt.ylabel("$k_y$ [rad/m]")

# calculate the wave number  at the peak and multiply with n_kp to get the range of the kwave in the first plot
k_peak = (2 * np.pi / Tp) ** 2 / g0
plt.xlim(-n_kp * k_peak, n_kp * k_peak)
plt.ylim(-n_kp * k_peak, n_kp * k_peak)

# create a contour plot with filled levels
CS1 = plt.contourf(kx_shift, ky_shift, Sm_shift, n_levels,
                   cmap=cm.get_cmap(cmap, len(levels) - 1),
                   norm=norm)
# set the color bar for the legend
cbar = plt.colorbar(CS1)
cbar.ax.set_ylabel("$E_{JS}(k_x,k_y)$")

if add_contour_lines:
    # add black lines around the contour shanges
    CS2 = plt.contour(kx_shift, ky_shift, Sm_shift, CS1.levels,
                      colors='k', hold='on')

plt.show()

##########################
# Second plot: the complex amplitude : Modules (left), phas (right)
##########################

# plot the contours using imshow. Is faster than with contourf

# the extend tuple contains the min/max of x and y component of the wave vector k

# create two subplot next to each other and fine tune it
fig2, axis = plt.subplots(1, 2)
title = "Magnitude and phase of complex amplitudes over k-space"
fig2.canvas.set_window_title(title)
titles = ["Amplitude", "Phase"]
fig2.subplots_adjust(hspace=1)
fig2.set_size_inches(10, 5, forward=True)

# loop over the axis and add the axis labels and the plot
for i, ax in enumerate(axis):
    ax.set_xlabel("$k_x$ [rad/m]")
    ax.set_ylabel("$k_y$ [rad/m]")
    if i == 1:
        # remove the y labels on the second graph to make it clearer
        ax.set_ylabel("")
        plt.setp(ax.get_yticklabels(), visible=False)
    ax.set_title(titles[i])
    ax.imshow(S_data[i], origin='lower', extent=k_boundaries, interpolation=None)

# the pause is required to be able to draw the plot without blocking
plt.show()
plt.pause(0.001)


##########################
# Third plot/animation of the wave field using matplotlib
##########################

def calculate_wave_surface(Ac, Od, time):
    """
    Calculate the wave field surface for a given time based on the complex Fourier components

    :param Ac:  complex amplitudes
    :param Od:  sign to distinguish travel direction of the wave
    :param time: current time
    :return:  the wave field in a numpy array
    """
    N = Ac.size / 2
    return np.real(N * np.fft.ifft2(Ac * np.exp(1j * (-time * Od))))


if matplotlib_animation:

    # create a new plot to hold the wave field
    fig3 = plt.figure()
    title = "Wave field animation"
    fig3.canvas.set_window_title(title)

    # initialise the axis of the plot
    dL = 0
    ax = fig3.add_subplot(111, aspect='equal', autoscale_on=False,
                          xlim=(-dL, Lx + dL), ylim=(-dL, Ly + dL))
    ax.set_xlabel("X [m]")
    ax.set_ylabel("Y [m]")

    # set the time label in the top left corner
    time_text = ax.text(0.02, 0.95, "", transform=ax.transAxes, color="yellow")

    # create the first plot of the sea surface.
    # use the LightSource shade to make a nicer contour with shadings
    L_boundaries = (0, Lx, 0, Ly)
    wave_surface = calculate_wave_surface(S_tilde, omega_dispersion, 0)
    rgb = ls.shade(wave_surface, cmap=cmapsea, vert_exag=vert_eg, blend_mode='hsv')
    im = plt.imshow(rgb, origin='lower', extent=L_boundaries, animated=True, vmin=-Hs, vmax=Hs)


    # for making a movie, a init function to initialise the first frame and an update function for updating
    # the next frames need to be defined.
    def init():
        time_text.set_text("")
        wave_surface = calculate_wave_surface(S_tilde, omega_dispersion, 0)
        rgb = ls.shade(wave_surface, cmap=cmapsea, vert_exag=vert_eg, blend_mode='hsv', vmin=-Hs,
                       vmax=Hs)

        # in stead of calling imshow again, use the set_array funtion to replace the old data. This is about
        # 10 x faster
        im.set_array(rgb)

        return im, time_text


    def update_contour_plot(i):
        # update the current plot

        # calculate the time and set the time label
        time = delta_t * i
        time_label = "Time : {:10.1f} s".format(time)
        print(time_label)
        time_text.set_text(time_label)

        # update the wave surface for the current ime
        wave_surface = calculate_wave_surface(S_tilde, omega_dispersion, time)

        # create the contour surface
        rgb = ls.shade(wave_surface, cmap=cmapsea, vert_exag=vert_eg, blend_mode='hsv', vmin=-Hs,
                       vmax=Hs)
        im.set_array(rgb)
        return im, time_text


    # measure the time required to update the plot for the first frame
    with Timer(name="update_contour_plot matplotlib") as timer:
        update_contour_plot(0)

    # based on the target dt (needed to set a given fps), calculate the interval
    dt_msec_target = 1000.0 / fps
    interval = dt_msec_target - timer.secs * 1000
    print("interval:  {}".format(interval))
    if interval < 0:
        interval = 1

    # start the animation. Make sure blit is true as this causes that only things that has changed are updated
    ani = animation.FuncAnimation(fig3, update_contour_plot, init_func=init, interval=interval,
                                  frames=n_frames, blit=blitting, repeat=repeat_movie)

    plt.show()

##########################
# Fourth plot/animation
##########################

if mayavi_animation:
    # create a mayavi animation
    # create the movie with moviepy. See
    # http://zulko.github.io/blog/2014/11/29/data-animations-with-python-and-moviepy/
    try:
        from mayavi import mlab
    except ImportError:
        print("can not import mlab. Skipping this example")
    else:

        # initialise the figure
        mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(800, 600))
        mlab.clf()

        # create the mesh for the x/y points
        Lx_nodes = np.linspace(0, Lx, Nx)
        Ly_nodes = np.linspace(0, Ly, Ny)

        # initialise the image for the first image
        wave_surface = calculate_wave_surface(S_tilde, omega_dispersion, 0)
        s = mlab.surf(Lx_nodes, Ly_nodes, wave_surface,
                      vmin=-Hs, vmax=Hs,
                      reset_zoom=False,
                      warp_scale=4,
                      colormap='GnBu')

        # add some orientation axis, set the view and add a label
        mlab.orientation_axes(s)
        mlab.view(-90, 65, 1.3 * Lx, (Lx / 2, Ly / 4, 0))
        text = mlab.text(0.05, 0.95, "Wave field at t={:6.1f} s".format(0))


        def make_frame(t):
            # this routine is used by moviepy to update the frames

            # set the current time
            time = t * speedup

            # calculate the wave surface at time t
            wave_surface = calculate_wave_surface(S_tilde, omega_dispersion, time)

            # update the wave data
            s.mlab_source.scalars = wave_surface
            # update the text label
            text.text = "Wave field at t={:6.1f} s".format(time)
            # return the screen shot to the animator
            return mlab.screenshot(antialiased=True)


        # call the animator. To update the image, use the make_frame function above
        if animate_the_scene:
            try:
                import moviepy.editor as mpy
            except ImportError:
                print("can not import moviepy. Skipping this example")
            else:
                animation = mpy.VideoClip(make_frame, duration=duration)

# this final call will make sure that the last show is blocking
plt.ioff()
plt.show()
