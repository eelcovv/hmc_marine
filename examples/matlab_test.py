import logging
from glob import glob

import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import seaborn as sns
from hmc_utils.misc import (create_logger)
from numpy.testing import assert_almost_equal

from hmc_marine.wave_spectra import (spectrum_jonswap, spreading_function, spreading_function2)

sns.set(context="notebook")


def test_matlab_jonswap_1d():
    directory_in = "../data/jonswap1d"
    file_list = glob(directory_in + "/*.mat")

    logger = create_logger(console_log_format_clean=True)

    logger.debug(file_list)
    fig = None
    line1, line2 = None, None

    for file_in in file_list:
        logger.debug("reading file {}".format(file_in))

        mat_in = sio.loadmat(file_in)
        structure = mat_in["T"][0, 0]
        omega = structure["omega"].reshape(-1)
        spectrum_1d = structure["S"].reshape(-1)

        gamma = structure["gamma"].reshape(-2)
        Hs = structure["Hs"].reshape(-2)
        Tp = structure["Tp"].reshape(-2)

        logger.debug("omega: {}".format(omega.shape))
        logger.debug("spectrum: {}".format(spectrum_1d.shape))
        logger.debug("Hs: {}".format(Hs))
        logger.debug("Tp: {}".format(Tp))
        logger.debug("gamma: {}".format(gamma))

        for spectrum_type in ("hmc", "dnv"):
            spec_pyt = spectrum_jonswap(omega=omega, Hs=Hs, Tp=Tp, gamma=gamma,
                                        spectral_version=spectrum_type)

            try:
                assert_almost_equal(spectrum_1d, spec_pyt, decimal=1)
            except AssertionError:
                logger.warning("Failed check for file {} with gamma {}".format(file_in, gamma))
                # plt.figure("jon {}".format(file_in))
                # line1, = plt.plot(omega, spectrum_1d, label="matlab")
                # line2, = plt.plot(omega, spec_pyt, label="python")
                # plt.legend()
                # plt.show()
            else:
                logger.info("Succeeded test for file {}".format(file_in))


def test_matlab_spreading_function():
    directory_in = "../data/spreading"
    file_list = glob(directory_in + "/*.mat")
    logger = create_logger(console_log_format_clean=True, console_log_level=logging.INFO)

    logger.debug(file_list)
    fig = None
    line1, line2 = None, None
    n_spreading = None

    for file_in in file_list:
        logger.debug("reading file {}".format(file_in))

        mat_in = sio.loadmat(file_in)
        structure = mat_in["T"][0, 0]
        heading = structure["heading"].reshape(-1)
        g_value = structure["G"].reshape(-1)

        spreading_matlab = structure["spreading"].flatten()[0]
        if n_spreading is None:
            n_spreading = 2 * spreading_matlab + 1  # check equation
        theta_0_deg = structure["comingFrom"].flatten()[0]

        s_spreading = 2 * n_spreading + 1

        theta = np.deg2rad(heading)
        theta_0 = np.deg2rad(theta_0_deg)
        delta_theta = theta[1] - theta[0]

        # scale sum_g to theta domain as well
        sum_g = g_value[:-1].sum() * delta_theta
        g_value /= sum_g

        logger.debug("heading: {}".format(heading.shape))
        logger.debug("coming_from: {}".format(theta_0))

        d_spreading_n = spreading_function(theta, theta0=theta_0, n_spreading_factor=n_spreading)
        d_spreading_s = spreading_function(theta, theta0=theta_0, s_spreading_factor=s_spreading)

        d_spreading_n2 = spreading_function2(theta, theta0=theta_0, n_spreading_factor=n_spreading)
        d_spreading_s2 = spreading_function2(theta, theta0=theta_0, s_spreading_factor=s_spreading)

        sum = d_spreading_n[:-1].sum() * delta_theta

        try:
            assert_almost_equal([sum], [1.0], decimal=2)
        except AssertionError:
            logger.warning("File {} not unity but {}".format(file_in, sum))

        try:
            assert_almost_equal(d_spreading_n, g_value, decimal=2)
        except AssertionError:
            logger.warning("File {} not equal with ml".format(file_in))

        fig = plt.figure()
        plt.plot(theta, g_value, '-o', label="matlab")
        plt.plot(theta, d_spreading_n, '-<', label="python n")
        plt.plot(theta, d_spreading_s, '->', label="python s")
        plt.plot(theta, d_spreading_n2, '-+', label="python n2")
        plt.plot(theta, d_spreading_s2, '-x', label="python s2")
        plt.legend()
        plt.show()


def main():
    # test_matlab_jonswap_1d()
    test_matlab_spreading_function()


if __name__ == "__main__":
    main()
