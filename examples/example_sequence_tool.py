import argparse
import logging
import os

import matplotlib.pyplot as plt
import seaborn as sns
from hmc_utils.misc import (create_logger, read_settings_file, read_value_from_dict_if_valid)

from hmc_marine.sequence_tool_utils import (SequenceToolSummary)

sns.set(context="notebook")


def make_sequence_tool_plots(settings_file="example_sequence_tool.yml", log_level=logging.INFO):
    """Create plot for the sequence tool

    Parameters
    ----------
    settings_file: configuration file name
    log_level: log level
    """

    logger = create_logger(console_log_format_clean=True, console_log_level=log_level)

    logger.info("Reading settings file {}".format(settings_file))
    settings = read_settings_file(settings_file)

    data_location = read_value_from_dict_if_valid(dictionary=settings["general"],
                                                  key="data_location",
                                                  default_value=".")

    for plot_name, plot_settings in settings["plots"].items():
        if not plot_settings["active"]:
            logger.info("Plot {} not active. Skipping".format(plot_name))
            continue

        logger.info("Creating plot {}".format(plot_name))

        # set the path and create the sequence tool object
        sub_path = read_value_from_dict_if_valid(dictionary=plot_settings, key="sub_path",
                                                 default_value=".")

        plot_title = read_value_from_dict_if_valid(plot_settings, key="plot_title")
        sub_title = read_value_from_dict_if_valid(plot_settings, key="plot_sub_title")
        legend_title = read_value_from_dict_if_valid(plot_settings, key="legend_title")

        fig = None
        axis = None

        # definition of the plot with varying crane and fixed viscosity
        for line_name, line_settings in plot_settings["lines"].items():
            file_base = line_settings["file_base"]
            file_name = os.path.join(data_location, sub_path, file_base) + ".seq_out"
            logger.info("adding line {}".format(line_name))
            logger.info("Reading sequence tool data from {}".format(file_name))

            line_style = line_settings["line_type"]
            line_title = line_settings["line_title"]

            # this is the line where the sequence tool data is actually loaded and stored in a
            # object 'seq' which we use to store all the data
            seq = SequenceToolSummary(file_name)
            seq.report()

            print("workability")
            print(seq.workability)

            fig, axis = seq.plot_wow(fig=fig,
                                     axis=axis,
                                     plot_title=plot_title,
                                     plot_sub_title=sub_title,
                                     legend_title=legend_title,
                                     line_style=line_style,
                                     line_title=line_title,
                                     wow_max=120, p90_max=200)


def parser_the_command_line_arguments():
    """
    Command line parser set up

    Returns
    -------
    tuple(args, parser)

    """
    parser = argparse.ArgumentParser(description='Sequence tool for waiting on weather analyzes',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--configuration_file', default="example_sequence_tool.yml",
                        help="Settings yaml file")
    # set the verbosity level command line arguments
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    # parse the command line
    args = parser.parse_args()

    return args, parser


if __name__ == "__main__":
    # this section is only parsed if the script is called from the command line. The parser can
    # be used to get the command line argument. These are passed to the main function.
    # Alternatively you can load the 'make_sequence_tool_plots' as a function and add the arguments
    # youself
    args, parser = parser_the_command_line_arguments()
    make_sequence_tool_plots(settings_file=args.configuration_file, log_level=args.log_level)

    plt.show()
