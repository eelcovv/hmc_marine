"""
Example script to demonstrate the usage of the Wave rider buoy quasi-2D spectral forecast data with
the class WaveSpectralMomentsQ2DReader

One spectrum is stored in one file, so you have to loop over multiple files. A spectrum is defined
as a series of four moments (mean, width, skewness, flatness) to describe the spectral energy
distribution per frequency bin. The WaveSpectraolMomentsQ2DReader converts this quasi-2D spectral
distribution to a normal PSD as stored in the WaveSpectralDensity2DReader class.

The conversion of the WRB data is quite time consuming. In order to save time the next time you run
the script, the data can be saved to a netcdf file. In this script it is demonstrated how to do that
"""
import logging
import os

import matplotlib.cm as cm
import matplotlib.pyplot as plt
from hmc_utils.file_and_directory import (scan_base_directory)
from hmc_utils.misc import (create_logger, Timer)

import hmc_marine.data_readers as dr

reset_netcdf = True

plt.ioff()
logger = create_logger(console_log_level=logging.INFO)
logger.debug("module loaded : {}".format(dr.__file__))

# the file name
data_location = os.path.join("data")
fugro_file_name = os.path.join(data_location, "Fugro_I0450_20170510_06.csv")
if not os.path.exists(fugro_file_name):
    data_location = os.path.join("../data")
    fugro_file_name = os.path.join(data_location, "Fugro_I0450_20170510_06.csv")

netcdf_example = dr.NetCDFInfo(file_location=data_location, reset_netcdf=reset_netcdf)
wrb_files_example = scan_base_directory(data_location, file_has_string_pattern="example.*",
                                        extension=".spt")[:1]
logger.info("Reading files {}".format(wrb_files_example))
with Timer(message="Reading with reset={}".format(reset_netcdf)) as timer:
    wrb_example = dr.WaveSpectralMomentsQ2DReader(file_names=wrb_files_example, n_frequencies=60,
                                                  n_directions=360, data_base_name="example")

# create a plot  to mimic the plot given by Datawell
# https://gist.github.com/seumasmorrison/5935386
wrb_example.plot_frame(frame_index=0, plot_title="Spectrum {}".format(wrb_files_example[0]),
                       number_of_contour_levels=10, color_map=cm.Reds, r_axis_type="frequency",
                       min_data_value=0.0, max_data_value=0.03
                       )

for cnt, (date_time, spectral_data) in enumerate(wrb_example.spectral_records.items()):
    spectral_data.report()

# now plot the murchison data
data_location = os.path.join(data_location, "WRB")
netcdf = dr.NetCDFInfo(file_location=data_location, reset_netcdf=False)
wrb_files = scan_base_directory(data_location, file_has_string_pattern="Mur.*", extension=".spt")
logger.info("Reading files {}".format(wrb_files))
with Timer(message="Reading with reset={}".format(reset_netcdf)) as timer:
    wrb = dr.WaveSpectralMomentsQ2DReader(file_names=wrb_files, n_frequencies=60, n_directions=360,
                                          data_base_name="example", netcdf=netcdf
                                          )

wrb.plot_frame(frame_index=0, plot_title="Spectrum {}".format(wrb_files[0]),
               r_axis_lim=(0, 0.25),
               r_axis_type="period",
               number_of_contour_levels=10,
               max_data_value=0.03,
               min_data_value=0.0,
               )

movie = wrb.animate_frames(plot_title="WRB Spectrum Animation",
                           r_axis_lim=(0, 0.25),
                           r_axis_type="period",
                           min_data_value=0,
                           max_data_value=0.05
                           )

for cnt, (date_time, spectral_data) in enumerate(wrb.spectral_records.items()):
    spectral_data.report()

plt.show()
