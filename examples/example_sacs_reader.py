import logging
import os

from hmc_utils.misc import (create_logger)

import hmc_marine.data_readers as dr

DATA_DIR = "data"
SACS_FILE_NAME = "AccelCoeffHEXTravBlockhigh75.txt"

logger = create_logger(console_log_level=logging.DEBUG)

file_name = os.path.join(DATA_DIR, SACS_FILE_NAME)
if not os.path.exists(file_name):
    file_name = os.path.join("..", file_name)

# create an object which hold all the sacs element stored in the sacs data file
sacs_obj = dr.SACSForceMomentVs6DOFReader(file_name=file_name)

# this will dump all the elements
sacs_obj.make_report()

# the elements are stored in the elements dictionary
elements = sacs_obj.elements
logger.info("keys: {}".format(elements.keys()))

# One element is a named tuple
element_rx4 = elements["SRX4_3975"]

# the forces per load condition are stored
logger.info(element_rx4)

# the forces of the AX loading are. Similar the forces of the AY, AZ, RX, RY, and RZ loading are
# store
forces_ax = element_rx4.forces["AX"]
logger.info(forces_ax)
forces_rx = element_rx4.forces["RX"]
logger.info(forces_rx)

# the moments of the AX loading are
moments_ax = element_rx4.moments["AX"]
logger.info(moments_ax)

# to report all info of on element to
sacs_obj.report_element("SRX4_3975")
