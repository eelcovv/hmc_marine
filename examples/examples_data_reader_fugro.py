"""
Example script to demonstrate the usage of the Fugro 2D spectral forecast data with the class WaveSpectralEnergy2DReader
"""

import logging
import os

import dateutil.parser as dparser
import matplotlib.pyplot as plt
from hmc_utils.misc import (create_logger, Timer)

# make sure that the local version of the hmc_marine modules are found first
import hmc_marine.data_readers as dr

plt.ioff()
logger = create_logger(console_log_level=logging.INFO)
logger.debug("module loaded : {}".format(dr.__file__))

# the file name
data_location = os.path.join("..", "data")
file_name = os.path.join(data_location, "Fugro_I0450_20170510_06.csv")
start_date_time = dparser.parse(os.path.splitext(file_name)[0], fuzzy=True)
logger.info("Reading Fugro file {} with start date/time {}".format(file_name, start_date_time))

# read the spectral data
fugro = dr.WaveSpectralEnergy2DReader(file_name=file_name)

# the sea state data (scalars with Hs, Tp, Theta, etc) vs date/time is stored  in the sea_states_df
# DataFrame
sea_states = fugro.sea_states_df
sea_states.info()

n_rows = 4
n_cols = 1
fig, axis = plt.subplots(ncols=n_cols, nrows=n_rows, sharex=True, figsize=(10, 10))
plt.subplots_adjust(wspace=0.4)

# top plot
sea_states.plot(y=["Hs", "HSig"], ax=axis[0])
axis[0].set_ylabel("Wave Height [m]")

# second plot
sea_states.plot(y=["HsTp2", "HsTp2true"], ax=axis[1])
axis[1].set_ylabel("HsTp2 [ms2]")

# third plot
sea_states.plot(y=["Tp_spec", "Tp"], ax=axis[2])
axis[2].set_ylabel("Tp [s]")

# bottom plot
sea_states.plot(y=["WS"], ax=axis[3])
axis[3].set_ylabel("Wind Speed [m/s]")

# the spectral data is stored in a dictionary called spectral_records, where the key is the
# date/time and the value is an Object of the type WaveSpectral Energy.
for cnt, (date_time, spectral_data) in enumerate(fugro.spectral_records.items()):
    spectral_data.report()

# fugro.plot_frame(frame_datetime="20170516T060000", r_axis_lim=(0, 0.25))
fugro.plot_frame(frame_index=0, r_axis_lim=(0, 0.25), plot_title="Spectral Density")

# finally, animate as well
movie = fugro.animate_frames(r_axis_lim=(0, 0.25), min_data_value=0, max_data_value=0.05,
                             plot_title="PSD animation")

plt.show()

# example of NetCDF usage
with Timer(message="Without netcdf") as _:
    fugro = dr.WaveSpectralEnergy2DReader(file_name=file_name, n_directions=360, n_frequencies=200)

fugro.plot_frame(frame_index=0, r_axis_lim=(0, 0.25), plot_title="From CSV")

# create netcdf file in the local directory and read the data again, this time with writing the data
# to netcdf
netcdf = dr.NetCDFInfo(file_location=data_location, reset_netcdf=True)
with Timer(message="Initializing netcdf") as _:
    fugro_tmp = dr.WaveSpectralEnergy2DReader(file_name=file_name, n_directions=360,
                                              n_frequencies=200, netcdf=netcdf,
                                              data_base_name="fugro")

netcdf = dr.NetCDFInfo(file_location=data_location)

with Timer(message="From netcdf") as _:
    fugro_new = dr.WaveSpectralEnergy2DReader(file_name=file_name, n_directions=360,
                                              n_frequencies=200, netcdf=netcdf,
                                              data_base_name="fugro")
fugro_new.plot_frame(frame_index=0, r_axis_lim=(0, 0.25), plot_title="From NETCDF")

plt.show()
