"""
Script to demonstrate to use the WaveSpectralComponents2D Reader class.

A excel file with the sea states (Hs, Tp, Theta) for all component (wind, swell, swell2) vs date/time is read
The WaveSpectralComponent2D reader is used to construct 2D wave spectra out of these using prescribed distribution
functions

Note that the default setup for the sea state components is used as specified in the SPECTRAL_COMPONENTS
dictionary in the data_reader modules. This choise can be overrulled by passing a personal  spectral_components
dictionary via the constructor argument list via the spectral_component argument
"""
import logging
import os
import sys

import dateutil.parser as dparser
import matplotlib.pyplot as plt
import numpy as np
from hmc_utils.misc import (create_logger, print_banner)

# make sure that the local version of the hmc_marine modules are found first
import hmc_marine.data_readers as dr

reset_netcdf = True

plt.ioff()

logger = create_logger(console_log_level=logging.INFO)
logger.debug("module loaded : {}".format(dr.__file__))

# the file name
data_location = os.path.join("..", "data")
file_name = os.path.join(data_location, "murchison_seastates_20160807.xls")
start_date_time = dparser.parse(os.path.splitext(file_name)[0], fuzzy=True)
logger.info("Reading Spectral components file {} with start date/time {}".format(file_name,
                                                                                 start_date_time))

# read the spectral data. The wave spectra are constructed based on the spectral component values Hs, Tp, Theta0 as
# found in the excel file and the specification in SPECTRAL_COMPONENTS. This can be overrulled by defining you own
# dictionary and passing it to the constructor as
# spectrum = dr.WaveSpectralComponentsReader(file_name=file_name, spectral_components=my_person_dict_with_compnents)

spectrum = dr.WaveSpectralComponentsReader(file_name=file_name)

for cnt, (date_time, spectral_data) in enumerate(spectrum.spectral_records.items()):
    spectral_data.report()

first_datetime = list(spectrum.spectral_records.keys())[0]
first_record = spectrum.spectral_records[first_datetime]

wind_component = spectrum.report_all_components(record_index=0, component_to_return="wind_waves",
                                                quiet=True)
swell1_component = spectrum.report_all_components(record_index=0, component_to_return="swell1",
                                                  quiet=True)
swell2_component = spectrum.report_all_components(record_index=0, component_to_return="swell2",
                                                  quiet=True)

print_banner("Sea states components")
logger.info("wind  :\n{}".format(wind_component))
logger.info("swell1:\n{}".format(swell1_component))
logger.info("swell2:\n{}".format(swell2_component))

# convert the Theta in radians and calculate frequency from Tp for all components
for df_comp in (wind_component, swell1_component, swell2_component):
    df_comp["Theta0"] = np.deg2rad(df_comp["Theta0"])
    df_comp["fp"] = 1.0 / df_comp["Tp"]

# create a plot of the first date/time
fig, axis = spectrum.plot_frame(frame_datetime=first_datetime, r_axis_lim=(0.01, 0.5))

# add scatter points at each of the component peaks
axis.scatter(wind_component["Theta0"], wind_component["fp"], s=20, c="red")
axis.scatter(swell1_component["Theta0"], swell1_component["fp"], s=20, c="cyan")
axis.scatter(swell2_component["Theta0"], swell2_component["fp"], s=20, c="yellow")

# start a movie
movie = spectrum.animate_frames(r_axis_lim=(0.000, 0.5), plot_title="Constructed Wave Spectrum",
                                min_data_value=0.00,
                                max_data_value=0.02
                                )

plt.show()
