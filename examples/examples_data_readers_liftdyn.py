import logging
import os
import sys

import colorcet as cc
import matplotlib.pyplot as plt
import numpy as np
from hmc_utils.misc import (create_logger)
from numpy import pi

# make sure that the local version of the hmc_marine modules are found first
sys.path = [".."] + sys.path
import hmc_marine as dr

file_name = os.path.join("data", "hermod_11_deep_POI1-acc-RAO.stf_plt")

logger = create_logger(console_log_level=logging.INFO)

logger.debug("module loaded : {}".format(dr.__file__))

logger.info("Reading Liftdyn file {}".format(file_name))

try:
    lift = dr.LiftDynReader(file_name=file_name)
except FileNotFoundError:
    # in case the file was not found we may be running the jupyter notebook within the explorer.
    # In that case we have to go one directory up first
    lift = dr.LiftDynReader(file_name=os.path.join("..", file_name))

plt.ioff()
fig1, axis1 = lift.plot()

fig2, axis2 = lift.plot(plot_title="All RAO vs Peak period", x_axis_type="period", x_axis_lim=(0, 20))

fig3, axis3 = lift.plot(plot_title="Surge RAO", x_axis_type="period", x_axis_lim=(0, 20), plot_index_selection=[0])

# the data can also be obtained. For instance

rao_surge = lift.dof_components[0]
rao_surge.report()

logger.info("Read Surgae RAO with following data : shape = {} type = {} item type {} first value {}"
            "".format(rao_surge.data.shape, type(rao_surge.data), type(rao_surge.data[0, 0]), rao_surge.data[0, 0]))

# let assume we need to resample  this data
lift.resample_data(new_directions=np.linspace(0, 2 * pi, endpoint=False, num=32),
                   new_frequencies=np.linspace(lift.frequencies[0], lift.frequencies[-1], endpoint=True, num=500))

rao_surge = lift.dof_components[0]
rao_surge.report()
logger.info("Read Surgae RAO with following data : shape = {} type = {} item type {} first value {}"
            "".format(rao_surge.data.shape, type(rao_surge.data), type(rao_surge.data[0, 0]), rao_surge.data[0, 0]))

fig4, axis4 = lift.plot(plot_title="After resampling")
fig5, axis5 = lift.plot(plot_title="All RAO vs Peak period After Resampling", x_axis_type="period", x_axis_lim=(0, 20))
fig6, axis6 = lift.plot(plot_title="Surge RAO After resampled", x_axis_type="period", x_axis_lim=(0, 20),
                        plot_index_selection=[0])

# We can apply the velocity shift over a range of velocities for all RAO's

lift.calculate_rao_per_velocity(n_velocity=4, max_velocity=5)
logger.info("shifted RAO over range {} m/s".format(lift.velocity_range))
n_rows = 2
n_cols = 2
ii = jj = 0
plot_component_index = 4
fig, axis = plt.subplots(nrows=n_rows, ncols=n_cols)
plt.subplots_adjust(hspace=0.4)
fig.canvas.set_window_title("Velocity shift")
freq_in_hz = np.array(lift.freq_in_hz)
direction_deg = np.rad2deg(np.append(lift.directions, [lift.directions[0] + 2 * pi]))
for cnt, velocity in enumerate(lift.velocity_range):

    ax = axis[ii][jj]

    lift.pick_rao_for_speed(velocity)

    ax.set_xlabel(lift.x_label)
    ax.set_ylabel(lift.y_label)

    # pick RAO nr 5, which is the pitch component
    component = lift.dof_components[plot_component_index]
    logger.info("Plotting component {} at velocity {} m/s".format(component.name,
                                                                  lift.current_speed_in_meter_per_second))

    # set the label frequency along the direction axis
    ax.set_ylim(0, 360.1)
    ax.set_yticks(range(0, 361, 90))

    # get the data magnitude and plot it. Note that only the RAO data in the data attribute is shift, which is a 3D
    # arrax of P x N x M. The 5th component can be selected and plotted
    data_mag = abs(lift.data[plot_component_index])

    # the data is periodic in the direction axis. Enforce that to make the plot nicely round from 0 ~ 360
    data_first_row = data_mag[:, 0].reshape(data_mag.shape[0], 1)
    data_mag = np.hstack((data_mag, data_first_row))

    # finally create the contour plot wit the RAO magnitude
    cs = ax.contourf(freq_in_hz, direction_deg, data_mag.T, cmap=cc.m_rainbow, zorder=0)

    ax.text(0.0, 1.05, "Velocity {:.1f} m/s".format(lift.current_speed_in_meter_per_second), transform=ax.transAxes,
            fontsize=10)

    jj += 1
    if jj == n_cols:
        jj = 0
        ii += 1

lift.plot(plot_title="After velocity shift")
plt.show()
